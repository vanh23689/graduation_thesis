export default {
    "These credentials do not match our records." : "Các thông tin xác thực này không khớp với hồ sơ của chúng tôi",
    "Please_confirm_your_account": "Vui lòng xác thực tài khoản của bạn",
    "sign_up_success_please_check_your_email" : " Vui lòng kiểm tra email của bạn",
    "edit_user_successfully": "Thay đổi người dùng thành công",
    "Logged in successfully": "Đăng nhập thành công"
}
