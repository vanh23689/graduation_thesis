import './bootstrap';
// import '../css/app.css';
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
// import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

//i18n
import { createI18n } from 'vue-i18n'
import messages from './Lang/index.js'
import vn from 'element-plus/dist/locale/vi.mjs'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// import vn from 'element-plus/es/locale/lang/ja';
//Vue Toast
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
//Vuex
import { createPinia } from "pinia";
//Axios
import { axios } from 'axios';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas);
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fab) ;
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(far);

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, app, props, plugin }) {
        const i18n = createI18n({
            locale: 'vn', // set locale
            fallbackLocale: 'en',
            allowComposition: true,
            messages
          })
        const pinia = createPinia();
        const root = createApp({ render: () => h(app, props) })
            .use(axios)
            .use(plugin)
            // .use(ZiggyVue, Ziggy)
            .mixin({ methods: { route } })
            .use(ElementPlus, {
                locale: vn,
            })
            .component("fa", FontAwesomeIcon)
            .use(VueToast, {position: 'top-right'})
            .use(pinia)
            .use(i18n);
            root.mount(el);
        return root
    },
});

InertiaProgress.init({ color: '#4B5563' });
