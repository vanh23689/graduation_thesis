export async function getPermissionList(currentPage) {
    return await axios.get(route('api.permissions.index'), {
        params: {
            page: currentPage
        }
    })
}

export async function deletePermission(permissionId) {
    await axios.delete(route('api.permissions.destroy', permissionId))
}
export async function editPermission(permissionId) {
    await axios.put(route('api.permissions.update', {
        params: {
            permissionId: permissionId
        }
    }))
}
