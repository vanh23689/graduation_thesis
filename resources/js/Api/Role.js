export async function getRoleList(currentPage) {
    return await axios.get(route('api.roles.index'), {
        params: {
            page: currentPage
        }
    })
}

export async function deleteRole(roleId) {
    await axios.delete(route('api.roles.destroy', roleId))
}
export async function editRole(roleId) {
    await axios.put(route('api.roles.update', {
        params: {
            roleId: roleId
        }
    }))
}
