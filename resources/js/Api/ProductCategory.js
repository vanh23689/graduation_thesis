export async function getProductCategoryList(currentPage) {
    return await axios.get(route('api.product_category.index'), {
        params: {
            page: currentPage
        }
    })
}
export async function deleteProductCategory(productCategoryId) {
    await axios.delete(route('api.product_category.destroy', productCategoryId))
}
export async function editProductCategory(productCategoryId) {
    await axios.put(route('api.product_category.update', {
        params: {
            userId: productCategoryId
        }
    }))
}
