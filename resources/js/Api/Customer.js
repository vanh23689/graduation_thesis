export async function getCustomerList(currentPage) {
    return await axios.get(route('api.showCustomers'), {
        params: {
            page: currentPage
        }
    })
}

export async function deleteCustomer(customerId) {
    await axios.delete(route('api.customers.destroy', customerId))
}

export async function editCustomer(customerId) {
    await axios.put(route('api.customers.update', {
        params: {
            customerId: customerId
        }
    }))
}

export async function customers(){
    return await axios.get(route('api.customers.index'))
}
