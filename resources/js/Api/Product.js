export async function getProductList(currentPage) {
    return await axios.get(route('api.products.index'), {
        params: {
            page: currentPage
        }
    })
}
export async function deleteProduct(productId) {
    await axios.delete(route('api.products.destroy', productId))
}
export async function editProduct(productId) {
    await axios.put(route('api.products.update', {
        params: {
            productId: productId
        }
    }))
}
