export async function getDonateList(currentPage) {
    return await axios.get(route('api.donates.index'), {
        params: {
            page: currentPage
        }
    })
}
export async function deleteDonate(donateId) {
    await axios.delete(route('api.donates.destroy', donateId))
}
