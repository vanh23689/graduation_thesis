export async function getUserList(currentPage) {
    return await axios.get(route('api.users.index'), {
        params: {
            page: currentPage
        }
    })
}

export async function deleteUser(userId) {
    await axios.delete(route('api.users.destroy', userId))
}
export async function editUser(userId) {
    await axios.put(route('api.users.update', {
        params: {
            userId: userId
        }
    }))
}
