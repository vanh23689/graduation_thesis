import zxcvbn from 'zxcvbn/lib/main.js'
export default {
    data(){
        return {
            showPassword: false,
            rules: {
                required: value => !!value || 'Enter a password',
                min: v => v.length >= 8 || 'Use 8 characters or more for your password',
                strength: v => zxcvbn(v).score >= 3 || 'Please choose a stronger password. Try a mix of letters, numbers, and symbols.',
            },
            color: null,
            width: 100
        }
    },
    watch: {
        'form.password': {
            handler() {
                const result = zxcvbn(this.form.password)

                switch (result.score) {
                    case 4:
                        this.color = "#8bc34a"
                        this.width = '100%'
                        return
                    case 3:
                        this.color = "#8bc34a"
                        this.width = '100%'
                        return
                    case 2:
                        this.color = "#ffeb3b"
                        this.width = '75%'
                        return
                    case 1:
                        this.color = "#ff9800"
                        this.width = '25%'
                        return
                    default:
                        this.width = 0
                        return
                }
            }
        }
    },
}
