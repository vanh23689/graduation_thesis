import axios from 'axios';
export default {
    data(){
        return{
            imageUrl: '',
            imgUpload: [],
            fileUpdate: null,
        }
    },
    methods: {
        upload_avatar(event) {
            this.fileUpdate = event.target.files[0]
            this.imgUpload.push(URL.createObjectURL(this.fileUpdate))
            this.form.avatar = this.fileUpdate
            if(event.isTrusted){
                this.imageUrl = this.imgUpload.slice(-1)[0]
            }
            let formData = new FormData();
            // append data
            formData.append('avatar', this.form.avatar);
            formData.append('_method', 'PUT')

            axios
                .post(route('api.user.upload', this.$attrs.user_id), formData, {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                })
                .then((res) => {
                    this.$toast.success('Thay đổi hình ảnh thành công !');
                })
                .catch((err) =>{
                    this.$toast.error('Thay đổi hình ảnh thất bại!');
                })
        },
    },
}
