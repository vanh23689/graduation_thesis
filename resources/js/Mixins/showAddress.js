import axios from 'axios';
export default {
    data(){
        return {
            citySelect : '',
            districtSelect : '',
            wardSelect: '',
            cities: [],
            districts: [],
            wards: [],
        }
    },
    created(){
        this.getCity()
    },
    methods: {
        getCity(){
            axios
                .get(route('api.get_city'))
                    .then((res) => {
                        this.cities = res.data
                    })
                    .catch( (e) => {
                        console.log(e);
                    })

        },
        getProvince(code){
            axios
                .get(route('api.get_provinces', {code: code}))
                    .then((res) => {
                        this.districts = res.data?.districts
                        this.form.city = res.data.name
                    })
                    .catch( (e) => {
                        console.log(e);
                    })

        },
        getWard(province_code){
            axios
                .get(route('api.get_ward', {code: province_code}))
                    .then((res) => {
                        this.wards = res.data?.wards
                        this.form.district = res.data.name
                    })
                    .catch((e) => {
                        console.log(e);
                    })
        }
    },
    watch: {
        'wardSelect'(e){
            this.form.ward = this.wards?.filter((p) => p.code === e).shift(0)?.name
            return this.form.ward
        }
    }
}
