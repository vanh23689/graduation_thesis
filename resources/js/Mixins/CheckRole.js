export default {
    data(){
        return {
            check_role: true,
        }
    },
    created() {
        this.checkRole();
    },
    methods: {
        checkRole(){
            this.$attrs.auth?.user.roles.filter((role) => {
                (role.name == "super_admin" || role.name == "admin") ? this.check_role == true : this.check_role == false
            });
        }
    }
}

