@extends('emails.template')
@section('content')
<div class="fit-content">
    <h4>Xin chào {{ $data['first_name'] }} {{ $data['last_name'] }} !</h4>
    <div style="text-align: center;">
        <a class="btn-primary" href="{{ $data['url'] }}"><strong>Xác nhận địa chỉ email</strong> </a>
    </div>
</div>
@endsection
