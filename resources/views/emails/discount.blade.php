<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mã giảm giá</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'poppins', sans-serif;

        }
        .container{
            width: 100%;
            height: 100vh;
            background: #f0fff3;
            display: flex;
            align-items: center;
            justify-content: center;

        }
        .coupon-card{
            background: linear-gradient(135deg, #7158fe, #9d4de6);
            color: #fff;
            text-align: center;
            padding: 40px 80px;
            border-radius: 15px;
            box-shadow: 0 10px 10px 0 rgba(0,0,0,0.15);
            position: relative;

        }
        .logo{
            width: 80px;
            border-radius: 8px;
            margin-bottom: 20px;

        }
        .coupon-card h3{
            font-size: 28px;
            font-weight: 400;
            line-height: 40px;

        }
        .coupon-card p{
            font-size: 15px;

        }
        .coupon-row{
            display: flex;
            align-items: center;
            margin: 25px auto;
            width: fit-content;

        }
        #cpnCode{
            border: 1px dashed #fff;
            padding: 10px 20px;
            border-right: 0;

        }
        #cpnBtn{
            border: 1px solid #fff;
            background: #fff;
            padding: 10px 20px;
            color: #7158fe;
            cursor: pointer;
        }
        .circle1, .circle2{
            background: #f0fff3;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);

        }
        .circle1{
            left: -25px;
        }
        .circle2{
            right: -25px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="coupon-card">
            <img src="https://i.postimg.cc/KvTqpZq9/uber.png" class="logo">
            <h3>20% mã giảm giá sản phầm giày Nike<br>sử dụng Agribank, ACB</h3>
            <div class="coupon-row">
                <span id="cpnCode">STEALDEAL20</span>
                <span id="cpnBtn">Lưu</span>
            </div>
            <p>Hạn dùng: 20-11-2031</p>
            <div class="circle1"></div>
            <div class="circle2"></div>
        </div>
    </div>
</body>
<script>
     var cpnBtn = document.getElementById("cpnBtn");
        var cpnCode = document.getElementById("cpnCode");

        cpnBtn.onclick = function(){
            navigator.clipboard.writeText(cpnCode.innerHTML);
            cpnBtn.innerHTML ="COPIED";
            setTimeout(function(){
                cpnBtn.innerHTML="COPY CODE";
            }, 3000);
        }
</script>
</html>
