@component('mail::message')

<div style="
    color: #3d4852;
    font-size: 19px;
    font-weight: bold;
    text-decoration: none;
    display: inline-block;"
>
Xin chào
</div>
<span>
    {{ $user->first_name }} {{ $user->last_name }}
</span>

<div>Tài khoản của bạn đã bị khóa</div>
<div> Vui lòng donate <b style="color:red">1 củ</b> cho chúng tôi</div>


@component('mail::button', ['url' => $url])
Nhấn vào đây
@endcomponent

Cảm ơn bạn !
<br>
Đội ngũ {{ config('app.name') }}
@endcomponent
