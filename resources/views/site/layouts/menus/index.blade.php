<section class="menu">
    <div class="main_header_area animated">
        <div class="container">
            <nav id="navigation1" class="navigation">
                <div class="nav-header">
                    <div class="nav-header-bar">
                        <i class="fas fa-bars"></i>
                        <span>Tất cả</span>
                    </div>
                    <ul class="nav-menu-toggle">
                        <li><a href="https://github.com/mblode" target="_blank">
                                <div>cửa hàng</div>
                            </a></li>
                        <li>
                            <a href="https://codepen.io/mblode/" target="_blank">
                                <div>sản phẩm</div>
                                <i class="fas fa-chevron-right"></i>
                            </a>
                            <ul class="sub-menu megamenu">
                                <li>
                                    <a href="">Danh mục sản phẩm</a>
                                    <ul class="sub-menu-column">
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Smart Phone
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Laptop
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="">Product Categories</a>
                                    <ul class="sub-menu-column">
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Smart Phone
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Laptop
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="">Product Categories</a>
                                    <ul class="sub-menu-column">
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Smart Phone
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                        <li class="menu__item">
                                            <a href="" class="menu__item-link">
                                                Laptop
                                            </a>
                                            <i class="fas fa-angle-right"></i>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="https://codepen.io/Lewitje/pen/CwFce/" target="_blank">
                                <div>Shop</div>
                            </a></li>
                    </ul>
                    <div class="nav-toggle"></div>
                </div>
                <div class="nav-menus-wrapper">
                    <ul class="nav-menu align-to-right">
                        <li>
                            <a href="{{ route('home.index') }}" target="_blank">Trang chủ</a>
                        </li>
                        <li>
                            <a href="#">Đánh giá</a>
                            <ul class="nav-dropdown">
                                <li><a href="#" target="_blank">Đánh giá sản phẩm</a></li>
                                <li><a href="#" target="_blank">Chi tiết sản phẩm</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Sản phẩm</a>
                            <div class="megamenu-panel">
                                <div class="megamenu-lists">
                                    @foreach($categories as $key => $product)
                                    <ul class="megamenu-list list-col-4">
                                        <li>
                                            @if($product->parent_id == 0)
                                                <a href="{{ route('product-category.show', $product->id) }}" target="_blank">
                                                    <div>{{ $product->name }}</div>
                                                </a>
                                            @else
                                                 <ul class="sub-menu megamenu">
                                                    <li class="menu__item">
                                                        <i class="fas fa-angle-right"></i>
                                                        <a href="{{ route('product-category.show', $product->id) }}" class="menu__item-link">
                                                            {{ $product->name }}
                                                        </a>
                                                    </li>
                                            </ul>
                                            @endif


                                        </li>
                                    </ul>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#">Mã giảm giá</a>
                            <ul class="nav-dropdown">
                                <li><a href="#" target="_blank">Danh sách mã giảm giá</a></li>
                                <li><a href="#" target="_blank">Mã giảm giá của bạn</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Khám phá</a>
                            <ul class="nav-dropdown">
                                <li><a href="#" target="_blank">Shop nổi bật</a></li>
                                <li><a href="#" target="_blank">Yêu thích</a></a></li>
                                <li><a href="#" target="_blank">Blog</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</section>
