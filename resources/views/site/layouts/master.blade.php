<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content=""/>
        <meta name="robots" content="INDEX,FOLLOW"/>
        <link  rel="canonical" href="" />
        <meta name="author" content="">
        <link  rel="icon" type="image/x-icon" href="" />

        {{--<meta property="og:image" content="{{$image_og}}" />--}}
        <meta property="og:site_name" content="http://localhost/site/home" />
        <meta property="og:description" content="" />
        <meta property="og:title" content="" />
        <meta property="og:url" content="" />
        <meta property="og:type" content="website" />
        <title>Compare Website </title>
        <link rel="icon" type="image/x-icon" href="{{ asset('favicon/favicon.jpg') }}">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <!-- Fontawesome-->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <!-- Owl Carousel CSS-->
        <link rel="stylesheet" href="{{ asset('site/assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/owl.theme.default.css') }}">
        <!-- Light Slider -->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css" />
        <!-- AOS Css -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link
            href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"
            rel="stylesheet"
        />
        <!-- ✅ load jQuery ✅ -->
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"
        ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="{{ asset('site/assets/css/style.css') }}">
        @yield('style.css')

    </head>
<body>
    @include('site.layouts.header')
    @yield('content')
    @include('site.layouts.footer')
    <!-- modal start -->
    @include('site.pages.modals.index')
    <!-- modal end -->
    <!-- compare start -->
    @include('site.layouts.others.compare')
    <!-- compare end -->
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
      integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> --}}
    <script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('site/assets/js/owl.carousel.min.js') }}"></script>
    <!-- Light Slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('site/assets/js/main.js') }}"></script>
    @yield('footer_script')
</body>
</html>
