<!-- Modal -->
<div class="modal fade" id="electronicDevice" tabindex="-1" role="dialog" aria-labelledby="electronicDeviceLabel"
    aria-hidden="true">
    <div class="modal-dialog-centered container" role="document">
        <div class="modal-content">
            <div class="modal-header" style="justify-content: end;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul id="lightSliderModal" class="slider-modal">
                            {{-- <li data-thumb="" data-src="">
                                <img src="" />
                            </li> --}}

                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="product__slide">
                            <h2 class="product__slide-title" id="title">
                            </h2>
                            <div class="product__slide-evaluate d-sm-flex align-items-sm-center">
                                <ul>
                                    <li>
                                        <div class="star-rating">
                                            <i class="fas fa-star full-star"></i>
                                            <i class="fas fa-star full-star"></i>
                                            <i class="fas fa-star-half-alt half-star"></i>
                                            <i class="far fa-star empty-star"></i>
                                            <i class="far fa-star empty-star"></i>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="p">
                                            <span>2.0/5</span>
                                            - 1 ratings
                                        </div>
                                    </li>
                                    <li>
                                        <div class="p"><span>1K+</span> Lượt xem</div>
                                    </li>
                                </ul>
                                <div class="product__slide-wishlist ml-sm-auto mt-3 mt-sm-0">
                                    <span>
                                        <a href="" class="wishlist__heart">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </span>
                                    <span class="wishlist__compare">
                                        <a href="">
                                            <i class="far fa-balance-scale"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="product__slide-description">
                                <li>- Mã sản phẩm: <span id="product_code"></span></li>
                                <li>- Tình trạng: <span id="order_status"></span></li>
                                <li>- Xem tại trang: <span id="see_at_page"></span></li>
                            </div>
                            <div class="product__slide-publicer">
                                <div class="im-publicer">
                                    Người bán:
                                    <a class="url fn n" href="https://themeim.com/wp/blurb/author/admin/">Admin</a>
                                </div>
                            </div>
                            <ul class="product__slide-market">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
