<!-- Compare -->
<div class="modal right fade" id="myModalBalance" tabindex="-1" role="dialog" aria-labelledby="myModalBalance">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalBalance">
                    So sánh
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="compare__tab">
                    <div class="compare__tab-header">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            @foreach($categories as $key => $item)
                            <li class="compare__item nav-item" role="presentation">
                                <span class="nav-link {{ $item->id == 1 ? 'active' : '' }}"
                                    id="pills-{{ $item->name }}-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-{{ $item->name }}" type="button" role="tab"
                                    aria-controls="pills-{{ $item->name }}" aria-selected="true">
                                    {{ $item->name }}
                                    {{-- (<span> {{ count(array($item['category'])) }} </span>) --}}
                                </span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="compare__tab-content">
                        <div class="compare__box tab-content" id="pills-tabContent">
                            @foreach($categories as $key => $item)
                            <div class="tab-pane fade {{ $item->id == 1 ? 'show active' : '' }}"
                                id="pills-{{ $item->name }}" role="tabpanel"
                                aria-labelledby="pills-{{ $item->name }}-tab">
                                @if (!empty(\Illuminate\Support\Facades\Session::get('cart')))
                                    @foreach (\Illuminate\Support\Facades\Session::get('cart') as $value)
                                        @if($value['category'] == $item->name)
                                        <div class="compare__box-item" data-category={{ $value['category'] }}>
                                            <a class="item__box-icon" title="Remove" data-id="{{ $value['id'] }}"><i class="fa fa-times" aria-hidden="true"
                                                    ></i></a>
                                            <div class="item__box-img">
                                                <a href="{{ route('product-detail.show', $value['id']) }}">
                                                    <img data-image="{{ $value['image'] }}" src="{{ $value['image'] }}"
                                                        alt="{{ $value['image'] }}">
                                                </a>
                                            </div>
                                            <div class="item__box-content">
                                                <div class="box__title">
                                                    <a data-name="{{ $value['name'] }}"
                                                        href="{{ route('product-detail.show', $value['id']) }}">{{
                                                        $value['name'] }}</a>
                                                </div>
                                                <div class="product__rating">
                                                    <div class="star-rating">
                                                        <div class="star-rating-box">
                                                            <i class="fas fa-star full-star"></i>
                                                            <i class="fas fa-star full-star"></i>
                                                            <i class="fas fa-star-half-alt half-star"></i>
                                                            <i class="far fa-star empty-star"></i>
                                                            <i class="far fa-star empty-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="price-start">
                                                    <p>Giá
                                                        <strong class="blurb-price-color">
                                                            <span class="min_price" data-min_price="{{ $value['min_price'] }}">{{
                                                                $value['min_price'] }}</span>
                                                            &nbsp;-&nbsp;
                                                            <span class="max_price" data-max_price="{{ $value['max_price'] }}">{{
                                                                $value['max_price'] }}</span>
                                                        </strong>
                                                    </p>
                                                </div>
                                            </div>
                                            </a>
                                        </div>
                                        @endif
                                    @endforeach
                                @endif

                            </div>
                            @endforeach
                        </div>
                        <a href="{{ route('compare.index') }}" class="compare__btn btn btn-primary">
                            So sánh
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </a>
                        <span class="error-notice error mt-4">Vui lòng thêm các mục vào nhóm so sánh này hoặc chọn nhóm
                            không trống</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div>
@yield('footer_script')
<script>
    $(".item__box-icon").on('click', function (e){
        e.preventDefault();
        let id = $(this).attr('data-id')
        console.log("🚀 ~ file: compare.blade.php ~ line 104 ~ id", id)
        $.ajax({
            type: 'DELETE',
            url: '{{ route('remove.from.cart') }}',
            data: {
                id: id
            },
            success: function(data){
                $('#pills-tabContent').html(data)
                $('#pills-Smartphone-tab span').text(data.length)
            }
        }).done(function() {
            setTimeout(function(){
                $("#overlay").fadeOut(300), 500;
            }, 500);
        });
    });
    $(".compare__btn").on('click', function(e) {
        e.preventDefault();
        let id = [];
        $(this).parent("div.compare__tab-content").find(".tab-pane.fade.show.active .compare__box-item a.item__box-icon").each(function( index, value ){
            id.push(parseInt(value.getAttribute('data-id')));
        })
        let count = $(".tab-pane.fade.active.show .compare__box-item").length

        let image = $(this).parent("div.compare__tab-content").find("img").attr("data-image")
        let name = $(this).parent("div.compare__tab-content").find(".box__title a").attr("data-name")
        let min_price = $(this).parent("div.compare__tab-content").find('.min_price').attr("data-min_price")
        let max_price = $(this).parent("div.compare__tab-content").find('.max_price').attr("data-max_price")
        let category = $(this).parent("div.compare__tab-content").find('.tab-pane.fade.show.active .compare__box-item').attr("data-category")
        let url = "{{ route('compare.index') }}"

        if(count < 2){
            $(".error-notice").addClass("d-block");
        }else {
            $.ajax({
                type: 'POST',
                url: '{{ route("compare.post") }}',
                data: {
                    id: id,
                    image: image,
                    min_price : min_price,
                    max_price : max_price,
                    category: category
                },
                success: function(data){
                    setInterval( window.location = url , 1000);
                }
            })
        }
    });
    $(".compare__item").click(function(){
        $(".error-notice").removeClass("d-block");
    });
</script>
