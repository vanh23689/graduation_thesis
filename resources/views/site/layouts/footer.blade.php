<!-- footer -->
<footer class="footer">
    <div class="container">
       <div class="footer__widget">
          <div class="row">
             <div class="col-md-3 col-sm-6">
                <div class="footer__widget-about">
                   <a href="">
                   <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/logo-1-4.png" alt="Logo">
                   </a>
                </div>
                <div class="footer__widget-text">
                   <p>Lorem ipsum dolor sit amet, anim id est adipisci vam aliquam qua anim id est laborum laborum. Perspconsectetur, adipisci vam aliquam qua anim id est laborum.</p>
                </div>
                <div class="footer__widget-social">
                   <a href="twitter.com">
                   <i class="fab fa-twitter"></i>
                   </a>
                   <a href="facebook.com">
                   <i class="fab fa-facebook-f"></i>
                   </a>
                   <a href="youtube.com">
                   <i class="fab fa-youtube"></i>
                   </a>
                </div>
             </div>
             <div class="col-md-3 col-sm-6">
                <div class="footer__widget-nav">
                   <h6 class="widget-title">Footer Menu</h6>
                   <div class="widget-menu">
                      <ul>
                         <li>
                            <a href="https://themeim.com/wp/blurb/blog/">Blog</a>
                         </li>
                         <li>
                            <a href="https://themeim.com/wp/blurb/blog/">Blog</a>
                         </li>
                         <li>
                            <a href="https://themeim.com/wp/blurb/blog/">Blog</a>
                         </li>
                         <li>
                            <a href="https://themeim.com/wp/blurb/product-reviews/">Product Reviews</a>
                         </li>
                         <li>
                            <a href="https://themeim.com/wp/blurb/product-reviews/">Product Reviews</a>
                         </li>
                      </ul>
                   </div>
                </div>
             </div>
             <div class="col-md-3 col-sm-6">
                <div class="footer__widget-nav">
                   <h6 class="widget-title">Recent Review</h6>
                   <div class="widget-menu">
                      <div class="widget-menu-img">
                         <img src="https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/tt3-2-4-p2zq8jmt1qxj1hcydr1adpqxibd4zmbsomvyxvxy9k.png" alt="">
                      </div>
                      <div class="widget-menu-content">
                         <a href="https://themeim.com/wp/blurb/product/titan-modern-smart-hand-watch-2/">Titan dashing smart  watch</a>
                         <div class="star-rating">
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star-half-alt half-star"></i>
                            <i class="far fa-star empty-star"></i>
                            <i class="far fa-star empty-star"></i>
                         </div>
                      </div>
                   </div>
                   <div class="widget-menu">
                      <div class="widget-menu-img">
                         <img src="https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/tt3-2-4-p2zq8jmt1qxj1hcydr1adpqxibd4zmbsomvyxvxy9k.png" alt="">
                      </div>
                      <div class="widget-menu-content">
                         <a href="https://themeim.com/wp/blurb/product/titan-modern-smart-hand-watch-2/">Titan dashing smart  watch</a>
                         <div class="star-rating">
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star-half-alt half-star"></i>
                            <i class="far fa-star empty-star"></i>
                            <i class="far fa-star empty-star"></i>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-md-3 col-sm-6">
                <div class="footer__widget-nav">
                   <h6 class="widget-title">Đăng ký bản tin</h6>
                   <div class="widget-custom">
                      <div class="widget-custom-text">
                        Bằng cách đăng ký vào danh sách gửi thư của chúng tôi, bạn sẽ được giảm giá 10%.
                      </div>
                      <div class="widget-custom-letter">
                         <form action="{{ route('subscribe') }}" method="POST" id="form_newsletter">
                            @csrf
                            <div class="form-control">
                               <input type="text"
                                    name="email"
                                    placeholder="Nhập email"
                                    id="email_subscribe"
                                />
                            </div>
                            <span id="error" style="display: inline-block; text-transform: capitalize;">

                            </span>
                            <button class="btn btn-primary" type="submit">
                                Tham gia
                            </button>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <div class="copyright">
             <div class="col-md-6 text-left">
                <div class="copyright-text">
                   <p>Copyright © <a href="https://themeim.com/">ThemeIM</a> 2019. All Right Reserved</p>
                </div>
             </div>
             <div class="col-md-6">
                <div class="footer__brand">
                   <a href="">
                   <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/4.png" class="img-fluid" alt="">
                   </a>
                   <a href="">
                   <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/5.png" class="img-fluid" alt="">
                   </a>
                   <a href="">
                   {{-- <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/6.png" class="img-fluid" alt=""> --}}
                   </a>
                </div>
             </div>
          </div>
       </div>
    </div>
 </footer>
{{-- @include('site.layouts.others.compare'); --}}
@include('site.layouts.others.modal')
@include('site.layouts.others.back_to_top')
@yield('footer_script')
<script>
    $('#form_newsletter').submit(function(e) {
        e.preventDefault();

        $.ajax({
            url: '/site/subscribe',
            type: 'POST',
            data: {
                _token: $('input[name=_token]').val(),
                email: $("#email_subscribe").val(),
            },
            dataType: 'json',
            success: function(res) {
            },
            error: function(error) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            }
        }).done(function(error) {
            $("#error").text(error.error.email[0]);
            setTimeout(function(){
                $("#overlay").fadeOut(300), 500;
            }, 500);
        });
    })
</script>
