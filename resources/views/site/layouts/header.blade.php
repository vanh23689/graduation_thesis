@yield('style.css')
<style>
.profile {
    width: 20em;
    position: absolute;
    top: 5px;
    z-index: 10;
    left: -32px;
    font-family: pfs-bold;
    color: #86919a;
    text-transform: uppercase;
    transition: width 0.5s;
    will-change: width;
}
.profile .option {
    padding: 0.5em 1em;
    cursor: pointer;
    background-color: #212529;
    color: #fff;
    font-weight: 500;
    text-transform: none;
    font-size: 12px;

}
.profile .option:not(.active) {
  display: none;
  opacity: 0;
  transform: translateY(-50%);
}
.profile.visible {
  -webkit-animation: bounce 1s;
          animation: bounce 1s;
  width: 24em;
}
.profile.visible:before, .profile.visible:after {
  border-color: #fff;
}
.profile.visible:before {
  opacity: 0;
}
.profile.visible:after {
  opacity: 1;
}
.profile.visible .option, .profile.visible a {
  color: #fff;
  display: block;
}
.profile.opacity .option {
  transform: translateZ(0);
  opacity: 1;
}
.profile.opacity .option:nth-child(0) {
  transition: opacity 0.5s 0s, transform 0.5s 0s;
}
.profile.opacity .option:nth-child(1) {
  transition: opacity 0.5s 0.1s, transform 0.5s 0.1s;
}
.profile.opacity .option:nth-child(2) {
  transition: opacity 0.5s 0.2s, transform 0.5s 0.2s;
}
.profile.opacity .option:nth-child(3) {
  transition: opacity 0.5s 0.3s, transform 0.5s 0.3s;
}
.profile.opacity .option:nth-child(4) {
  transition: opacity 0.5s 0.4s, transform 0.5s 0.4s;
}
.profile.opacity .option:nth-child(5) {
  transition: opacity 0.5s 0.5s, transform 0.5s 0.5s;
}
.profile.opacity .option:nth-child(6) {
  transition: opacity 0.5s 0.6s, transform 0.5s 0.6s;
}
.profile.opacity .option:nth-child(7) {
  transition: opacity 0.5s 0.7s, transform 0.5s 0.7s;
}
.profile.opacity .option:nth-child(8) {
  transition: opacity 0.5s 0.8s, transform 0.5s 0.8s;
}
.profile.opacity .option:nth-child(9) {
  transition: opacity 0.5s 0.9s, transform 0.5s 0.9s;
}
.profile.withBG .option {
  transition: background-color 0.1s;
}
.profile.withBG .option:not(.placeholder):hover {
  background-color: #5aafee;
}
.profile.withBG .option:not(.placeholder).active {
  background-color: #5aafee;
}
.profile:after, .profile:before {
  content: "";
  position: absolute;
  top: 1.5em;
  right: 1em;
  width: 0.75em;
  height: 0.75em;
  border: 0.2em solid #86919a;
  transform: rotate(45deg);
  transform-origin: 50% 50%;
  transition: opacity 0.2s;
}
.profile:before {
  border-left: none;
  border-top: none;
  top: 1.2em;
}
.profile:after {
  border-right: none;
  border-bottom: none;
  opacity: 0;
}

.mini-hack {
  opacity: 0;
  transform: translateY(-50%);
}

</style>

<header class="header">
    <div class="top__bar">
        <div class="container">
            <div class="row">
              <div class="col-lg-4 col-xs-6">
                <div class="top__bar-media"><a href=""><i class="fab fa-facebook-f"></i></a><a href=""><i class="fab fa-instagram"></i></a><a href=""><i class="fab fa-pinterest"></i></a></div>
              </div>
              <div class="col-lg-4">
                <div class="top__bar-text">
                  <p><span style="color: red">UPTO 50% OFF</span> TO ALL VIRTUAL PRODUCTS</p>
                </div>
              </div>
              <div class="col-lg-4 col-xs-6">
                <div class="top__bar-right">
                    @guest('customer')
                        <button class="sign-in btn btn-primary fst-color-bg" type="button" data-toggle="modal" data-target="#exampleModalCenter">
                            <i class="fas fa-user"></i>
                        </button>
                    @endguest

                    @auth('customer')

                        <div class="profile">
                            <div class="option active placeholder" data-value="placeholder">
                                Tên người dùng: {{ auth('customer')->user()->name }}
                            </div>
                            <div class="option" data-value="wow">
                                <a href="{{ route('logout.customer') }}" style="width: 100%;">
                                    Đăng xuất
                                </a>
                            </div>

                        </div>
                    @endauth


                    <div class="account__section">
                        <select name="USD">USD
                        <option value="USD">USD</option>
                        <option value="VND">VND</option>
                        </select>
                    </div>
                </div>
              </div>
            </div>

          </div>
    </div>

    <div class="center__bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="{{ route('home.index') }}" class="navbar-brand mr-0">
                            <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/Blurb_Logo1.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    @include('site.pages.search.index')
                </div>
                <div class="col-lg-3">
                    <div class="center__bar-cart">
                        {{-- <form action="{{ route('donate.create') }}" method="POST">
                            @csrf
                            <button name="redirect" type="submit" title="Donate">
                                <i class="far fa-donate"></i>
                            </button>
                        </form> --}}
                        <a href="{{ route('donate.vnpay.index') }}" title="Donate">
                            <i class="far fa-donate"></i>
                        </a>
                        <a href="{{ route('wishlist.index') }}">
                            <i class="far fa-heart"></i>
                            <div class="count count-wishlist">{{ count((array) session('wishlist')) }}</div>
                        </a>
                        <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModalBalance" style="padding: 0">
                            <i class="fas fa-balance-scale"></i>
                            <div class="count count-item">{{ count((array) session('cart')) }}</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="col-md-12 px-0">
                        <div class="modal-section">
                            <div class="modal-section-img">
                                <img src="https://www.themeim.com/demo/blurb/demo/img/login-img-1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="modal-section-box">
                                <ul class="nav nav-tabs" id="myTab" role="tablist" style="padding: 0;">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="login-tab" data-bs-toggle="tab" data-bs-target="#login" type="button" role="tab" aria-controls="login" aria-selected="true">Đăng nhập</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="register-tab" data-bs-toggle="tab" data-bs-target="#register" type="button" role="tab" aria-controls="register" aria-selected="false">Đăng ký</button>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                                        <div class="login-section text-center">
                                            <div class="login-form text-left">
                                                <form>
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="text" class="form-control" name="email" placeholder="John mist |">
                                                    </div>
                                                    <p class="error" id="email"></p>
                                                    <div class="form-group">
                                                        <label for="password">Mật khẩu</label>
                                                        <input type="password" class="form-control" name="password" autocomplete="on" placeholder="*** *** ***">
                                                    </div>
                                                    <p class="error" id="password"></p>
                                                    <button type="submit" class="btn btn-primary wd-login-btn loginForm">Đăng nhập</button>

                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input">
                                                            Lưu mật khẩu
                                                        </label>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                                        <div class="singup-section text-center">
                                            <div class="login-form text-left">
                                                <p class="success"></p>
                                                <form method="POST" id="registerForm" name="registerForm">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="name">Tên</label>
                                                        <input type="text" class="form-control" name="name_register" placeholder="Tên">
                                                    </div>
                                                    <p class="error" id="name"></p>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="text" class="form-control" name="email_register" placeholder="Email">
                                                    </div>
                                                    <p class="error" id="email"></p>
                                                    <div class="form-group">
                                                        <label for="password">Mật khẩu</label>
                                                        <input type="password" class="form-control" name="password_register" autocomplete="on" placeholder="*** *** ***">
                                                    </div>
                                                    <p class="error" id="password"></p>
                                                    <div class="form-group">
                                                        <label for="password_confirmation">Nhập lại mật khẩu</label>
                                                        <input type="password" class="form-control" name="password_confirmation_register" autocomplete="on" placeholder="*** *** ***">
                                                    </div>
                                                    <p class="error" id="password_confirmation"></p>
                                                    <button type="submit" class="btn btn-primary wd-register-btn" value="add">Đăng ký</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-center flex-column mt-4" style="font-size: 14px;">
                                    <a class="ml-1 fb-login-button" href="{{ route('auth.social', 'facebook') }}" id="btn-fblogin" style="background: #3b5998;color: #fff;border-radius: 9px; padding: 10px 20px; font-size: 16px;">
                                        <i class="fab fa-facebook-f"></i> Đăng nhập với facebook
                                    </a>
                                    <a class="ml-1 mt-4 fb-login-button" href="{{ route('auth.social', 'google') }}" id="btn-gglogin" style="background: #fff;color: #62646a;border-radius: 9px; padding: 10px 20px; font-size: 16px; border: 1px solid #e4e5e7; font-weight: 600;">
                                        <i class="fab fa-google"></i> Đăng nhập với google
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@yield('footer_script')
<script>
    $(document).ready(function() {
        $(".profile .option").click(function() {
            var val = $(this).attr("data-value"),
                $profile = $(".profile"),
                prevActive = $(".profile .option.active").attr("data-value"),
                options = $(".profile .option").length;
                $profile.find(".option.active").addClass("mini-hack");
                $profile.toggleClass("visible");
                $profile.removeClass("withBG");
                $(this).css("top");
                $profile.toggleClass("opacity");
                $(".mini-hack").removeClass("mini-hack");
                if ($profile.hasClass("visible")) {
                    setTimeout(function() {
                        $profile.addClass("withBG");
                    }, 400 + options*100);
                }
                triggerAnimation();
                    if (val !== "placeholder" || prevActive === "placeholder") {
                    $(".profile .option").removeClass("active");
                    $(this).addClass("active");
                };
            });

            function triggerAnimation() {
                var finalWidth = $(".profile").hasClass("visible") ? 20 : 20;
                    $(".profile").css("width", "24em");
                    setTimeout(function() {
                    $(".profile").css("width", finalWidth + "em");
                }, 400);
            }
        });

        $(".wd-register-btn").click(function(e){
            var _token = $("input[name='_token']").val();
            var name = $("input[name='name_register']").val();
            var email = $("input[name='email_register']").val();
            var password = $("input[name='password_register']").val();
            var password_confirmation = $("input[name='password_confirmation_register']").val();

            $.ajax({
                url: "{{ route('register-customer') }}",
                type: "POST",
                data: {
                    _token:_token,
                    email : email,
                    name : name,
                    password : password,
                    password_confirmation : password_confirmation
                },
                success: function(data){
                    $(".success").html(data.success);
                },
                error: function(data){
                    let resp = data.responseJSON;
                    for (index in resp) {
                        $("#" + index).html(resp[index]);
                    }

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
            e.preventDefault();
        })

        $(".wd-login-btn").click(function(e){
            var _token = $("input[name='_token']").val();
            var email = $("input[name='email']").val();
            var password = $("input[name='password']").val();

            $.ajax({
                url: "{{ route('login-customer') }}",
                type: "POST",
                data: {
                    _token:_token,
                    email : email,
                    password : password,
                },
                success: function(data){
                    $(".success").html(data.success);
                },
                error: function(data){
                    let resp = data.responseJSON;
                    for (index in resp) {
                        $("#" + index).html(resp[index]);
                    }

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                    location.reload();
                }, 500);
            });
            e.preventDefault();
        })
</script>


