@if (!empty($pc))
    @foreach ($pc as $key => $value)
        @if ($key == 0)
            <article class="col-12" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1300">
                <figure class="amazon__product-box">
                    <input type="hidden" data-id="{{ $pc[$key]->id }}">
                    <div class="product__meta-top">
                        <a class="product__img" href="{{ route('product-detail.show', $pc[$key]->id) }}"
                            data-img="{{ $pc[$key]->img }}">
                            <img src="{{ $pc[$key]->img }}" alt="">
                        </a>
                        <div class="product__view compare-btn">
                            <button class="btn btn-primary btn-sm view-pc" data-toggle="modal"
                                data-target="#electronicDevice">
                                <i class="fas fa-eye"></i>
                                Xem nhanh scsa
                            </button>
                        </div>
                    </div>
                    <div class="product__meta-bottom">
                        <div class="product__rating">
                            <div class="star-rating">
                                <div class="star-rating-box">
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star-half-alt half-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                </div>
                            </div>
                            <div class="product__rating-right">
                                <div class="post-views">
                                    <span class="post-view-number">{{ $pc[$key]->total_views }}</span>
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </div>
                                <div class="product__line"></div>
                                <div class="wishlist">
                                    <span>
                                        <a href="{{ route('add.to.wishlist', $pc[$key]->id) }}">
                                            <i class="fal fa-heart"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('product-detail.show', $pc[$key]->id) }}" class="product-name">
                            <h6 data-name="{{ $pc[$key]->name }}">{{ $pc[$key]->name }}</h6>
                        </a>
                        <a href="{{ route('product-detail.show', $pc[$key]->id) }}">
                            <div class="price-start">
                                <p>Giá
                                    <strong class="blurb-price-color">
                                        {{-- <span>đ</span> --}}
                                        <span class="min_price" data-min_price="{{ $maxPrice['pc'] }}">
                                            {{ $maxPrice['pc'] }}
                                        </span>
                                        &nbsp;-&nbsp;
                                        {{-- <span>₫ </span> --}}
                                        <span class="max_price" data-max_price="{{ $minPrice['pc'] }}">
                                            {{ $minPrice['pc'] }}
                                        </span>
                                    </strong>
                                </p>
                            </div>
                        </a>
                        <div class="product__meta-footer">
                            <div class="left__content">
                                <span>Danh mục:</span>
                                <i class="fas fa-store"></i>
                                @if (!empty($pc[$key]->categories))
                                    <a
                                        href="{{ route('product-category.show', $pc[$key]->categories->id) }}">{{ $pc[$key]->categories->name }}</a>
                                @endif
                            </div>

                            <div class="compare-btn">
                                <span>
                                    <a class="btn btn-primary btn-sm sl-button liked add-pc" title="Compare"
                                        data-icon="fa fa-balance-scale"><i class="fa fa-balance-scale"
                                            aria-hidden="true"></i> </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </figure>
            </article>
        @endif
    @endforeach
@endif
