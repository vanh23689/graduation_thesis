@if (!empty($tablet))
@foreach ($tablet as $key => $value)
@if ($key == 0)
    <article class="col-12" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
        <figure class="amazon__product-box">
            <input type="hidden" data-id="{{ $tablet[$key]->id }}">
            <div class="product__meta-top">
                <a class="product__img" href="{{ route('product-detail.show', $tablet[$key]->id ) }}" data-img="{{ $tablet[$key]->img }}">
                    <img src="{{ $tablet[$key]->img }}" alt="">
                </a>
                <div class="product__view compare-btn">
                    <button class="btn btn-primary btn-sm view-tablet" data-toggle="modal" data-target="#electronicDevice">
                        <i class="fas fa-eye"></i>
                        Xem nhanh
                    </button>
                </div>
            </div>
            <div class="product__meta-bottom">
                <div class="product__rating">
                    <div class="star-rating">
                        <div class="star-rating-box">
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star full-star"></i>
                            <i class="fas fa-star-half-alt half-star"></i>
                            <i class="far fa-star empty-star"></i>
                            <i class="far fa-star empty-star"></i>
                        </div>
                    </div>
                    <div class="product__rating-right">
                        <div class="post-views">
                            <span class="post-view-number">{{ $tablet[$key]->total_views }}</span>
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </div>
                        <div class="product__line"></div>
                        <div class="wishlist">
                            <span>
                                <a href="{{ route('add.to.wishlist', $tablet[$key]->id) }}">
                                    <i class="fal fa-heart"></i>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
                <a href="{{ route('product-detail.show', $tablet[$key]->id ) }}" class="product-name">
                    <h6 data-name="{{ $tablet[$key]->name }}">{{ $tablet[$key]->name }}</h6>
                </a>
                <a href="{{ route('product-detail.show', $tablet[$key]->id ) }}">
                    <div class="price-start">
                    <p>Giá:
                        <strong class="blurb-price-color">
                            {{-- <span>đ</span> --}}
                            <span class="min_price" data-min_price="{{ $maxPrice['tablet'] }}">
                                {{ $maxPrice['tablet'] }}
                            </span>
                            &nbsp;-&nbsp;
                            {{-- <span>₫ </span> --}}
                            <span class="max_price" data-max_price="{{ $minPrice['tablet'] }}">
                                {{ $minPrice['tablet'] }}
                            </span>
                        </strong>
                    </p>
                    </div>
                </a>
                <div class="product__meta-footer">
                    <div class="left__content">
                        <span>Danh mục:</span>
                            <i class="fas fa-store"></i>
                            @if (!empty($tablet[$key]->categories))
                                    <a
                                        href="{{ route('product-category.show', $tablet[$key]->categories->id) }}">{{ $tablet[$key]->categories->name }}</a>
                                @endif
                    </div>
                    <div class="compare-btn">
                        <span>
                            <a class="btn btn-primary btn-sm sl-button liked add-tablet" title="Compare" data-icon="fa fa-balance-scale"><i class="fa fa-balance-scale" aria-hidden="true"></i> </a>
                        </span>
                    </div>
                </div>
            </div>
        </figure>
    </article>
@endif
@endforeach
@endif



