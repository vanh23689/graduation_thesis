
<section class="amazon ebay">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="600">
                <h2 class="amazon__title ebay__title">
                    <span class="amazon__title-color">Thiết bị</span>
                    Điện tử
                </h2>
            </div>
            <div class="col-md-12 px-2">
                <div class="amazon__line">
                    <div class="amazon__line-small left"></div>
                    <div class="amazon__line-small"></div>
                </div>
            </div>
            <div class="amazon__product owl-carousel owl-theme">
                @include('site.pages.home.electronic_device.laptop')
                @include('site.pages.home.electronic_device.mobile')
                @include('site.pages.home.electronic_device.tablet')
                @include('site.pages.home.electronic_device.pc')
            </div>
        </div>
    </div>
</section>
@include('site.layouts.others.loading')
@yield('footer_script')
<script>
    $(document).ready(function() {
        var arr = [];

        $(".add-laptop").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            console.log("🚀 ~ file: index.blade.php ~ line 57 ~ res", res)
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-mobile").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });

        });

        $(".add-pc").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {

                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            console.log("🚀 ~ file: index.blade.php ~ line 137 ~ res", res)
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-tablet").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {

                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            console.log("🚀 ~ file: index.blade.php ~ line 176 ~ res", res)
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-wishlist").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-wishlist/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name"),
                    website: ele.parents('.product__meta-bottom').find('a.product-name').attr("href")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/wishlist/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-wishlist').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-laptop").on('click', function(e) {
            e.preventDefault();

            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#product_code").text(data.data[0]?.offices.product_code)
                        $("#order_status").text(data.data[0]?.offices.order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-mobile").on('click', function(e) {
            e.preventDefault();

            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.get(url, function(data){
                arr = data.category
                $.each(arr, function (index, val){
                    $(".product__slide-market").append(
                    '<li>' +
                        '<div class="product__logo">' +
                            '<img src=' + val.img+ ' ' +'alt='+ val.name +'>' +
                        '</div>'+
                        '<div class="product__price mx-auto p">' +
                            '<span class="product__price-min">' +
                            val.current_price +
                            '</span>' +
                            '<span class="product__price-">' +
                            'VND' +
                            '</span>'+
                        '</div>'+
                        '<div class="product__price-active p">'+
                            val.order_status +
                        '</div>'+
                        '<a href='+ val.website + '>' +
                            '<span class="btn btn-secondary im-btn-1">' +
                                'Tìm hiểu' +
                            '</span>'+
                        '</a>'+
                    '</li>');
                })


                $("#title").text(data.data[0]?.name)
                $("#product_code").text(data.data[0]?.telecommunitions.product_code)
                $("#order_status").text(data.data[0]?.telecommunitions.order_status)
                $(".slider-modal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");

            });


        });

        $(".view-pc").on('click', function(e) {
            e.preventDefault();

            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#product_code").text(data.data[0]?.offices.product_code)
                        $("#order_status").text(data.data[0]?.offices.order_status)
                        $(".slider-modal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-tablet").on('click', function(e) {
            e.preventDefault();

            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    var arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#product_code").text(data.data[0]?.offices.product_code)
                        $("#order_status").text(data.data[0]?.offices.order_status)
                        $(".slider-modal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");

                    }


            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $("button.btn.btn-primary.btn-sm").click(function(){
            $(".product__slide-market li").remove();
        });

    })

</script>

