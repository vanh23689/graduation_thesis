@extends('site.layouts.master')
@section('title'){{'Trang chủ'}}@endsection
@section('content')
<!-- menus start -->
@include('site.layouts.menus.index')
<!-- menus end -->
<!-- banner start -->
@include('site.pages.home.banner.index')
<!-- banner end -->
<!-- service start -->
@include('site.pages.home.services.index')
<!-- service end -->
<!-- alie_xpress start -->
@include('site.pages.home.household_appliances.index')
<!-- alie_xpress end -->
<!-- ebay start -->
@include('site.pages.home.electronic_device.index')
<!-- ebay end -->
<!-- ebay_aliexpress start -->
@include('site.pages.home.ebay_aliexpress.index')
<!-- ebay_aliexpress end -->
<!-- weekly start -->
@include('site.pages.home.weekly.index')
<!-- weekly end -->
<!-- purchase start -->
@include('site.pages.home.purchase.index')
<!-- purchase end -->
@endsection
