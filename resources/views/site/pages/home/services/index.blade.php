<section class="service">
    <div class="container">
      <div class="row">
        <div class="col-6 col-sm-12 col-md-6 col-lg-3" data-aos="zoom-in" data-aos-duration="600">
          <div class="service__box">
            <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/4-5.png" alt="Social Business">
            <div class="service__box-text d-flex align-items-center">
              <h5>Kinh doanh xã hội</h5>
            </div>
            <div class="service__box-hover">
              <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/social-bg-4.png" alt="Social Business">
            </div>
          </div>
        </div>
        <div class="col-6 col-sm-12 col-md-6 col-lg-3" data-aos="zoom-in" data-aos-duration="900">
          <div class="service__box">
            <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/2-1-4.png" alt="Social Business">
            <div class="service__box-text d-flex align-items-center">
              <h5>So sánh giá</h5>
            </div>
            <div class="service__box-hover">
              <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/price-bg-4.png" alt="Social Business">
            </div>
          </div>
        </div>
        <div class="col-6 col-sm-12 col-md-6 col-lg-3" data-aos="zoom-in" data-aos-duration="1200">
          <div class="service__box">
            <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/3-6.png" alt="Social Business">
            <div class="service__box-text d-flex align-items-center">
              <h5>Cửa hàng đa đồ dùng</h5>
            </div>
            <div class="service__box-hover">
              <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/multivendor-bg-4.png" alt="Social Business">
            </div>
          </div>
        </div>
        <div class="col-6 col-sm-12 col-md-6 col-lg-3" data-aos="zoom-in" data-aos-duration="1500">
          <div class="service__box">
            <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/2-1-4.png" alt="Social Business">
            <div class="service__box-text d-flex align-items-center">
              <h5>Review sản phẩm</h5>
            </div>
            <div class="service__box-hover">
              <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/price-bg-4.png" alt="Social Business">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
