<section class="banner" style="background: url('https://themeim.com/wp/blurb/wp-content/uploads/2019/05/BG-2-4.jpg');">
    <div class="banner__box owl-carousel owl-theme">
      <div class="banner__box-slide item" >
        <div class="slide__item">
          <div class="container">
            <div class="slide__item-wrap" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1200">
              <div class="slide__img ml-auto order-sm-2">
                <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/1-7.png" alt="">
              </div>
              <div class="slide__text" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1200">
                <h1 class="slide__text-title">
                  <span>Surface <mark>Laptop</mark></span>

                  Blurb Core i5
                </h1>
                <div class="slide__text-content">
                  Multivendor Store
                </div>
                <a class="slide__text-link" href="" data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">Go to store</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="banner__box-slide item" >
        <div class="slide__item">
          <div class="container">
            <div class="slide__item-wrap">
              <div class="slide__img ml-auto order-sm-2">
                <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/1-7.png" alt="">
              </div>
              <div class="slide__text">
                <h1 class="slide__text-title">
                  <span>Surface <mark>Laptop</mark></span>

                  Blurb Core i5
                </h1>
                <div class="slide__text-content">
                  Multivendor Store
                </div>
                <a class="slide__text-link" href="">Go to store</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
