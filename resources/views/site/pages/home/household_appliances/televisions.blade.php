@if (!empty($televisions))
    @foreach ($televisions as $key => $value)
        @if ($key == 0)
            <article class="col-12" data-aos="zoom-out" data-aos-delay="100" data-aos-duration="700">
                <figure class="amazon__product-box">
                    <input type="hidden" data-id="{{ $televisions[$key]->id }}">
                    <div class="product__meta-top">
                        <a class="product__img" href="{{ route('product-detail.show', $televisions[$key]->id) }}"
                            data-img="{{ $televisions[$key]->img }}">
                            <img src="{{ $televisions[$key]->img }}" alt="">
                        </a>
                        <div class="product__view compare-btn">
                            <button class="btn btn-primary btn-sm view-televisions" data-toggle="modal"
                                data-target="#electronicDevice">
                                <i class="fas fa-eye"></i>
                                Xem nhanh
                            </button>
                        </div>
                    </div>
                    <div class="product__meta-bottom">
                        <div class="product__rating">
                            <div class="star-rating">
                                <div class="star-rating-box">
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star-half-alt half-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                </div>
                            </div>
                            <div class="product__rating-right">
                                <div class="post-views">
                                    <span class="post-view-number">{{ $televisions[$key]->total_views ?? null }}</span>
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </div>
                                <div class="product__line"></div>
                                <div class="wishlist">
                                    <span>
                                        <a href="{{ route('add.to.wishlist', $televisions[$key]->id) }}">
                                            <i class="fal fa-heart"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('product-detail.show', $televisions[$key]->id) }}" class="product-name">
                            <h6 data-name="{{ $televisions[$key]->name }}">{{ $televisions[$key]->name }}</h6>
                        </a>
                        <a href="{{ route('product-detail.show', $televisions[$key]->id) }}">
                            <div class="price-start">
                                <p>Giá
                                    <strong class="blurb-price-color">
                                        {{-- <span>đ</span> --}}
                                        <span class="min_price" data-min_price="{{ $minPrice['televisions'] }}">
                                            {{ $minPrice['televisions'] }}
                                        </span>
                                        &nbsp;-&nbsp;
                                        {{-- <span>₫ </span> --}}
                                        <span class="max_price" data-max_price="{{ $maxPrice['televisions'] }}">
                                            {{ $maxPrice['televisions'] }}
                                        </span>
                                    </strong>
                                </p>
                            </div>
                        </a>
                        <div class="product__meta-footer">
                            <div class="left__content">
                                <span>Danh mục:</span>
                                <i class="fas fa-store"></i>
                                @if (!empty($televisions[$key]->categories))
                                    <a
                                        href="{{ route('product-category.show', $televisions[$key]->categories->id) }}">{{ $televisions[$key]->categories->name }}
                                    </a>
                                @endif

                            </div>

                            <div class="compare-btn">
                                <span>
                                    <a class="btn btn-primary btn-sm sl-button liked add-televisions" title="Compare"
                                        data-icon="fa fa-balance-scale"><i class="fa fa-balance-scale"
                                            aria-hidden="true"></i> </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </figure>
            </article>
        @endif
    @endforeach
@endif
