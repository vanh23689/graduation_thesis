@if (!empty($airConditioner))
    @foreach ($airConditioner as $key => $value)
        @if ($key == 0)
            <article class="col-12" data-aos="zoom-out" data-aos-delay="100" data-aos-duration="1300">
                <figure class="amazon__product-box">
                    <input type="hidden" data-id="{{ $airConditioner[$key]->id }}">
                    <div class="product__meta-top">
                        <a class="product__img" href="{{ route('product-detail.show', $airConditioner[$key]->id) }}"
                            data-img="{{ $airConditioner[$key]->img }}">
                            <img src="{{ $airConditioner[$key]->img }}" alt="">
                        </a>
                        <div class="product__view compare-btn">
                            <button class="btn btn-primary btn-sm view-airConditioner" data-toggle="modal"
                                data-target="#electronicDevice">
                                <i class="fas fa-eye"></i>
                                Xem nhanh
                            </button>
                        </div>
                    </div>
                    <div class="product__meta-bottom">
                        <div class="product__rating">
                            <div class="star-rating">
                                <div class="star-rating-box">
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star-half-alt half-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                </div>
                            </div>
                            <div class="product__rating-right">
                                <div class="post-views">
                                    <span
                                        class="post-view-number">{{ $airConditioner[$key]->total_views ?? null }}</span>
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </div>
                                <div class="product__line"></div>
                                <div class="wishlist">
                                    <span>
                                        <a href="{{ route('add.to.wishlist', $airConditioner[$key]->id) }}">
                                            <i class="fal fa-heart"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('product-detail.show', $airConditioner[$key]->id) }}" class="product-name">
                            <h6 data-name="{{ $airConditioner[$key]->name }}">{{ $airConditioner[$key]->name }}</h6>
                        </a>
                        <a href="{{ route('product-detail.show', $airConditioner[$key]->id) }}">
                            <div class="price-start">
                                <p>Giá
                                    <strong class="blurb-price-color">
                                        {{-- <span>đ</span> --}}
                                        <span class="min_price" data-min_price="{{ $minPrice['airConditioner'] }}">
                                            {{ $minPrice['airConditioner'] }}
                                        </span>
                                        &nbsp;-&nbsp;
                                        {{-- <span>₫ </span> --}}
                                        <span class="max_price" data-max_price="{{ $maxPrice['airConditioner'] }}">
                                            {{ $maxPrice['airConditioner'] }}
                                        </span>
                                    </strong>
                                </p>
                            </div>
                        </a>
                        <div class="product__meta-footer">
                            <div class="left__content">
                                <span>Danh mục:</span>
                                <i class="fas fa-store"></i>
                                <a
                                    href="{{ route('product-category.show', $airConditioner[$key]->categories->id) }}">{{ $airConditioner[$key]->categories->name }}</a>
                            </div>

                            <div class="compare-btn">
                                <span>
                                    <a class="btn btn-primary btn-sm sl-button liked add-airConditioner" title="Compare"
                                        data-icon="fa fa-balance-scale"><i class="fa fa-balance-scale"
                                            aria-hidden="true"></i> </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </figure>
            </article>
        @endif
    @endforeach
@endif
