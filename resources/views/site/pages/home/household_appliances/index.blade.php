<section class="ali_express amazon">
    <div class="container">
        <div class="row">
          <div class="col-md-12" data-aos="fade-up" data-aos-delay="100" data-aos-duration="600">
            <h2 class="amazon__title">
              <span class="amazon__title-color">Thiết bị </span>
              Trong gia đình
            </h2>
          </div>
          <div class="col-md-12 px-2">
              <div class="amazon__line">
                  <div class="amazon__line-small"></div>
                </div>
          </div>
          <div class="amazon__product owl-carousel owl-theme">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @include('site.pages.home.household_appliances.tivi')
                @include('site.pages.home.household_appliances.televisions')
                @include('site.pages.home.household_appliances.cooker')
                @include('site.pages.home.household_appliances.air_conditioner')
          </div>
        </div>
    </div>
</section>
@yield('footer_script')
<script>
    jQuery(document).ready(function($){
        var arr = [];
        var ele = $(this)
        $(".add-airConditioner").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-cooker").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-tivi").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".add-televisions").on('click', function (e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-airConditioner").on('click', function(e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })

                        $("#title").text(data.data[0]?.name)
                        $("#see_at_page").html('<a href="'+ data.data[0]?.website +'">'+ data.data[0]?.website +'</a> ')
                        $("#product_code").text(data.data[0]?.civils.product_code)
                        $("#order_status").text(data.data[0]?.civils.order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                        $(".product__slide-evaluate ul li").find("div.p span").text(data.data[0]?.total_views)
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-cooker").on('click', function(e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#see_at_page").html('<a href="'+ data.data[0]?.website +'">'+ data.data[0]?.website +'</a> ')
                        $("#product_code").text(data.data[0]?.civils.product_code)
                        $("#order_status").text(data.data[0]?.civils.order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                        $(".product__slide-evaluate ul li").find("div.p span").text(data.data[0]?.total_views)
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-tivi").on('click', function(e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#see_at_page").html('<a href="'+ data.data[0]?.website +'">'+ data.data[0]?.website +'</a> ')
                        $("#product_code").text(data.data[0]?.civils.product_code)
                        $("#order_status").text(data.data[0]?.civils.order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                        $(".product__slide-evaluate ul li").find("div.p span").text(data.data[0]?.total_views)
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-televisions").on('click', function(e) {
            e.preventDefault();

            let ele = $(this)
            let id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))
            let url = 'show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    arr = data.category
                    $.each(arr, function (index, val){
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="Presell">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                // 'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a href='+ val.website + '>' +
                                '<span class="btn btn-secondary im-btn-1">' +
                                    'Tìm hiểu' +
                                '</span>'+
                            '</a>'+
                        '</li>');
                        })
                        $("#title").text(data.data[0]?.name)
                        $("#see_at_page").html('<a href="'+ data.data[0]?.website +'">'+ data.data[0]?.website +'</a> ')
                        $("#product_code").text(data.data[0]?.civils.product_code)
                        $("#order_status").text(data.data[0]?.civils.order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");
                        $(".product__slide-evaluate ul li").find("div.p span").text(data.data[0]?.total_views)
                    }

            }).done(function(data) {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });
    })

</script>
