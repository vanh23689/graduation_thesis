<section class="amazon weekly">
    <div class="container">
       <div class="row">
          <div class="col-md-12">
             <div class="weekly__title" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1200">
                <h2 class="amazon__title ebay__title">
                   <span class="amazon__title-color">TOP</span>
                   tin tức hot trong tuần
                </h2>
                <h6 class="ebay__subtitle">
                    Liên kết, so sánh hiệu suất và tham gia, đó là sự lựa chọn thông minh cho khách hàng.
                </h6>
             </div>
          </div>
          <div class="col-sm-6 col-lg-4 mb-4">
             <figure class="weekly__content">
                <div class="weekly__content-top">
                   <a class="weekly__img" href="#">
                   <img src=" https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/16-3-p2zq8jmx6us1evhczh5d3btc41eldgiegdaqzjwko0.jpg " alt="">
                   </a>
                </div>
                <figcaption class="weekly__content-caption">
                   <div class="weekly__info">
                      <ul>
                         <li>
                            <span>By</span>
                            -
                            <a href="">Admin</a>
                         </li>
                         <li>
                            <span>Views </span>
                            824
                         </li>
                         <li>
                            <span>Comments - </span>
                            <a href="" class="comments-link">0</a>
                         </li>
                      </ul>
                   </div>
                   <a href="" class="weekly__blog">
                   EDD login plugin get most popular for online sell
                   </a>
                   <div class="weekly__description">
                      Digital download plugin best for your application level solution to get
                      <a class="btn btn-link" href="https://themeim.com/wp/blurb/edd-login-plugin-get-most-popular/">
                      [. . .]
                      </a>
                   </div>
                </figcaption>
             </figure>
          </div>
          <div class="col-sm-6 col-lg-4 mb-4">
             <figure class="weekly__content">
                <div class="weekly__content-top">
                   <a class="weekly__img" href="#">
                   <img src=" https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/16-3-p2zq8jmx6us1evhczh5d3btc41eldgiegdaqzjwko0.jpg " alt="">
                   </a>
                </div>
                <figcaption class="weekly__content-caption">
                   <div class="weekly__info">
                      <ul>
                         <li>
                            <span>By</span>
                            -
                            <a href="">Admin</a>
                         </li>
                         <li>
                            <span>Views </span>
                            824
                         </li>
                         <li>
                            <span>Comments - </span>
                            <a href="" class="comments-link">0</a>
                         </li>
                      </ul>
                   </div>
                   <a href="" class="weekly__blog">
                   EDD login plugin get most popular for online sell
                   </a>
                   <div class="weekly__description">
                      Digital download plugin best for your application level solution to get
                      <a class="btn btn-link" href="https://themeim.com/wp/blurb/edd-login-plugin-get-most-popular/">
                      [. . .]
                      </a>
                   </div>
                </figcaption>
             </figure>
          </div>
          <div class="col-sm-6 col-lg-4">
             <figure class="weekly__content">
                <div class="weekly__content-top">
                   <a class="weekly__img" href="#">
                   <img src=" https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/16-3-p2zq8jmx6us1evhczh5d3btc41eldgiegdaqzjwko0.jpg " alt="">
                   </a>
                </div>
                <figcaption class="weekly__content-caption">
                   <div class="weekly__info">
                      <ul>
                         <li>
                            <span>By</span>
                            -
                            <a href="">Admin</a>
                         </li>
                         <li>
                            <span>Views </span>
                            824
                         </li>
                         <li>
                            <span>Comments - </span>
                            <a href="" class="comments-link">0</a>
                         </li>
                      </ul>
                   </div>
                   <a href="" class="weekly__blog">
                   EDD login plugin get most popular for online sell
                   </a>
                   <div class="weekly__description">
                      Digital download plugin best for your application level solution to get
                      <a class="btn btn-link" href="https://themeim.com/wp/blurb/edd-login-plugin-get-most-popular/">
                      [. . .]
                      </a>
                   </div>
                </figcaption>
             </figure>
          </div>
       </div>
    </div>
 </section>
