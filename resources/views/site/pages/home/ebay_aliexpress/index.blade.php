<div class="amazon ebay ali_express">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1200">
                <h2 class="amazon__title ebay__title">
                    <span class="amazon__title-color">TOP</span>
                    khách hàng tin dùng
                </h2>
                <h6 class="ebay__subtitle">
                    Liên kết, so sánh hiệu suất và tham gia, đó là sự lựa chọn thông minh cho khách hàng.
                </h6>
            </div>
            <div class="col-md-6">
                <div class="ali_express__content">
                    <div class="ali_express__content-link owl-carousel owl-theme">
                        <div class="ali_express__box">
                            <img src="https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/slider1-p2zq8neaoi600sbg8w4g52mz7b2ia2ezcqy6guts5k.jpg"
                                alt="">
                            <div class="ali_express__content-user">
                                <div class="user_img">
                                    <img width="45" height="45"
                                        src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/1.jpg"
                                        class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
                                </div>
                                <div class="user_name">
                                    <a class="im-link" href="#">
                                        <span class="name">ThemeIM</span>
                                    </a>
                                    <div class="role">TeamLeader</div>
                                </div>
                                <div class="star-rating">
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star full-star"></i>
                                    <i class="fas fa-star-half-alt half-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                    <i class="far fa-star empty-star"></i>
                                </div>
                            </div>
                            <div class="ali_express__content-description">
                                Tôi tin tưởng và thường xuyên dùng website này
                            </div>
                        </div>
                    </div>
                    {{-- <div class="ali_express__box">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/elementor/thumbs/slider2-p2zq8neaoi600sbg8w4g52mz7b2ia2ezcqy6guts5k.jpg"
                            alt="">
                    </div> --}}
                </div>
            </div>
            <div class="col-md-6">
                <div class="ali_express__video">
                    <iframe width="420" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</div>
