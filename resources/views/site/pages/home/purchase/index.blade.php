<section class="purchase">
    <div class="container">
      <div class="row">
        <div class="purchase__step" >
          <div class="purchase__step-img">
            <img width="46" height="46" src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/compare.png" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
          </div>
          <div class="purchase__step-content">
            <h3>Lets Compare</h3>
            <p>Chose your product with price comparisons.</p>
          </div>
          <div class="purchase__step-arow">
            <img src="https://themeim.com/wp/blurb/wp-content/themes/blurb/assets/images/details-img/angle2.png" alt="angle">
          </div>
        </div>

        <div class="purchase__step" >
          <div class="purchase__step-img">
            <img width="46" height="46" src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/review.png" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
          </div>
          <div class="purchase__step-content">
            <h3>Take Review</h3>
            <p>Check your selected product review</p>
          </div>
          <div class="purchase__step-arow">
            <img src="https://themeim.com/wp/blurb/wp-content/themes/blurb/assets/images/details-img/angle2.png" alt="angle">
          </div>
        </div>

        <div class="purchase__step" >
          <div class="purchase__step-img">
            <img width="44" height="44" src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/vendor.png" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
          </div>
          <div class="purchase__step-content">
            <h3>Multi-Vendor</h3>
            <p>Check your product from vendor store.</p>
          </div>
          <div class="purchase__step-arow">
            <img src="https://themeim.com/wp/blurb/wp-content/themes/blurb/assets/images/details-img/angle2.png" alt="angle">
          </div>
        </div>

        <div class="purchase__step step-active" >
          <div class="col-12 col-sm-8 purchase__step-content">
            <h3>Enjoy Result</h3>
          </div>
          <div class="purchase__step-img">
            <img width="60" height="60" src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/icon.png" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
          </div>
        </div>
      </div>
    </div>
  </section>
