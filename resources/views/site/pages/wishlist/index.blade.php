@extends('site.layouts.master')
@section('title'){{'Yêu thích'}}@endsection
@section('content')
<section class="wishlist blurb-page-container blog-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="wishlist__product">
                    <table class="col-md-12 p0 table table-responsive">
                        <thead>
                            <tr class="wishlist__product-title">
                                  <th class="text-center">Xóa</th>
                                  <th class="text-center">Hình ảnh</th>
                                  <th class="text-center">Tên sản phẩm</th>
                                  <th class="text-center">Ratings</th>
                                  <th class="text-center">Khoảng giá</th>
                                  <th class="text-center">Khả dụng</th>
                                  <th class="text-center">Link</th>
                            </tr>
                          </thead>
                        <tbody id="wishlist">
                            @if(!empty(\Illuminate\Support\Facades\Session::get('wishlist')))
                                @foreach(\Illuminate\Support\Facades\Session::get('wishlist') as $key => $value)

                                    <tr>
                                        <td class="text-center remove-icon">
                                            <div class="vertical-center">
                                                <a title="Remove" class="item__box-icon"><i class="fa fa-times" aria-hidden="true" data-id="{{ $value['id'] }}"></i></a>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <img src="{{ $value['image'] }}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy"
                                         sizes="(max-width: 700px) 100vw, 700px">	</td>
                                        <td class="text-center">
                                            <div class="vertical-center">
                                                <p>{{ $value['name'] }}</p>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="vertical-center">
                                                <div class="wishlist-ratings">
                                                    <strong>2.80</strong>
                                                </div>
                                                <div class="star-rating mt-0" ><i class="fa fa-star full-star" aria-hidden="true"></i><i class="fa fa-star full-star" aria-hidden="true"></i><i class="fa fa-star-half-o half-star" aria-hidden="true"></i><i class="fa fa-star-o empty-star" aria-hidden="true"></i><i class="fa fa-star-o empty-star" aria-hidden="true"></i></div>            <span class="product-ratings-text">2 Ratings</span>		</div>
                                        </td>
                                        <td class="text-center">
                                            <div class="vertical-center">
                                                <div class="wishlist-price">
                                                    <p>
                                                        {{-- <span class="blurb-currency-symbol">$</span> --}}
                                                        <span class="blurb-price-min">{{ $value['max_price'] }}</span> -
                                                        {{-- <span class="blurb-currency-symbol">$</span> --}}
                                                        <span class="blurb-price-max">{{ $value['min_price'] }}</span></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="vertical-center">
                                                <div class="green-color"><i class="fa fa-check" aria-hidden="true"></i> </div>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="vertical-center">
                                                <a href="{{ $value['website'] }}" class="btn btn-primary select-market-btn">
                                                    Chi tiết <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@yield('footer_script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).on('click', '.item__box-icon i.fa.fa-times', function (e){
    console.log("🚀 ~ file: index.blade.php ~ line 101 ~ e", e)

        e.preventDefault();
        var id = $(this).attr('data-id')
        $.ajax({
            type: 'DELETE',
            url: '/site/remove-from-wishlist',
            data: {
                id: id
            },
            success: function(data){
                $("#wishlist").html(data);
            }
        }).done(function() {
            setTimeout(function(){
                $("#overlay").fadeOut(300), 500;
            }, 500);
        });
    });
</script>
