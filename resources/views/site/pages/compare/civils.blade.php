{{-- civils --}}
@if (!empty($prod_civil))
        <li>
            <div class="scpecifiction__list-info">Dung tích </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Khử mùi </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Bộ xử lí </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tổng công suất đỉnh </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Số loa </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Chia sẻ thông minh </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Kích thước chân đế </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Trọng lượng khung </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Số cửa </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Công suất tủ đông </div>

        </li>

        <li>
            <div class="scpecifiction__list-info">Dung tích ngăn đá </div>

        <li>
            <div class="scpecifiction__list-info">Tuyết </div>

        <li>
            <div class="scpecifiction__list-info">Công suất tủ lạnh </div>

        <li>
            <div class="scpecifiction__list-info">Khay đựng </div>

        <li>
            <div class="scpecifiction__list-info">Vật liệu bên ngoài </div>

        <li>
            <div class="scpecifiction__list-info">Máy làm đá tự động </div>

        <li>
            <div class="scpecifiction__list-info">Lấy nước bên ngoài </div>

        <li>
            <div class="scpecifiction__list-info">Chuông cửa </div>

        <li>
            <div class="scpecifiction__list-info">Vật liệu </div>

        <li>
            <div class="scpecifiction__list-info">Chế độ </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Khay hấp </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tốc độ làm mát tối thiểu </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tốc độ làm mát tối đa </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tốc độ làm mát trung bình </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Làm lạnh nhanh </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tự đoán lỗi </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Tự khởi động lại </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Đuổi muỗi </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Gas </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Hút ẩm </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Mức độ ồn </div>
        </li>

        <li>
            <div class="scpecifiction__list-info">Phạm vi có hiệu lưc </div>
        </li>

@endif
{{-- end civils --}}
