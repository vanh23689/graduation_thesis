@extends('site.layouts.master')
@section('title'){{'Yêu thích'}}@endsection
@section('content')
<!-- menus start -->
@include('site.layouts.menus.index')
<!-- menus end -->
<section class="compare">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="compare__products">
                    <ul>
                        <li class="compare__products-item">
                            <div class="compare__products-count">
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                <h6>Compare product</h6>
                                <p>4 products added</p>
                            </div>
                        </li>
                        <li>Giá tốt</li>
                        <div class="compare__products-heading">Tổng quan</div>
                        <li>Tính năng</li>
                        <li>Giá</li>
                        <li>Kích thước</li>
                        <li>Màn hình</li>
                        <li>Cân nặng</li>
                        <li>Chiều cao</li>
                        <li>Nhà sản xuất</li>
                        <li>Sản xuất năm</li>
                        <div class="compare__products-heading">Thông số kỹ thuật</div>
                        <li>Wifi</li>
                        <li>Bluetooth</li>
                        <li>Đồ họa</li>
                        <li>Công nghệ sạc</li>
                        <li>CPU</li>
                        <li>HDMI</li>
                        <li>Tai nghe</li>
                        <li>Địa điểm bảo hành</li>
                        <li>LCD</li>
                        <li>Mạng</li>
                        <li>RAM</li>
                        <li>Độ phân giải</li>
                        <li>Loa ngoài</li>
                        <li>Cảm ứng</li>
                        <li>USB</li>
                        <li>VGA</li>
                        <li>Voice</li>
                        <li>THời hạn bảo hành</li>
                        <li>Công suất</li>
                        @include('site.pages.compare.civils')
                    </ul>
                    <ul class="compare__products-carousel owl-carousel owl-theme">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@yield('footer_script')
<script>
    // $(".blurb-remove i").on('click', function (e){
    //     e.preventDefault();

    //     let id = $(this).attr('data-id')
    //     console.log("🚀 ~ file: index.blade.php ~ line 124 ~ id", id)
    //     $.ajax({
    //         type: 'DELETE',
    //         url: '{{ route('remove.from.product_to_compare') }}',
    //         data: {
    //             id: id
    //         },
    //         success: function(data){
    //             $('.compare__products-carousel').html(data)
    //         }
    //     }).done(function() {
    //         setTimeout(function(){
    //             $("#overlay").fadeOut(300), 500;
    //         }, 500);
    //     });
    // });
</script>
@endsection

