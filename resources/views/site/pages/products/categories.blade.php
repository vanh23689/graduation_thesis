@extends('site.layouts.master')
@section('title'){{'Danh mục sản phẩm'}}@endsection
@section('content')
<!-- menus start -->
@include('site.layouts.menus.index')
<!-- menus end -->
<!-- banner start -->
@include('site.pages.products.banner')
<!-- banner end -->
<main id="main">
    <section class="blog__section sidebar">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    @include('site.pages.products.categories.product')
                </div>
                <div class="col-md-4 col-lg-3">
                    @include('site.pages.products.categories.search')
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
