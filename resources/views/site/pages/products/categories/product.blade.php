<div class="dropdown filter px-4">
    <select class="form-select" aria-label="Default select example" name="sort" id="sort">
        <option selected>Xếp theo: </option>
        <option value="{{ url()->full() }}?sort_by=tang_dan">Giá tăng dần</option>
        <option value="{{ url()->full() }}?sort_by=giam_dan">Giá giảm dần</option>
        <option value="{{ url()->full() }}?sort_by=a_z">Tên từ A -> Z</option>
        <option value="{{ url()->full() }}?sort_by=z_a">Tên từ Z -> A</option>
    </select>
</div>
@foreach ($products as $product)
    <div class="col-md-4 col-sm-6 mt-4">
        <article class="col-12">
            <figure class="amazon__product-box">
                <input type="hidden" data-id="{{ $product->id }}">
                <div class="product__meta-top">
                    <a class="product__img" href="{{ route('product-detail.show', $product->id ) }}" data-img="{{ $product->img }}">
                            <img src="{{ $product->img ?? $product->images->first()->image_url ?? '' }}" alt="{{ $product->name }}">
                    </a>
                    <div class="product__view compare-btn">
                        <button class="btn btn-primary btn-sm view-laptop" data-toggle="modal" data-target="#electronicDevice">
                            <i class="fas fa-eye"></i>
                            Xem nhanh
                        </button>
                    </div>
                </div>
                <div class="product__meta-bottom">
                    <div class="product__rating">
                        <div class="star-rating m-0">
                            <div class="star-rating-box">
                                <i class="fas fa-star full-star"></i>
                                <i class="fas fa-star full-star"></i>
                                <i class="fas fa-star-half-alt half-star"></i>
                                <i class="far fa-star empty-star"></i>
                                <i class="far fa-star empty-star"></i>
                            </div>
                        </div>
                        <div class="product__rating-right">
                            <div class="post-views">
                                <span class="post-view-number">{{ $product->total_views }}</span>
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </div>
                            <div class="product__line"></div>
                            <div class="wishlist">
                                <span>
                                    <a href="{{ route('add.to.wishlist', $product->id) }}">
                                        <i class="fal fa-heart"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('product-detail.show', $product->id ) }}">
                        <h6 data-name="{{ $product->name }}">{{ $product->name }}</h6>
                    </a>
                    <a href="{{ route('product-detail.show', $product->id ) }}">
                        <div class="price-start">
                        <p>Giá
                            <strong class="blurb-price-color">
                                {{-- <span>đ</span> --}}
                                <span class="min_price" data-min_price="{{ $product->current_price }}">
                                    {{ $product->current_price ?? 0}}
                                </span>
                                &nbsp;-&nbsp;
                                {{-- <span>₫ </span> --}}
                                <span class="max_price" data-max_price="{{ $product->initial_price }}">
                                    {{ $product->initial_price ?? '  '}}
                                </span>
                            </strong>
                        </p>
                        </div>
                    </a>
                    <div class="product__meta-footer">
                        <div class="left__content">
                            <span>Danh mục:</span>
                            <i class="fas fa-store"></i>
                            {{-- <a href="{{ route('product-category.show', $product->categories->id) }}" data-category="{{ $product->categories->name }}">{{ $product->categories->name }}</a> --}}
                        </div>

                        <div class="compare-btn">
                        <span>
                            <a class="btn btn-primary btn-sm sl-button liked add-product" title="Compare"><i class="fa fa-balance-scale" aria-hidden="true"></i> </a>
                        </span>
                        </div>
                    </div>
                </div>
            </figure>
        </article>
    </div>
@endforeach
{{ $products->links('vendor.pagination.default') }}
@yield('footer_script')
<script>
    $(document).ready(function() {
        $(".add-product").on('click', function (e) {
            e.preventDefault();

            var ele = $(this)
            var id  = parseInt(ele.parents('.amazon__product-box').find('input').attr("data-id"))

            $.ajax({
                url: "/site/add-to-cart/" + id,
                enctype: 'multipart/form-data',
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    img: ele.parents('figure.amazon__product-box').find('.product__img').attr("data-img"),
                    min_price: ele.parents('.product__meta-bottom').find('.min_price').attr("data-min_price"),
                    max_price: ele.parents('.product__meta-bottom').find('.max_price').attr('data-max_price'),
                    name: ele.parents('.product__meta-bottom').find('h6').attr("data-name"),
                    category: ele.parents('.product__meta-footer').find('a').attr("data-category")
                },
                success: function (response) {
                    $.ajax({
                        url: '/site/cart/' + id,
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(res){
                            $('.count-item').html(res)
                        }
                    })

                }
            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $(".view-laptop").on('click', function(e) {
            e.preventDefault();
            var id = parseInt($(this).parents('.amazon__product-box').find('input').attr("data-id"));
            var url = '../show-modal/' + id

            $.ajax({
                type: 'GET',
                url: url,
                processData: false,
                success: function(data) {
                    var arr = data.category
                    let order_status = data.data[0]?.offices?.order_status || data.data[0]?.civils?.order_status || data.data[0]?.telecommunitions?.order_status || 'Chưa rõ'
                    $.each(arr, function (index, val){

                        if(typeof order_status == "undefined") {
                            val.order_status = 'Chưa rõ'
                        }else{
                            val.order_status = order_status
                        }
                        $(".product__slide-market").append('<li>' +
                            '<div class="product__logo">' +
                                '<img src=' + val.img+ ' ' +'alt="' + val.name +'">' +
                            '</div>'+
                            '<div class="product__price mx-auto p">' +
                                '<span class="product__price-min">' +
                                val.current_price +
                                '</span>' +
                                '<span class="product__price-">' +
                                'VND' +
                                '</span>'+
                            '</div>'+
                            '<div class="product__price-active p">'+
                                val.order_status +
                            '</div>'+
                            '<a class="btn btn-secondary .im-btn-1" href='+ val.website +'>'+
                                'Tìm hiểu'+
                            '</a>'+
                        '</li>');
                        })

                        $("#title").text(data.data[0]?.name)
                        $("#product_code").text(data.data[0]?.offices?.product_code || data.data[0]?.civils?.product_code || data.data[0]?.telecommunitions?.product_code)
                        $("#order_status").text( order_status)
                        $("#lightSliderModal").html("<li data-thumb="+ data.data[0]?.img + ' ' + "data-src="+ data.data[0]?.img + ">" + "<img src="+ data.data[0]?.img + " " + "alt=" + " " + data.data[0]?.name + " " + "/>" + "</li>");

                    }


            }).done(function() {
                setTimeout(function(){
                    $("#overlay").fadeOut(300), 500;
                }, 500);
            });
        });

        $("button.btn.btn-primary.btn-sm").click(function(){
            $(".product__slide-market li").remove();
        });

        $("#sort").on('change', function() {
            let url = $(this).val();
            if (url) {
                window.location = url
            }
            return false;
        })
    })

</script>

@yield('style')
<style>
    .filter {
        width: 30%;
        margin-left: auto;
    }
    .filter select{
        font-size: 14px;
    }
</style>
