{{-- telecommunitions --}}
@if (!is_null($category->telecommunitions))
    @if(!is_null($category->telecommunitions->chipset))
    <li>
    <div class="scpecifiction__list-info">Chipset :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->chipset ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->memory))
    <li>
    <div class="scpecifiction__list-info">Bộ nhớ :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->memory ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->before_camera))
    <li>
    <div class="scpecifiction__list-info">Camera trước :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->before_camera ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->before_video))
    <li>
    <div class="scpecifiction__list-info">Video trước :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->before_video ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->after_camera))
    <li>
    <div class="scpecifiction__list-info">Camera sau :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->after_camera ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->after_video))
    <li>
    <div class="scpecifiction__list-info">Video sau :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->after_video ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->sim))
    <li>
    <div class="scpecifiction__list-info">SIM :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->sim ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->flash_light))
    <li>
    <div class="scpecifiction__list-info">Đèn sáng :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->flash_light ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->fingerprint_sensor))
    <li>
    <div class="scpecifiction__list-info">Cảm biến vân tay :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->fingerprint_sensor ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->special_features))
    <li>
    <div class="scpecifiction__list-info">Tính năng đặc biệt :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->special_features ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->lan_standard))
    <li>
    <div class="scpecifiction__list-info">Tiêu chuẩn LAN :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->lan_standard ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->types_of_sensors))
    <li>
    <div class="scpecifiction__list-info">Các loại cảm biến :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->types_of_sensors ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->telecommunitions->speaker_phone))
    <li>
    <div class="scpecifiction__list-info">Loa điện thoại :</div>
    <div class="scpecifiction__list-info">{{ $category->telecommunitions->speaker_phone ?? ''}}</div>
    </li>
    @endif
@endif
{{-- end telecommunitions --}}


