{{-- offices --}}
@if(!empty($category->offices))
    @if(!is_null($category->offices->caching))
    <li>
    <div class="scpecifiction__list-info">Bộ nhớ đệm :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->caching ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->cpu_speed))
    <li>
    <div class="scpecifiction__list-info">Tốc độ cpu :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->cpu_speed ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->ram_capacity))
    <li>
    <div class="scpecifiction__list-info">Dung lượng ram :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->ram_capacity ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->bus_speed))
    <li>
    <div class="scpecifiction__list-info">Tốc độ BUS :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->bus_speed ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->hard_drive))
    <li>
    <div class="scpecifiction__list-info">Ổ cứng :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->hard_drive ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->longs))
    <li>
    <div class="scpecifiction__list-info">Chiều dài :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->longs ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->width))
    <li>
    <div class="scpecifiction__list-info">Chiều rộng :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->width ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->height))
    <li>
    <div class="scpecifiction__list-info">Chiều cao :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->height ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->camera))
    <li>
    <div class="scpecifiction__list-info">Camera :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->camera ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->connect))
    <li>
    <div class="scpecifiction__list-info">Kết nối :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->connect ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->lan_standard))
    <li>
    <div class="scpecifiction__list-info">Tiêu chuẩn LAN :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->lan_standard ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->serial_port))
    <li>
    <div class="scpecifiction__list-info">Cổng nối tiếp :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->serial_port ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->features))
    <li>
    <div class="scpecifiction__list-info">Tính năng khác :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->features ?? ''}}</div>
    </li>
    @endif
    @if(!is_null($category->offices->webcam))
    <li>
    <div class="scpecifiction__list-info">Webcam :</div>
    <div class="scpecifiction__list-info">{{ $category->offices->webcam ?? ''}}</div>
    </li>
    @endif
@endif
{{-- end offices --}}
