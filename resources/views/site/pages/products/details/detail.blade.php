@extends('site.layouts.master')
@section('title')
    {{ 'Chi tiết' }}
@endsection
@section('content')
    @include('site.pages.products.banner')
    <main id="main">
        <section class="blog__section product-details">
            <div class="container">
                <div class="row">
                    <article>
                        <div class="col-12 im-product-gallery-wrapper">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <ul id="lightSlider">
                                        @foreach ($product_images as $key => $image)
                                            <li data-thumb="{{ $image }}" data-src="{{ $image }}">
                                                <img id="zoom-{{ $key }}" data-zoom-image="{{ $image }}"
                                                    src="{{ $image }}" />
                                            </li>
                                        @endforeach

                                        {{-- <li data-thumb="https://www.w3schools.com/howto/img_forest.jpg" data-src="https://www.w3schools.com/howto/img_forest.jpg">
                                <img src="https://www.w3schools.com/howto/img_forest.jpg" />
                              </li> --}}
                                    </ul>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="product__slide">
                                        <h2 class="product__slide-title">
                                            {{ $product->name }}
                                        </h2>
                                        <div class="product__slide-evaluate d-sm-flex align-items-sm-center">
                                            <ul>
                                                <li>
                                                    <div class="star-rating">
                                                        <i class="fas fa-star full-star"></i>
                                                        <i class="fas fa-star full-star"></i>
                                                        <i class="fas fa-star-half-alt half-star"></i>
                                                        <i class="far fa-star empty-star"></i>
                                                        <i class="far fa-star empty-star"></i>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="p">
                                                        <span>2.0/5</span>
                                                        - 1 ratings
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="p"><span>{{ $product->total_views }}</span> Lượt xem
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="product__slide-wishlist ml-sm-auto mt-3 mt-sm-0">
                                                <span>
                                                    <a href="" class="wishlist__heart">
                                                        <i class="fas fa-heart"></i>
                                                    </a>
                                                </span>
                                                <span class="wishlist__compare">
                                                    <a href="">
                                                        <i class="far fa-balance-scale"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="product__slide-description">
                                            <li>- Mã sản phẩm:
                                                {{ $product->offices->product_code ?? ($product->civils->product_code ?? ($product->telecommunitions->product_code ?? '')) }}
                                            </li>
                                            <li>- Tình trạng:
                                                {{ $product->offices->order_status ?? ($product->civils->order_status ?? ($product->telecommunitions->order_status ?? '')) }}
                                            </li>
                                            <li>- Xem tại trang:
                                                <a href="{{ $product->website }}">
                                                    {{ $product->website ?? '' }}
                                                </a>
                                            </li>
                                        </div>
                                        <div class="product__slide-publicer">
                                            <div class="im-publicer">
                                                Người bán:
                                                <a class="url fn n"
                                                    href="https://themeim.com/wp/blurb/author/admin/">Admin</a>
                                            </div>
                                        </div>
                                        <ul class="product__slide-market">
                                            @foreach ($product_categories as $category)
                                                <li>
                                                    <div class="product__logo">
                                                        <img src="{{ $category->logo }}" alt="{{ $category->name }}">
                                                    </div>
                                                    <div class="product__price mx-auto p">
                                                        <div class="product__price-min">
                                                            {{ $category->current_price ? $category->current_price : $category->initial_price }}
                                                        </div>
                                                    </div>
                                                    <div class="product__price-active p">
                                                        {{ $category->order_status ? $category->order_status : 'Chưa xác định' }}
                                                        <span class="product__price-tooltip">
                                                            Click vào "Tìm hiểu" để xem chi tiết
                                                        </span>
                                                    </div>
                                                    <a href="{{ $category->website }}" class="btn btn-secondary .im-btn-1">
                                                        Tìm hiểu
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 im-product-details-tab-area">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="product__item" role="presentation">
                                    <button class="product__item-link active" id="pills-{{ $category->id }}-tab"
                                        data-bs-toggle="pill" data-bs-target="#pills-{{ $category->id }}" type="button"
                                        role="tab" aria-controls="pills-{{ $category->id }}" aria-selected="true">Mô
                                        tả</button>
                                </li>
                                <li class="product__item" role="presentation">
                                    <button class="product__item-link" id="pills-full-{{ $category->id }}-tab"
                                        data-bs-toggle="pill" data-bs-target="#pills-full-{{ $category->id }}"
                                        type="button" role="tab" aria-controls="pills-full-{{ $category->id }}"
                                        aria-selected="false">Thông số kỹ thuật</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-{{ $category->id }}" role="tabpanel"
                                    aria-labelledby="pills-{{ $category->id }}-tab">
                                    <h3 class="im-title">{{ $category->name }}</h3>
                                    <div class="p">{{ $category->description }}</div>
                                </div>
                                <div class="tab-pane fade" id="pills-full-{{ $category->id }}" role="tabpanel"
                                    aria-labelledby="pills-full-{{ $category->id }}-tab">
                                    <h3 class="im-title">Thông số kĩ thuật</h3>
                                    @include('site.pages.products.details.specifications')
                                </div>
                                <div class="tab-pane fade" id="pills-review" role="tabpanel"
                                    aria-labelledby="pills-review-tab">...</div>
                                <div class="tab-pane fade" id="pills-price-history" role="tabpanel"
                                    aria-labelledby="pills-price-history-tab">...</div>
                            </div>
                        </div>
                        <section class="related_items amazon">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="amazon__title">
                                        Sản phẩm liên quan
                                    </h2>
                                </div>
                                <div class="col-md-12 px-2">
                                    <div class="amazon__line">
                                        <div class="amazon__line-small"></div>
                                    </div>
                                </div>
                                <div class="amazon__product owl-carousel owl-theme">
                                    @foreach ($product_categories as $category)
                                        <article class="col-12">
                                            <figure class="amazon__product-box">
                                                <div class="product__meta-top">
                                                    <a class="product__img" href="#">
                                                        <img src="{{ $category->img }}" alt="">
                                                    </a>
                                                    <div class="product__view compare-btn">
                                                        <button class="btn btn-primary btn-sm" type="button"
                                                            data-toggle="modal" data-target="#exampleModalCenter">
                                                            <i class="fas fa-eye"></i>
                                                            Quick view
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="product__meta-bottom">
                                                    <div class="product__rating">
                                                        <div class="star-rating">
                                                            <div class="star-rating-box">
                                                                <i class="fas fa-star full-star"></i>
                                                                <i class="fas fa-star full-star"></i>
                                                                <i class="fas fa-star-half-alt half-star"></i>
                                                                <i class="far fa-star empty-star"></i>
                                                                <i class="far fa-star empty-star"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="https://themeim.com/wp/blurb/product/yamaha-yzf-r15-v1/">
                                                        <h6>{{ $category->name }}</h6>
                                                    </a>
                                                    <a href="https://themeim.com/wp/blurb/product/yamaha-yzf-r15-v1/">
                                                        <div class="price-start">
                                                            <p>Giá
                                                                <strong class="blurb-price-color">
                                                                    <span>{{ $category->initial_price ?? 0 }}</span>
                                                                    -
                                                                    <span>{{ $category->current_price }}</span>
                                                                </strong>
                                                            </p>
                                                        </div>
                                                    </a>
                                                    <div class="product__meta-footer">
                                                        <div class="left__content">
                                                            <span>Sold by:</span>
                                                            <i class="fas fa-store"></i>
                                                            <a href="https://themeim.com/wp/blurb/author/admin/">Admin</a>
                                                        </div>

                                                        <div class="compare-btn">
                                                            <span>
                                                                <a href="https://themeim.com/wp/blurb/wp-admin/admin-ajax.php?action=process_simple_like&amp;post_id=378&amp;nonce=a7ba7053d9&amp;type=compare&amp;disabled=true"
                                                                    class="btn btn-primary btn-sm sl-button sl-compare-378 liked"
                                                                    title="Remove Compare" data-icon="fa fa-balance-scale"
                                                                    data-counter="0"><i class="fa fa-balance-scale"
                                                                        aria-hidden="true"></i> </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </article>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </article>
                </div>
            </div>
        </section>
    </main>
@endsection

