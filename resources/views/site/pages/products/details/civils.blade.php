{{-- civils --}}
@if(!empty($category->civils))
@if(!is_null($category->civils->capacity))
<li>
   <div class="scpecifiction__list-info">Dung tích :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->capacity ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->deodorant))
<li>
   <div class="scpecifiction__list-info">Khử mùi :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->deodorant ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->processor))
<li>
   <div class="scpecifiction__list-info">Bộ xử lí :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->processor ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->total_peaker_power))
<li>
   <div class="scpecifiction__list-info">Tổng công suất đỉnh :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->total_peaker_power ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->number_of_speakers))
<li>
   <div class="scpecifiction__list-info">Số loa :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->number_of_speakers ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->smart_sharing))
<li>
   <div class="scpecifiction__list-info">Chia sẻ thông minh :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->smart_sharing ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->dimensions_with_stand))
<li>
   <div class="scpecifiction__list-info">Kích thước chân đế :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->dimensions_with_stand ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->bracket_weight))
<li>
   <div class="scpecifiction__list-info">Trọng lượng khung :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->bracket_weight ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->door_number))
<li>
   <div class="scpecifiction__list-info">Số cửa :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->door_number ?? ''}}</div>
</li>
@endif
@if(!empty($category->civils->freezer_capacity) || !empty($category->offices->capacity))
<li>
   <div class="scpecifiction__list-info">Công suất tủ đông :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->freezer_capacity ?? $category->offices->capacity ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->freezer_compartment_capacity))
<li>
   <div class="scpecifiction__list-info">Dung tích ngăn đá :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->freezer_compartment_capacity ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->snow))
<li>
   <div class="scpecifiction__list-info">Tuyết :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->snow ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->refrigerator_technology))
<li>
   <div class="scpecifiction__list-info">Công suất tủ lạnh :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->refrigerator_technology ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->tray_material))
<li>
   <div class="scpecifiction__list-info">Khay đựng :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->tray_material ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->outer_material))
<li>
   <div class="scpecifiction__list-info">Vật liệu bên ngoài :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->outer_material ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->automatic_ice_maker))
<li>
   <div class="scpecifiction__list-info">Máy làm đá tự động :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->automatic_ice_maker ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->get_outside_water))
<li>
   <div class="scpecifiction__list-info">Lấy nước bên ngoài :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->get_outside_water ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->door_bell))
<li>
   <div class="scpecifiction__list-info">Chuông cửa :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->door_bell ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->material))
<li>
   <div class="scpecifiction__list-info">Vật liệu :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->material ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->regime))
<li>
   <div class="scpecifiction__list-info">Chế độ :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->regime ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->steaming_tray))
<li>
   <div class="scpecifiction__list-info">Khay hấp :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->steaming_tray ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->minimum_cooling_rate))
<li>
   <div class="scpecifiction__list-info">Tốc độ làm mát tối thiểu :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->minimum_cooling_rate ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->maximum_cooling_speed))
<li>
   <div class="scpecifiction__list-info">Tốc độ làm mát tối đa :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->maximum_cooling_speed ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->average_cooling_speed))
<li>
   <div class="scpecifiction__list-info">Tốc độ làm mát trung bình :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->average_cooling_speed ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->rapid_cooling))
<li>
   <div class="scpecifiction__list-info">Làm lạnh nhanh :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->rapid_cooling ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->error_guessing))
<li>
   <div class="scpecifiction__list-info">Tự đoán lỗi :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->error_guessing ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->auto_restart))
<li>
   <div class="scpecifiction__list-info">Tự khởi động lại :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->auto_restart ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->mosquito_repellent))
<li>
   <div class="scpecifiction__list-info">Đuổi muỗi :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->mosquito_repellent ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->gas))
<li>
   <div class="scpecifiction__list-info">Gas :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->gas ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->hygroscopic))
<li>
   <div class="scpecifiction__list-info">Hút ẩm :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->hygroscopic ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->noise_level))
<li>
   <div class="scpecifiction__list-info">Mức độ ồn :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->noise_level ?? ''}}</div>
</li>
@endif
@if(!is_null($category->civils->effective_range))
<li>
   <div class="scpecifiction__list-info">Phạm vi có hiệu lưc :</div>
   <div class="scpecifiction__list-info">{{ $category->civils->effective_range ?? ''}}</div>
</li>
@endif
@endif
{{-- end civils --}}
