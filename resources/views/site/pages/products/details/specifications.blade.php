<ul class="scpecifiction__list">
    @if(!is_null($category->size))
    <li>
       <div class="scpecifiction__list-info">Kích thước :</div>
       <div class="scpecifiction__list-info">{{ $category->size }}</div>
    </li>
    @endif
    @if(!is_null($category->working_hours))
    <li>
       <div class="scpecifiction__list-info">Giờ làm việc :</div>
       <div class="scpecifiction__list-info">{{ $category->working_hours }}</div>
    </li>
    @endif
    @if(!is_null($category->insurance))
    <li>
       <div class="scpecifiction__list-info">Địa điểm bảo hành:</div>
       <div class="scpecifiction__list-info">{{ $category->insurance }}</div>
    </li>
    @endif
    @if(!is_null($category->warranty_period))
    <li>
       <div class="scpecifiction__list-info">Thời hạn bảo hành :</div>
       <div class="scpecifiction__list-info">{{ $category->warranty_period }}</div>
    </li>
    @endif
    @if(!is_null($category->weight))
    <li>
       <div class="scpecifiction__list-info">Cân nặng :</div>
       <div class="scpecifiction__list-info">{{ $category->weight }}</div>
    </li>
    @endif
    @if(!is_null($category->type))
    <li>
       <div class="scpecifiction__list-info">Loại :</div>
       <div class="scpecifiction__list-info">{{ $category->type }}</div>
    </li>
    @endif
    @if(!is_null($category->wattage))
    <li>
       <div class="scpecifiction__list-info">Công suất :</div>
       <div class="scpecifiction__list-info">{{ $category->wattage }}</div>
    </li>
    @endif
    @if(!is_null($category->screen))
    <li>
       <div class="scpecifiction__list-info">Màn hình :</div>
       <div class="scpecifiction__list-info">{{ $category->screen }}</div>
    </li>
    @endif
    @if(!is_null($category->cpu))
    <li>
       <div class="scpecifiction__list-info">CPU :</div>
       <div class="scpecifiction__list-info">{{ $category->cpu }}</div>
    </li>
    @endif
    @if(!is_null($category->lcd))
    <li>
       <div class="scpecifiction__list-info">LCD :</div>
       <div class="scpecifiction__list-info">{{ $category->lcd }}</div>
    </li>
    @endif
    @if(!is_null($category->ram))
    <li>
       <div class="scpecifiction__list-info">RAM :</div>
       <div class="scpecifiction__list-info">{{ $category->ram }}</div>
    </li>
    @endif
    @if(!is_null($category->nfc))
    <li>
       <div class="scpecifiction__list-info">NFC :</div>
       <div class="scpecifiction__list-info">{{ $category->nfc }}</div>
    </li>
    @endif
    @if(!is_null($category->vga))
    <li>
       <div class="scpecifiction__list-info">VGA :</div>
       <div class="scpecifiction__list-info">{{ $category->vga }}</div>
    </li>
    @endif
    @if(!is_null($category->touch))
    <li>
       <div class="scpecifiction__list-info">Cảm ứng :</div>
       <div class="scpecifiction__list-info">{{ $category->touch }}</div>
    </li>
    @endif
    @if(!is_null($category->operating_system))
    <li>
       <div class="scpecifiction__list-info">Hệ điều hành :</div>
       <div class="scpecifiction__list-info">{{ $category->operating_system }}</div>
    </li>
    @endif
    @if(!is_null($category->origin))
    <li>
       <div class="scpecifiction__list-info">Nguồn gốc :</div>
       <div class="scpecifiction__list-info">{{ $category->origin }}</div>
    </li>
    @endif
    @if(!is_null($category->hdmi))
    <li>
       <div class="scpecifiction__list-info">HDMI :</div>
       <div class="scpecifiction__list-info">{{ $category->hdmi }}</div>
    </li>
    @endif
    @if(!is_null($category->version))
    <li>
       <div class="scpecifiction__list-info">Phiên bản :</div>
       <div class="scpecifiction__list-info">{{ $category->version }}</div>
    </li>
    @endif
    @if(!is_null($category->wifi))
    <li>
       <div class="scpecifiction__list-info">WIFI :</div>
       <div class="scpecifiction__list-info">{{ $category->wifi }}</div>
    </li>
    @endif
    @if(!is_null($category->voice))
    <li>
       <div class="scpecifiction__list-info">Voice :</div>
       <div class="scpecifiction__list-info">{{ $category->voice }}</div>
    </li>
    @endif
    @if(!is_null($category->usb))
    <li>
       <div class="scpecifiction__list-info">USB :</div>
       <div class="scpecifiction__list-info">{{ $category->usb }}</div>
    </li>
    @endif
    @if(!is_null($category->graphics))
    <li>
       <div class="scpecifiction__list-info">Đồ họa :</div>
       <div class="scpecifiction__list-info">{{ $category->graphics }}</div>
    </li>
    @endif
    @if(!is_null($category->resolution))
    <li>
       <div class="scpecifiction__list-info">Độ phân giải :</div>
       <div class="scpecifiction__list-info">{{ $category->resolution }}</div>
    </li>
    @endif
    @if(!is_null($category->network_support))
    <li>
       <div class="scpecifiction__list-info">Hỗ trợ mạng :</div>
       <div class="scpecifiction__list-info">{{ $category->network_support }}</div>
    </li>
    @endif
    @if(!is_null($category->charging_technology))
    <li>
       <div class="scpecifiction__list-info">Công nghệ sạc :</div>
       <div class="scpecifiction__list-info">{{ $category->charging_technology }}</div>
    </li>
    @endif
    @if(!is_null($category->image_processing))
    <li>
       <div class="scpecifiction__list-info">Xử lí hình ảnh :</div>
       <div class="scpecifiction__list-info">{{ $category->image_processing }}</div>
    </li>
    @endif
    @if(!is_null($category->audio))
    <li>
       <div class="scpecifiction__list-info">Audio :</div>
       <div class="scpecifiction__list-info">{{ $category->audio }}</div>
    </li>
    @endif
    @if(!is_null($category->speakerphone))
    <li>
       <div class="scpecifiction__list-info">Loa ngoài :</div>
       <div class="scpecifiction__list-info">{{ $category->speakerphone }}</div>
    </li>
    @endif
    @if(!is_null($category->headphone))
    <li>
       <div class="scpecifiction__list-info">Tai nghe :</div>
       <div class="scpecifiction__list-info">{{ $category->headphone }}</div>
    </li>
    @endif
    <li>
       <div class="scpecifiction__list-info">Tình trạng :</div>
       @if(!empty($category->order_status))
       <div class="scpecifiction__list-info">{{ $category->order_status }}</div>
       @else
       <div class="scpecifiction__list-info">Vui lòng xem tại website của sản phẩm</div>
       @endif
    </li>
    @if(!is_null($category->producer))
    <li>
       <div class="scpecifiction__list-info">Nhà sản xuất :</div>
       <div class="scpecifiction__list-info">{{ $category->producer }}</div>
    </li>
    @endif
    @include('site.pages.products.details.civils')
    @include('site.pages.products.details.offices')
    @include('site.pages.products.details.telecommunications')
</ul>
