<section class="banner" style="background: url('https://themeim.com/wp/blurb/wp-content/uploads/2019/05/BG-2-4.jpg');" data-aos="fade-up">
    <div class="product-detail">
      <div class="banner__box-slide" >
        <div class="slide__item">
          <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slide__item-wrap">
                        <div class="slide__text">
                            <h1 class="slide__text-title">
                                Danh mục sản phẩm: {{ $category_name }}
                            </h1>
                        </div>
                        <div class="slide__text-content" style="order: 2;">
                            <a href="{{ route('home.index') }}">Trang chủ</a>
                            &nbsp;
                            <span class="sep">/</span>
                            &nbsp;
                            <span class="crnt">{{ $category_name }}</span>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
