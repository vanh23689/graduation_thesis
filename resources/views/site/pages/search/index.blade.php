<form action="" style="height:100% ">
    @csrf
    <div class="input-group center__bar-search">
        <input type="text" name="search" class="header__search-form fomr-control" id="search_name" name="search_name" value="" placeholder="Tìm kiếm...">
        <div id="productList">
            <br>
        </div>
        <span class="header__search-icon">
            <button type="submit">
                <i class="fas fa-search"></i>
            </button>
        </span>
    </div>
</form>
@yield('footer_script')
<script>
    $(document).ready(function(e){
        $('#search_name').keyup(function(){    // bắt sự kiện keyup khi người dùng gõ từ khóa tim kiếm
            let query = $(this).val();

            if(query != ''){

                let _token = $('input[name="_token"]').val(); // token để mã hóa dữ liệu
                $.ajax({
                    url: "{{ route('search') }}", // đường dẫn khi gửi dữ liệu đi 'search' là tên route mình đặt bạn mở route lên xem là hiểu nó là cái gì
                    method: "POST", // phương thức gửi dữ liệu.
                    data:
                        {   query:query,
                            _token:_token
                        },
                    success: function(data){
                        $('#productList').fadeIn();
                        $('#productList').html(data); // nhận dữ liệu dạng html và gán vào cặp thẻ có id là productList

                        if(!query){
                            $('#productList').fadeOut();
                        }
                    },
                    complete: function(){
                        $('#overlay').css("visibility", "hidden");
                    }

                })
            }
        })

        $(document).on('click', 'body', function(){
            $("#productList").fadeOut();
        })

        $(document).on('click', '#overlay', function(){
            $("#overlay").fadeOut();
        })
    });
</script>
