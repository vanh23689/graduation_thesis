<!-- Modal -->
<input type="hidden" name="professor_id" />
<div class="modal fade" id="modalDisplay" tabindex="-1" role="dialog" aria-labelledby="modalDisplayTitle" aria-hidden="true">
    <div class="modal-dialog-centered container" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <ul id="lightSlider">
                <li data-thumb="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png" data-src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png" />
                </li>
                <li data-thumb="https://www.w3schools.com/howto/img_forest.jpg" data-src="https://www.w3schools.com/howto/img_forest.jpg">
                    <img src="https://www.w3schools.com/howto/img_forest.jpg" />
                </li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="product__slide">
                <h2 class="product__slide-title">
                    Asus max twenty six laptop
                </h2>
                <div class="product__slide-evaluate d-sm-flex align-items-sm-center">
                    <ul>
                    <li>
                        <div class="star-rating">
                        <i class="fas fa-star full-star"></i>
                        <i class="fas fa-star full-star"></i>
                        <i class="fas fa-star-half-alt half-star"></i>
                        <i class="far fa-star empty-star"></i>
                        <i class="far fa-star empty-star"></i>
                        </div>
                    </li>
                    <li>
                        <div class="p">
                        <span>2.0/5</span>
                        - 1 ratings
                        </div>
                    </li>
                    <li>
                        <div class="p"><span>1K+</span> Views</div>
                    </li>
                    </ul>
                    <div class="product__slide-wishlist ml-sm-auto mt-3 mt-sm-0">
                    <span>
                        <a href="" class="wishlist__heart">
                        <i class="fas fa-heart"></i>
                        </a>
                    </span>
                    <span class="wishlist__compare">
                        <a href="">
                        <i class="far fa-balance-scale"></i>
                        </a>
                    </span>
                    </div>
                </div>
                <div class="product__slide-description">
                    <li>- Display 5.5</li>
                    <li>- Camera 13MP Video 1080p</li>
                </div>
                <div class="product__slide-publicer">
                    <div class="im-publicer">
                    Người bán:
                    <a class="url fn n" href="https://themeim.com/wp/blurb/author/admin/">Admin</a>
                    </div>
                </div>
                <ul class="product__slide-market">
                    <li>
                    <div class="product__logo">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/market_logo-4.png" alt="Presell">
                    </div>
                    <div class="product__price mx-auto p">
                        <span class="product__price-min">
                        1.500.000
                        </span>
                        <span class="product__price-">
                        VND
                        </span>
                    </div>
                    <div class="product__price-active p">
                        Đang bán
                    </div>
                    <a href="" class="btn btn-secondary .im-btn-1">
                        Tìm hiểu
                    </a>
                    </li>
                    <li>
                    <div class="product__logo">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/market_logo-3.png" alt="Fexport">
                    </div>
                    <div class="product__price mx-auto p">
                        <span class="product__price-min">
                        1.500.000
                        </span>
                        <span class="product__price-">
                        VND
                        </span>
                    </div>
                    <div class="product__price-active p">
                        Đang bán
                    </div>
                    <a href="" class="btn btn-secondary .im-btn-1">
                        Tìm hiểu
                    </a>
                    </li>
                    <li>
                    <div class="product__logo">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/market_logo-3.png" alt="Fexport">
                    </div>
                    <div class="product__price mx-auto p">
                        <span class="product__price-min">
                        1.500.000
                        </span>
                        <span class="product__price-">
                        VND
                        </span>
                    </div>
                    <div class="product__price-active p">
                        Đang bán
                    </div>
                    <a href="" class="btn btn-secondary .im-btn-1">
                        Tìm hiểu
                    </a>
                    </li>
                    <li>
                    <div class="product__logo">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/market_logo-3.png" alt="Fexport">
                    </div>
                    <div class="product__price mx-auto p">
                        <span class="product__price-min">
                        1.500.000
                        </span>
                        <span class="product__price-">
                        VND
                        </span>
                    </div>
                    <div class="product__price-active p">
                        Đang bán
                    </div>
                    <a href="" class="btn btn-secondary .im-btn-1">
                        Tìm hiểu
                    </a>
                    </li>
                    <li>
                    <div class="product__logo">
                        <img src="https://themeim.com/wp/blurb/wp-content/uploads/2019/05/market_logo-3.png" alt="Fexport">
                    </div>
                    <div class="product__price mx-auto p">
                        <span class="product__price-min">
                        1.500.000
                        </span>
                        <span class="product__price-">
                        VND
                        </span>
                    </div>
                    <div class="product__price-active p">
                        Đang bán
                    </div>
                    <a href="" class="btn btn-secondary .im-btn-1">
                        Tìm hiểu
                    </a>
                    </li>
                </ul>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
