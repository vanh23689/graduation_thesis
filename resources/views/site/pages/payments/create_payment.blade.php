<form action="{{ route('donate.create') }}" method="POST">
    @csrf
    <div class="field full mt-4">
        <label for="name">Tên người gửi</label>
        <input id="name" type="text" placeholder="Nguyễn Văn A" name="name" maxlength="255">
    </div>
    <div class="field full">
        <label for="address">Số tiền </label>
        <input id="address" type="text" placeholder="1000000" name="money" maxlength="255">
    </div>
    <div class="field full">
        <label for="bankCode">Ngân hàng</label>
        <select id="bankCode" class="null card-image form-control" name="bankCode">
            <option value="">Không chọn </option>
            <option value="MBAPP">Ung dung MobileBanking</option>
            <option value="VNPAYQR">VNPAYQR</option>
            <option value="VNBANK">LOCAL BANK</option>
            <option value="IB">INTERNET BANKING</option>
            <option value="ATM">ATM CARD</option>
            <option value="INTCARD">INTERNATIONAL CARD</option>
            <option value="VISA">VISA</option>
            <option value="MASTERCARD"> MASTERCARD</option>
            <option value="JCB">JCB</option>
            <option value="UPI">UPI</option>
            <option value="VIB">VIB</option>
            <option value="VIETCAPITALBANK">VIETCAPITALBANK</option>
            <option value="SCB">Ngan hang SCB</option>
            <option value="NCB">Ngan hang NCB</option>
            <option value="SACOMBANK">Ngan hang SacomBank  </option>
            <option value="EXIMBANK">Ngan hang EximBank </option>
            <option value="MSBANK">Ngan hang MSBANK </option>
            <option value="NAMABANK">Ngan hang NamABank </option>
            <option value="VNMART"> Vi dien tu VnMart</option>
            <option value="VIETINBANK">Ngan hang Vietinbank  </option>
            <option value="VIETCOMBANK">Ngan hang VCB </option>
            <option value="HDBANK">Ngan hang HDBank</option>
            <option value="DONGABANK">Ngan hang Dong A</option>
            <option value="TPBANK">Ngân hàng TPBank </option>
            <option value="OJB">Ngân hàng OceanBank</option>
            <option value="BIDV">Ngân hàng BIDV </option>
            <option value="TECHCOMBANK">Ngân hàng Techcombank </option>
            <option value="VPBANK">Ngan hang VPBank </option>
            <option value="AGRIBANK">Ngan hang Agribank </option>
            <option value="MBBANK">Ngan hang MBBank </option>
            <option value="ACB">Ngan hang ACB </option>
            <option value="OCB">Ngan hang OCB </option>
            <option value="IVB">Ngan hang IVB </option>
            <option value="SHB">Ngan hang SHB </option>
        </select>
    </div>
    <div class="flex justify-space-between">
        <div class="field half">
            <label for="language">Ngôn ngữ</label>
            <select name="language" id="language" name="language" class="form-control">
                <option value="vn">Tiếng Việt</option>
                <option value="en">English</option>
            </select>
        </div>
    </div>
    <div class="field full">
        <label for="description">Nội dung thanh toán</label>
        <textarea id="description" type="text" name="description" placeholder="Nội dung thanh toán"></textarea>
    </div>
    <button id="PayButton" class="btn btn-block mx-auto w-auto button button-primary submit-button" type="submit" name="redirect">
        <span class="submit-button-lock">Gửi</span>
    </button>
</form>
