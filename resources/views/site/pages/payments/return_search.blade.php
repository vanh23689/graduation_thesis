@extends('site.layouts.master')
@section('title')
    {{ 'Trang chủ' }}
@endsection
@section('content')
    @yield('style.css')
    <link rel="stylesheet" href="{{ asset('site/assets/css/vnpay-return.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <article class="card">
        <div class="container">
            <div class="card-title">
                <h2>Tìm kiếm giao dịch</h2>
            </div>
            <div class="card-body" id="tabs">
                <div class="payment-type">
                    <div class="payment-info tab-content">
                        <div class="column billing tab-pane active" id="home">
                            <div class="title">
                                <h4>Kết quả</h4>
                            </div>
                            @if(!empty($responseData))
                                    <div class="field full mt-4">
                                        <label >Số tiền: </label>
                                        <label>{{ number_format(str_replace('Amount=', '', $responseData[0])) }}</label>
                                    </div>
                                    <div class="field full">
                                        <label >Mã ngân hàng: </label>
                                        <label>{{ str_replace('BankCode=', '', $responseData[1]) }}</label>
                                    </div>
                                    <div class="field full">
                                        <label >Chủ tài khoản: </label>
                                        <label>{{ str_replace('CardHolder=', '', $responseData[2]) }}</label>
                                    </div>
                                    <div class="field full">
                                        <label >Số tài khoản: </label>
                                        <label>{{ str_replace('CardNumber=', '', $responseData[3]) }}</label>
                                    </div>
                                    <div class="field full">
                                        <label >Thời gian: </label>
                                        <label>{{ date('F jS, Y h:i:s', strtotime(str_replace('PayDate=', '', $responseData[8]))) }}</label>
                                    </div>
                                    <div class="field full">
                                        <label >Mã GD Tại VNPAY:</label>
                                        <label>{{ str_replace('TransactionNo=', '', $responseData[12]) }}</label>
                                    </div>
                                    @if(!empty($responseData[9]))
                                        <div class="field full">
                                            <label >Kết quả:</label>
                                            <label>
                                                @if(str_replace('ResponseCode=', '', $responseData[9]) == "00")
                                                    <span class="text-green-600">GD thành công</span>
                                                @else
                                                    <span class="text-red-600">GD thất bại</span>
                                                @endif
                                            </label>
                                        </div>
                                    @endif
                            @else
                                @if($errors)
                                    <div class="alert alert-danger" style="margin-top: 2rem;font-size: 14px">
                                        {{ $errors }}
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-actions flex justify-space-between">
                <div class="flex-start">
                    <a href="{{ route('home.index') }}">
                        <button class="button button-secondary">
                            Quay về trang chủ
                        </button>
                    </a>
                </div>
                <div class="flex-end">
                    <a href="{{ route('donate.vnpay.index') }}">
                        <button class="button button-link">
                            Tiếp tục
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </article>
    @yield('footer_script')
@endsection
