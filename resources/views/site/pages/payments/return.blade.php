@extends('site.layouts.master')
@section('title')
    {{ 'Trang chủ' }}
@endsection
@section('content')
    @yield('style.css')
    <link rel="stylesheet" href="{{ asset('site/assets/css/vnpay-return.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <article class="card">
        <div class="container">
            <div class="card-title">
                <h2>Thanh toán</h2>
            </div>
            <div class="card-body" id="tabs">
                <div class="payment-type">
                    <div class="payment-info tab-content">
                        <div class="column billing tab-pane active" id="home">
                            <div class="title">
                                <div class="num">1</div>
                                <h4>Kết quả</h4>
                            </div>
                            @if(empty($dataResponse['error']))
                                <div class="field full mt-4">
                                    <label >Mã đơn hàng: </label>
                                    <label>{{ $dataResponse['order_id'] }}</label>
                                </div>
                                <div class="field full">
                                    <label >Số tiền: </label>
                                    <label>{{ $dataResponse['amount'] }}</label>
                                </div>
                                <div class="field full">
                                    <label >Nội dung: </label>
                                    <label>{{ $dataResponse['order_info'] }}</label>
                                </div>
                                @if (!empty($dataResponse['create_by']))
                                <div class="field full">
                                    <label>Người tạo: </label>
                                    <label style="text-transform: none;">{{ $dataResponse['create_by'] }}</label>
                                </div>
                                @endif
                                @if (!empty($dataResponse['transaction_status']))
                                <div class="field full">
                                    <label >Mã phản hồi (vnp_ResponseCode):</label>
                                    <label>{{ $dataResponse['transaction_status'] }}</label>
                                </div>
                                @endif
                                @if (!empty($dataResponse['transaction_no']))
                                <div class="field full">
                                    <label >Mã GD Tại VNPAY:</label>
                                    <label>{{ $dataResponse['transaction_no'] }}</label>
                                </div>
                                @endif
                                @if (!empty($dataResponse['bank_code']))
                                <div class="field full">
                                    <label >Mã Ngân hàng:</label>
                                    <label>{{ $dataResponse['bank_code'] }}</label>
                                </div>
                                @endif
                                @if (!empty($dataResponse['pay_date']))
                                <div class="field full">
                                    <label >Thời gian thanh toán:</label>
                                    <label>{{ $dataResponse['pay_date'] }}</label>
                                </div>
                                @endif
                                @if (!empty($dataResponse['vnp_TransDate']))
                                <div class="field full">
                                    <label >Thời gian yêu cầu:</label>
                                    <label>{{ $dataResponse['vnp_TransDate'] }}</label>
                                </div>
                                @endif
                                @if(!empty($dataResponse['secureHash']))
                                    <div class="field full">
                                        <label >Kết quả:</label>
                                        <label>
                                            @if($dataResponse['secureHash'] == $dataResponse['vnp_SecureHash'])
                                                @if($dataResponse['transaction_status'] == '00')
                                                    <span class="text-green-600">GD thành công</span>
                                                @else
                                                    <span class="text-red-600">GD thất bại</span>
                                                @endif
                                            @endif
                                        </label>
                                    </div>
                                @endif
                            @else
                                @if($dataResponse['error'])
                                    <div class="alert alert-danger" style="margin-top: 2rem;font-size: 14px">
                                        {{ $dataResponse['error'] }}
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-actions flex justify-space-between">
                <div class="flex-start">
                    <a href="{{ route('home.index') }}">
                        <button class="button button-secondary">
                            Quay về trang chủ
                        </button>
                    </a>
                </div>
                <div class="flex-end">
                    <a href="{{ route('donate.vnpay.index') }}">
                        <button class="button button-link">
                            Tiếp tục
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </article>
    @yield('footer_script')
@endsection
