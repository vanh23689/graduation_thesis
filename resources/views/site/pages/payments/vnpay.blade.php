@extends('site.layouts.master')
@section('title')
    {{ 'Trang chủ' }}
@endsection
@section('content')
    @yield('style.css')
    <link rel="stylesheet" href="{{ asset('site/assets/css/vnpay.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <article class="card">
        <div class="container">
            <div class="card-title">
                <h2>Payment</h2>
            </div>
            <div class="card-body">
                <div class="payment-type" id="tabs">
                    <div class="types flex justify-space-between">
                        <div class="type selected" data-id="pay">
                            <div class="logo">
                                <i class="far fa-credit-card"></i>
                            </div>
                            <div class="text">
                                <p>Thanh toán với Pay</p>
                            </div>
                        </div>
                        <div class="type" data-id="transaction">
                            <div class="logo">
                                <i class="fab fa-paypal"></i>
                            </div>
                            <div class="text">
                                <p>Tra cứu giao dịch</p>
                            </div>
                        </div>
                        <div class="type" data-id="refund">
                            <div class="logo">
                                <i class="fab fa-amazon"></i>
                            </div>
                            <div class="text">
                                <p>Yêu cầu hoàn tiền</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payment-info tab-content">
                    <div class="column billing tab-pane active" id="pay">
                        <div class="title">
                            <div class="num">1</div>
                            <h4>Thanh toán với Pay</h4>
                        </div>
                        @include('site.pages.payments.create_payment')

                    </div>
                    <div class="column shipping tab-pane" id="transaction">
                        @include('site.pages.payments.transaction')
                    </div>
                    <div class="column shipping tab-pane" id="refund">
                        @include('site.pages.payments.refund')
                    </div>

                </div>
            </div>
            <div class="card-actions flex justify-space-between">
                <div class="flex-start">
                    <a href="{{ route('home.index') }}">
                        <button class="button button-secondary">
                            Quay về trang chủ
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </article>
@yield('footer_script')
<script>
    const tabs = document.getElementById("tabs");
    const tabButton = document.querySelectorAll(".type");
    const contents = document.querySelectorAll(".column.tab-pane");
    tabs.onclick = e => {
    console.log("🚀 ~ file: vnpay.blade.php ~ line 353 ~ e", e)
    const id = e.path[2].dataset.id;
    if (id) {
        tabButton.forEach(btn => {
            if(btn.dataset.id == id){
                btn.classList.add("selected");

            }else{
                btn.classList.remove("selected");
            }

        });

        contents.forEach(content => {
        content.classList.remove("active");
        });
        const element = document.getElementById(id);
        element.classList.add("active");
    }
    }
</script>
@endsection

