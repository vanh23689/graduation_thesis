<form action="{{ route('transaction.post') }}" method="POST">
    @csrf
    <div class="field full mt-4">
        <label for="orderid">Mã đơn hàng</label>
        <input id="orderid" type="text" placeholder="VD: ABC123" data-val="true" name="orderid" maxlength="255">
    </div>
    <div class="field full">
        <label for="paymentdate">Ngày thanh toán </label>
        <input id="paymentdate" type="text" placeholder="VD: 20221018133107" data-val="true"  name="paymentdate" maxlength="255">
    </div>

    <input type="submit"  class="btn btn-block mx-auto w-auto button button-primary submit-button" value="Gửi" />

</form>
