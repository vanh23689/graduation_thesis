<form action="{{ route('refund.post') }}" method="POST">
    @csrf
    <div class="field full mt-4">
        <label for="orderid_refund">Mã đơn hàng</label>
        <input id="orderid_refund" type="text" placeholder="ABC123" data-val="true" name="orderid_refund" maxlength="255">
    </div>
    <div class="field full">
        <label for="trantype">Kiểu hoàn tiền </label>
        <select id="trantype" class="null card-image form-control" name="trantype">
            <option value="02">Hoàn tiền toàn phần</option>
            <option value="03">Hoàn tiền 1 phần</option>
        </select>
    </div>
    <div class="field full">
        <label for="amount">Số tiền</label>
        <input class="form-control" data-val="true" data-val-number="The field Amount must be a number." data-val-required="The Amount field is required." id="amount" max="100000000" min="1" name="amount" type="number" value="10000" />
    </div>
    <div class="field full">
        <label>Payment Date</label>
        <input class="form-control" data-val="true"  name="paymentdate" type="text" value="" />
    </div>
    <div class="field full">
        <label for="description_refund">Lời nhắn (bắt buộc)</label>
        <textarea id="description_refund" type="text" name="description_refund" placeholder="Lời nhắn"></textarea>
    </div>
    <div class="field full">
        <label >Mail người khởi tạo GD hoàn tiền</label>
        <input class="form-control" data-val="true"  name="mail" type="text" value="" />
    </div>
    <input type="submit"  class="btn btn-block mx-auto w-auto button button-primary submit-button" value="Gửi" />




</form>
