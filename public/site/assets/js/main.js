!(function(n, i, e, a) {
    ;(n.navigation = function(t, s) {
    var o = {
        responsive: !0,
        mobileBreakpoint: 991,
        showDuration: 200,
        hideDuration: 200,
        showDelayDuration: 0,
        hideDelayDuration: 0,
        submenuTrigger: "hover",
        effect: "fadeIn",
        submenuIndicator: !0,
        submenuIndicatorTrigger: !1,
        hideSubWhenGoOut: !0,
        visibleSubmenusOnMobile: !1,
        fixed: !1,
        overlay: !0,
        overlayColor: "rgba(0, 0, 0, 0.5)",
        hidden: !1,
        hiddenOnMobile: !1,
        offCanvasSide: "left",
        offCanvasCloseButton: !0,
        animationOnShow: "",
        animationOnHide: "",
        onInit: function() {},
        onLandscape: function() {},
        onPortrait: function() {},
        onShowOffCanvas: function() {},
        onHideOffCanvas: function() {}
        },
        r = this,
        u = Number.MAX_VALUE,
        d = 1,
        l = "click.nav touchstart.nav",
        f = "mouseenter focusin",
        c = "mouseleave focusout"
    r.settings = {}
    var t = (n(t), t)
    n(t).find(".nav-search").length > 0 &&
        n(t)
        .find(".nav-search")
        .find("form")
        .prepend(
            "<span class='nav-search-close-button' tabindex='0'>&#10005;</span>"
        ),
        (r.init = function() {
        ;(r.settings = n.extend({}, o, s)),
            r.settings.offCanvasCloseButton &&
            n(t)
                .find(".nav-menus-wrapper")
                .prepend(
                "<span class='nav-menus-wrapper-close-button'>&#10005;</span>"
                ),
            "right" == r.settings.offCanvasSide &&
            n(t)
                .find(".nav-menus-wrapper")
                .addClass("nav-menus-wrapper-right"),
            r.settings.hidden &&
            (n(t).addClass("navigation-hidden"),
            (r.settings.mobileBreakpoint = 99999)),
            v(),
            r.settings.fixed && n(t).addClass("navigation-fixed"),
            n(t)
            .find(".nav-toggle")
            .on("click touchstart", function(n) {
                n.stopPropagation(),
                n.preventDefault(),
                r.showOffcanvas(),
                s !== a && r.callback("onShowOffCanvas")
            }),
            n(t)
            .find(".nav-menus-wrapper-close-button")
            .on("click touchstart", function() {
                r.hideOffcanvas(), s !== a && r.callback("onHideOffCanvas")
            }),
            n(t)
            .find(".nav-search-button, .nav-search-close-button")
            .on("click touchstart keydown", function(i) {
                i.stopPropagation(), i.preventDefault()
                var e = i.keyCode || i.which
                "click" === i.type ||
                "touchstart" === i.type ||
                ("keydown" === i.type && 13 == e)
                ? r.toggleSearch()
                : 9 == e && n(i.target).blur()
            }),
            n(t).find(".megamenu-tabs").length > 0 && y(),
            n(i).resize(function() {
            r.initNavigationMode(C()), O(), r.settings.hiddenOnMobile && m()
            }),
            r.initNavigationMode(C()),
            r.settings.hiddenOnMobile && m(),
            s !== a && r.callback("onInit")
        })
    var h = function() {
        n(t)
            .find(".nav-submenu")
            .hide(0),
            n(t)
            .find("li")
            .removeClass("focus")
        },
        v = function() {
        n(t)
            .find("li")
            .each(function() {
            n(this).children(".nav-dropdown,.megamenu-panel").length > 0 &&
                (n(this)
                .children(".nav-dropdown,.megamenu-panel")
                .addClass("nav-submenu"),
                r.settings.submenuIndicator &&
                n(this)
                    .children("a")
                    .append(
                    "<span class='submenu-indicator'><span class='submenu-indicator-chevron'></span></span>"
                    ))
            })
        },
        m = function() {
        n(t).hasClass("navigation-portrait")
            ? n(t).addClass("navigation-hidden")
            : n(t).removeClass("navigation-hidden")
        }
    ;(r.showSubmenu = function(i, e) {
        C() > r.settings.mobileBreakpoint &&
        n(t)
            .find(".nav-search")
            .find("form")
            .fadeOut(),
        "fade" == e
            ? n(i)
                .children(".nav-submenu")
                .stop(!0, !0)
                .delay(r.settings.showDelayDuration)
                .fadeIn(r.settings.showDuration)
                .removeClass(r.settings.animationOnHide)
                .addClass(r.settings.animationOnShow)
            : n(i)
                .children(".nav-submenu")
                .stop(!0, !0)
                .delay(r.settings.showDelayDuration)
                .slideDown(r.settings.showDuration)
                .removeClass(r.settings.animationOnHide)
                .addClass(r.settings.animationOnShow),
        n(i).addClass("focus")
    }),
        (r.hideSubmenu = function(i, e) {
        "fade" == e
            ? n(i)
                .find(".nav-submenu")
                .stop(!0, !0)
                .delay(r.settings.hideDelayDuration)
                .fadeOut(r.settings.hideDuration)
                .removeClass(r.settings.animationOnShow)
                .addClass(r.settings.animationOnHide)
            : n(i)
                .find(".nav-submenu")
                .stop(!0, !0)
                .delay(r.settings.hideDelayDuration)
                .slideUp(r.settings.hideDuration)
                .removeClass(r.settings.animationOnShow)
                .addClass(r.settings.animationOnHide),
            n(i)
            .removeClass("focus")
            .find(".focus")
            .removeClass("focus")
        })
    var p = function() {
        n("body").addClass("no-scroll"),
            r.settings.overlay &&
            (n(t).append("<div class='nav-overlay-panel'></div>"),
            n(t)
                .find(".nav-overlay-panel")
                .css("background-color", r.settings.overlayColor)
                .fadeIn(300)
                .on("click touchstart", function(n) {
                r.hideOffcanvas()
                }))
        },
        g = function() {
        n("body").removeClass("no-scroll"),
            r.settings.overlay &&
            n(t)
                .find(".nav-overlay-panel")
                .fadeOut(400, function() {
                n(this).remove()
                })
        }
    ;(r.showOffcanvas = function() {
        p(),
        "left" == r.settings.offCanvasSide
            ? n(t)
                .find(".nav-menus-wrapper")
                .css("transition-property", "left")
                .addClass("nav-menus-wrapper-open")
            : n(t)
                .find(".nav-menus-wrapper")
                .css("transition-property", "right")
                .addClass("nav-menus-wrapper-open")
    }),
        (r.hideOffcanvas = function() {
        n(t)
            .find(".nav-menus-wrapper")
            .removeClass("nav-menus-wrapper-open")
            .on(
            "webkitTransitionEnd moztransitionend transitionend oTransitionEnd",
            function() {
                n(t)
                .find(".nav-menus-wrapper")
                .css("transition-property", "none")
                .off()
            }
            ),
            g()
        }),
        (r.toggleOffcanvas = function() {
        C() <= r.settings.mobileBreakpoint &&
            (n(t)
            .find(".nav-menus-wrapper")
            .hasClass("nav-menus-wrapper-open")
            ? (r.hideOffcanvas(), s !== a && r.callback("onHideOffCanvas"))
            : (r.showOffcanvas(), s !== a && r.callback("onShowOffCanvas")))
        }),
        (r.toggleSearch = function() {
        "none" ==
        n(t)
            .find(".nav-search")
            .find("form")
            .css("display")
            ? (n(t)
                .find(".nav-search")
                .find("form")
                .fadeIn(200),
            n(t)
                .find(".nav-search")
                .find("input")
                .focus())
            : (n(t)
                .find(".nav-search")
                .find("form")
                .fadeOut(200),
            n(t)
                .find(".nav-search")
                .find("input")
                .blur())
        }),
        (r.initNavigationMode = function(i) {
        r.settings.responsive
            ? (i <= r.settings.mobileBreakpoint &&
                u > r.settings.mobileBreakpoint &&
                (n(t)
                .addClass("navigation-portrait")
                .removeClass("navigation-landscape"),
                S(),
                s !== a && r.callback("onPortrait")),
            i > r.settings.mobileBreakpoint &&
                d <= r.settings.mobileBreakpoint &&
                (n(t)
                .addClass("navigation-landscape")
                .removeClass("navigation-portrait"),
                k(),
                g(),
                r.hideOffcanvas(),
                s !== a && r.callback("onLandscape")),
            (u = i),
            (d = i))
            : (n(t).addClass("navigation-landscape"),
            k(),
            s !== a && r.callback("onLandscape"))
        })
    var b = function() {
        n("html").on("click.body touchstart.body", function(i) {
            0 === n(i.target).closest(".navigation").length &&
            (n(t)
                .find(".nav-submenu")
                .fadeOut(),
            n(t)
                .find(".focus")
                .removeClass("focus"),
            n(t)
                .find(".nav-search")
                .find("form")
                .fadeOut())
        })
        },
        C = function() {
        return (
            i.innerWidth || e.documentElement.clientWidth || e.body.clientWidth
        )
        },
        w = function() {
        n(t)
            .find(".nav-menu")
            .find("li, a")
            .off(l)
            .off(f)
            .off(c)
        },
        O = function() {
        if (C() > r.settings.mobileBreakpoint) {
            var i = n(t).outerWidth(!0)
            n(t)
            .find(".nav-menu")
            .children("li")
            .children(".nav-submenu")
            .each(function() {
                n(this)
                .parent()
                .position().left +
                n(this).outerWidth() >
                i
                ? n(this).css("right", 0)
                : n(this).css("right", "auto")
            })
        }
        },
        y = function() {
        function i(i) {
            var e = n(i)
                .children(".megamenu-tabs-nav")
                .children("li"),
            a = n(i).children(".megamenu-tabs-pane")
            n(e).on("click.tabs touchstart.tabs", function(i) {
            i.stopPropagation(),
                i.preventDefault(),
                n(e).removeClass("active"),
                n(this).addClass("active"),
                n(a)
                .hide(0)
                .removeClass("active"),
                n(a[n(this).index()])
                .show(0)
                .addClass("active")
            })
        }
        if (n(t).find(".megamenu-tabs").length > 0)
            for (var e = n(t).find(".megamenu-tabs"), a = 0; a < e.length; a++)
            i(e[a])
        },
        k = function() {
        w(),
            h(),
            navigator.userAgent.match(/Mobi/i) ||
            navigator.maxTouchPoints > 0 ||
            "click" == r.settings.submenuTrigger
            ? n(t)
                .find(".nav-menu, .nav-dropdown")
                .children("li")
                .children("a")
                .on(l, function(e) {
                    if (
                    (r.hideSubmenu(
                        n(this)
                        .parent("li")
                        .siblings("li"),
                        r.settings.effect
                    ),
                    n(this)
                        .closest(".nav-menu")
                        .siblings(".nav-menu")
                        .find(".nav-submenu")
                        .fadeOut(r.settings.hideDuration),
                    n(this).siblings(".nav-submenu").length > 0)
                    ) {
                    if (
                        (e.stopPropagation(),
                        e.preventDefault(),
                        "none" ==
                        n(this)
                            .siblings(".nav-submenu")
                            .css("display"))
                    )
                        return (
                        r.showSubmenu(
                            n(this).parent("li"),
                            r.settings.effect
                        ),
                        O(),
                        !1
                        )
                    if (
                        (r.hideSubmenu(n(this).parent("li"), r.settings.effect),
                        "_blank" === n(this).attr("target") ||
                        "blank" === n(this).attr("target"))
                    )
                        i.open(n(this).attr("href"))
                    else {
                        if (
                        "#" === n(this).attr("href") ||
                        "" === n(this).attr("href") ||
                        "javascript:void(0)" === n(this).attr("href")
                        )
                        return !1
                        i.location.href = n(this).attr("href")
                    }
                    }
                })
            : n(t)
                .find(".nav-menu")
                .find("li")
                .on(f, function() {
                    r.showSubmenu(this, r.settings.effect), O()
                })
                .on(c, function() {
                    r.hideSubmenu(this, r.settings.effect)
                }),
            r.settings.hideSubWhenGoOut && b()
        },
        S = function() {
        w(),
            h(),
            r.settings.visibleSubmenusOnMobile
            ? n(t)
                .find(".nav-submenu")
                .show(0)
            : (n(t)
                .find(".submenu-indicator")
                .removeClass("submenu-indicator-up"),
                r.settings.submenuIndicator &&
                r.settings.submenuIndicatorTrigger
                ? n(t)
                    .find(".submenu-indicator")
                    .on(l, function(i) {
                        return (
                        i.stopPropagation(),
                        i.preventDefault(),
                        r.hideSubmenu(
                            n(this)
                            .parent("a")
                            .parent("li")
                            .siblings("li"),
                            "slide"
                        ),
                        r.hideSubmenu(
                            n(this)
                            .closest(".nav-menu")
                            .siblings(".nav-menu")
                            .children("li"),
                            "slide"
                        ),
                        "none" ==
                        n(this)
                            .parent("a")
                            .siblings(".nav-submenu")
                            .css("display")
                            ? (n(this).addClass("submenu-indicator-up"),
                            n(this)
                                .parent("a")
                                .parent("li")
                                .siblings("li")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                            n(this)
                                .closest(".nav-menu")
                                .siblings(".nav-menu")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                            r.showSubmenu(
                                n(this)
                                .parent("a")
                                .parent("li"),
                                "slide"
                            ),
                            !1)
                            : (n(this)
                                .parent("a")
                                .parent("li")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                            void r.hideSubmenu(
                                n(this)
                                .parent("a")
                                .parent("li"),
                                "slide"
                            ))
                        )
                    })
                : n(t)
                    .find(".nav-menu, .nav-dropdown")
                    .children("li")
                    .children("a")
                    .on(l, function(e) {
                        if (
                        (e.stopPropagation(),
                        e.preventDefault(),
                        r.hideSubmenu(
                            n(this)
                            .parent("li")
                            .siblings("li"),
                            r.settings.effect
                        ),
                        r.hideSubmenu(
                            n(this)
                            .closest(".nav-menu")
                            .siblings(".nav-menu")
                            .children("li"),
                            "slide"
                        ),
                        "none" ==
                            n(this)
                            .siblings(".nav-submenu")
                            .css("display"))
                        )
                        return (
                            n(this)
                            .children(".submenu-indicator")
                            .addClass("submenu-indicator-up"),
                            n(this)
                            .parent("li")
                            .siblings("li")
                            .find(".submenu-indicator")
                            .removeClass("submenu-indicator-up"),
                            n(this)
                            .closest(".nav-menu")
                            .siblings(".nav-menu")
                            .find(".submenu-indicator")
                            .removeClass("submenu-indicator-up"),
                            r.showSubmenu(n(this).parent("li"), "slide"),
                            !1
                        )
                        if (
                        (n(this)
                            .parent("li")
                            .find(".submenu-indicator")
                            .removeClass("submenu-indicator-up"),
                        r.hideSubmenu(n(this).parent("li"), "slide"),
                        "_blank" === n(this).attr("target") ||
                            "blank" === n(this).attr("target"))
                        )
                        i.open(n(this).attr("href"))
                        else {
                        if (
                            "#" === n(this).attr("href") ||
                            "" === n(this).attr("href") ||
                            "javascript:void(0)" === n(this).attr("href")
                        )
                            return !1
                        i.location.href = n(this).attr("href")
                        }
                    }))
        }
    ;(r.callback = function(n) {
        s[n] !== a && s[n].call(t)
    }),
        r.init()
    }),
    (n.fn.navigation = function(i) {
        return this.each(function() {
        if (a === n(this).data("navigation")) {
            var e = new n.navigation(this, i)
            n(this).data("navigation", e)
        }
        })
    })
})(jQuery, window, document);
(function($) {
    "use strict"

    var $window = $(window);

    if ($.fn.navigation) {
    $("#navigation1").navigation()
    $("#always-hidden-nav").navigation({
        hidden: true
    })
    }

    $window.on("load", function() {
    $("#preloader").fadeOut("slow", function() {
        $(this).remove()
    })
    });
    // Scroll to top
    var btn = $('#scrollUp');

    $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
    });

    btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
    });

    $(".nav-header-bar").click (function(){
    // $(this).toggleClass("open");
    $(".nav-menu-toggle").toggleClass("open");
    });

    // Light Sldier
    $("#lightSlider").lightSlider({
        gallery:true,
        item:1,
        loop:false,
        slideMove: 1,
        prevHtml: '',
        nextHtml: '',
        thumbItem:4,
        slideMargin:0,
    });
    $('#lightSliderModal').lightSlider({
        gallery:true,
        item:1,
        loop:false,
        slideMove: 1,
        prevHtml: '',
        nextHtml: '',
        thumbItem:4,
        slideMargin:0,
    });
    //Owl carousel
    $('.banner__box').owlCarousel({
        margin:10,
        responsiveClass:true,
        dots:false,
        loop: true,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="32" height="17" viewBox="0 0 32 17"><path id="Forma_1" data-name="Forma 1" class="sldier_arrow_path" d="M8.158,0.007L8.835,0.685,1.5,8.019H32V8.979H1.5l7.338,7.334-0.677.679L0,8.839V8.16Z"></path></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="17" viewBox="0 0 32 17"><path id="Forma_2" data-name="Forma 1" class="sldier_arrow_path" d="M23.842,0.007l-0.677.678L30.5,8.019H0V8.979H30.5l-7.338,7.334,0.677,0.679L32,8.839V8.16Z"></path></svg>'],
        responsive:{
            0:{
                items:1,
                nav:false
            },
            600:{
                items:1,
                nav:false
            },
            1000:{
                items:1,
                nav:true,
            }
        }
    });
    $('.amazon__product').owlCarousel({
        margin:10,
        responsiveClass:true,
        dots:false,
        loop: true,
        navText: ['<i class="fas fa-long-arrow-alt-left"></i>', '<i class="fas fa-long-arrow-alt-right"></i>'],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:true
            },
            1000:{
                items:4,
                nav:true,
            }
    }
    });
    $('.ali_express__content-link').owlCarousel({
    margin:10,
    responsiveClass:true,
    dots:false,
    loop: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true,
        }
    }
    });
    var html = '';
    var owl = $('.owl-carousel');
        owl.owlCarousel({
            nav:false,
            dot: false,
            margin:10,
            loop:true,
            responsive:{
                0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:2
		        }
            }
    });

    $.ajax({
        url: '/site/compare-get/',
        dataType: 'json',
        success: function(data) {
            var content = '';
            $.each(data.product, function (index, val){
                let features = (val.features != null) ? '<li>'+ val.features +'</li>' : '<li>'+ '-' +'</li>';
                let producer = (val.producer != null) ? '<li>' + val.producer + '</li>' : '<li>'+ '-' +'</li>';
                let version = (val.version != null) ? val.version : '-';
                let size = (val.size != null) ? val.size : '-';
                let screen = (val.screen != null) ? val.screen : '-';
                let weight = (val.weight != null) ? val.weight : '-';
                let height = (val.height != null) ? val.height : '-';
                let wifi = (val.wifi != null) ? val.wifi : '-';
                let bluetooth = (val.bluetooth != null) ? val.bluetooth : '-';
                let graphics = (val.graphics != null) ? val.graphics : '-';
                let charging_technology = (val.charging_technology != null) ? val.charging_technology : '-';
                let cpu = (val.cpu != null) ? val.cpu : '-';
                let hdmi = (val.hdmi != null) ? val.hdmi : '-';
                let headphone = (val.headphone != null) ? val.headphone : '-';
                let insurance = (val.insurance != null) ? val.insurance : '-';
                let lcd = (val.lcd != null) ? val.lcd : '-';
                let ram = (val.ram != null) ? val.ram : '-';
                let resolution = (val.resolution != null) ? val.resolution : '-';
                let speakerphone = (val.speakerphone != null) ? val.speakerphone : '-';
                let touch = (val.touch != null) ? val.touch : '-';
                let usb = (val.usb != null) ? val.usb : '-';
                let vga = (val.vga != null) ? val.vga : '-';
                let voice = (val.voice != null) ? val.voice : '-';
                let warranty_period = (val.warranty_period != null) ? val.warranty_period : '-';
                let wattage = (val.wattage != null) ? val.wattage : '-';
                let network_support = (val.network_support != null) ? val.network_support : '-';
                // civils
                let auto_restart = (val.civils.auto_restart != null) ? val.civils.auto_restart : '-';
                let automatic_ice_maker = (val.civils.automatic_ice_maker != null) ? val.civils.automatic_ice_maker : '-';
                let average_cooling_speed = (val.civils.average_cooling_speed != null) ? val.civils.average_cooling_speed : '-';
                let bracket_weight = (val.civils.bracket_weight != null) ? val.civils.bracket_weight : '-';
                let capacity = (val.civils.capacity != null) ? val.civils.capacity : '-';
                let deodorant = (val.civils.deodorant != null) ? val.civils.deodorant : '-';
                let dimensions_with_stand = (val.civils.dimensions_with_stand != null) ? val.civils.dimensions_with_stand : '-';
                let door_bell = (val.civils.door_bell != null) ? val.civils.door_bell : '-';
                let door_number = (val.civils.door_number != null) ? val.civils.door_number : '-';
                let effective_range = (val.civils.effective_range != null) ? val.civils.effective_range : '-';
                let error_guessing = (val.civils.error_guessing != null) ? val.civils.error_guessing : '-';
                let freezer_capacity = (val.civils.freezer_capacity != null) ? val.civils.freezer_capacity : '-';
                let freezer_compartment_capacity = (val.civils.freezer_compartment_capacity != null) ? val.civils.freezer_compartment_capacity : '-';
                let gas = (val.civils.gas != null) ? val.civils.gas : '-';
                let get_outside_water = (val.civils.get_outside_water != null) ? val.civils.get_outside_water : '-';
                let hygroscopic = (val.civils.hygroscopic != null) ? val.civils.hygroscopic : '-';
                let material = (val.civils.material != null) ? val.civils.material : '-';
                let maximum_cooling_speed = (val.civils.maximum_cooling_speed != null) ? val.civils.maximum_cooling_speed : '-';
                let minimum_cooling_rate = (val.civils.minimum_cooling_rate != null) ? val.civils.minimum_cooling_rate : '-';
                let mosquito_repellent = (val.civils.mosquito_repellent != null) ? val.civils.mosquito_repellent : '-';
                let noise_level = (val.civils.noise_level != null) ? val.civils.noise_level : '-';
                let number_of_speakers = (val.civils.number_of_speakers != null) ? val.civils.number_of_speakers : '-';
                let outer_material = (val.civils.outer_material != null) ? val.civils.outer_material : '-';
                let processor = (val.civils.processor != null) ? val.civils.processor : '-';
                let rapid_cooling = (val.civils.rapid_cooling != null) ? val.civils.rapid_cooling : '-';
                let refrigerator_technology = (val.civils.refrigerator_technology != null) ? val.civils.refrigerator_technology : '-';
                let regime = (val.civils.regime != null) ? val.civils.regime : '-';
                let smart_sharing = (val.civils.smart_sharing != null) ? val.civils.smart_sharing : '-';
                let snow = (val.civils.snow != null) ? val.civils.snow : '-';
                let steaming_tray = (val.civils.steaming_tray != null) ? val.civils.steaming_tray : '-';
                let total_peaker_power = (val.civils.total_peaker_power != null) ? val.civils.total_peaker_power : '-';
                let tray_material = (val.civils.tray_material != null) ? val.civils.tray_material : '-';
                //end civils
                content = '<div class="chart__item" ><ul data-id="'+ val.id +'">' +
                            '<li><a class="blurb-remove"><i class="fa fa-times" aria-hidden="true"></i></a><div class="chart__item-img"><img src="'+ val.img +'" alt="'+ val.name +'"></div><h2><a href="'+ "product-detail/" + val.id +'">'+ val.name +'</a></h2><div class="wishlist"><a class="wishlist__heart"><i class="far fa-heart"></i></a></div></li>' +
                            '<li class="chart__item-logo"><img src="'+ val.logo +'" alt="'+ val.name +'"><div class="chart__item-price"><a href="'+ val.website +'" target="_blank">Tìm hiểu</a><span class="blurb-currency-symbol"></span><span class="blurb-price-min">'+ val.current_price + '</span></div></li>' +
                            '<li class="overview" style="height: 65px;"></li>' +
                            features +
                            '<li>' +
                                '<div class="price-start">' +
                                    '<p class="text-center">' +
                                        '<strong class="blurb-price-color">' +
                                            '<span>'+ val.current_price + ' (' + val.reduce + ')' +'</span>' +
                                        '</strong>' +
                                    '</p>' +
                                '</div>' +
                            '</li>' +
                            '<li>' +
                                size +
                            '</li>' +
                            '<li>' +
                                screen +
                            '</li>' +
                            '<li>' +
                                weight +
                            '</li>' +
                            '<li>' +
                                height +
                            '</li>' +
                                producer +
                            '<li>' +
                                version +
                            '</li>' +
                            '<li class="overview" style="height: 65px;"></li>' +
                            '<li>' +
                                wifi +
                            '</li>' +
                            '<li>' +
                                bluetooth +
                            '</li>' +
                            '<li>' +
                                graphics +
                            '</li>' +
                            '<li>' +
                                charging_technology +
                            '</li>' +
                            '<li>' +
                                cpu +
                            '</li>' +
                            '<li>' +
                                hdmi +
                            '</li>' +
                            '<li>' +
                                headphone +
                            '</li>' +
                            '<li>' +
                                insurance +
                            '</li>' +
                            '<li>' +
                                lcd +
                            '</li>' +
                            '<li>' +
                                network_support +
                            '</li>' +
                            '<li>' +
                                ram +
                            '</li>' +
                            '<li>' +
                                resolution +
                            '</li>' +
                            '<li>' +
                                speakerphone +
                            '</li>' +
                            '<li>' +
                                touch +
                            '</li>' +
                            '<li>' +
                                usb +
                            '</li>' +
                            '<li>' +
                                vga +
                            '</li>' +
                            '<li>' +
                                voice +
                            '</li>' +
                            '<li>' +
                                warranty_period +
                            '</li>' +
                            '<li>' +
                                wattage +
                            '</li>' +
                            '<li>' +
                                capacity +
                            '</li>' +
                            '<li>' +
                                deodorant +
                            '</li>' +
                            '<li>' +
                                processor +
                            '</li>' +
                            '<li>' +
                                total_peaker_power +
                            '</li>' +
                            '<li>' +
                            number_of_speakers +
                            '</li>' +
                            '<li>' +
                            smart_sharing +
                            '</li>' +
                            '<li>' +
                            dimensions_with_stand +
                            '</li>' +
                            '<li>' +
                            bracket_weight +
                            '</li>' +
                            '<li>' +
                            door_number +
                            '</li>' +
                            '<li>' +
                            freezer_capacity +
                            '</li>' +
                            '<li>' +
                            freezer_compartment_capacity +
                            '</li>' +
                            '<li>' +
                            snow +
                            '</li>' +
                            '<li>' +
                            refrigerator_technology +
                            '</li>' +
                            '<li>' +
                            tray_material +
                            '</li>' +
                            '<li>' +
                            outer_material +
                            '</li>' +
                            '<li>' +
                            automatic_ice_maker +
                            '</li>' +
                            '<li>' +
                            get_outside_water +
                            '</li>' +
                            '<li>' +
                            door_bell +
                            '</li>' +
                            '<li>' +
                            material +
                            '</li>' +
                            '<li>' +
                            regime +
                            '</li>' +
                            '<li>' +
                            steaming_tray +
                            '</li>' +
                            '<li>' +
                            minimum_cooling_rate +
                            '</li>' +
                            '<li>' +
                            maximum_cooling_speed +
                            '</li>' +
                            '<li>' +
                            average_cooling_speed +
                            '</li>' +
                            '<li>' +
                            rapid_cooling +
                            '</li>' +
                            '<li>' +
                            error_guessing +
                            '</li>' +
                            '<li>' +
                            auto_restart +
                            '</li>' +
                            '<li>' +
                            mosquito_repellent +
                            '</li>' +
                            '<li>' +
                            gas +
                            '</li>' +
                            '<li>' +
                            hygroscopic +
                            '</li>' +
                            '<li>' +
                            noise_level +
                            '</li>' +
                            '<li>' +
                            effective_range +
                            '</li>' +
                            '</ul></div>';
                $('.compare__products-carousel').owlCarousel().trigger('add.owl.carousel', [jQuery(content)]).trigger('refresh.owl.carousel');

            });
        }
    });

    $(".compare__products-carousel .owl-stage").on('click', function(e) {
        e.preventDefault();
        var id = $(this).find('.owl-item.active ul').data('id');
        $.ajax({
            type: 'DELETE',
            url: "/remove-product-to-compare",
            data: {
                id: this.id
            },
            success: function(data){
                console.log("🚀 ~ file: main.js ~ line 838 ~ $ ~ data", data)
                return
                $('.compare__products-carousel').html(data)
            }
        }).done(function() {
            setTimeout(function(){
                $("#overlay").fadeOut(300), 500;
            }, 500);
        });

    });
    $(".compare__products .compare__products-carousel .owl-stage-outer .owl-stage").on('click', function (e){


        e.preventDefault();
        return
        // let id = $(this).attr('data-id')
        // console.log("🚀 ~ file: index.blade.php ~ line 124 ~ id", id)
        // $.ajax({
        //     type: 'DELETE',
        //     url: '{{ route('remove.from.product_to_compare') }}',
        //     data: {
        //         id: id
        //     },
        //     success: function(data){
        //         $('.compare__products-carousel').html(data)
        //     }
        // }).done(function() {
        //     setTimeout(function(){
        //         $("#overlay").fadeOut(300), 500;
        //     }, 500);
        // });
    });
    $(document).ajaxSend(function() {
        $("#overlay").fadeIn(300);
    });
    // AOS Js
    AOS.init();
    jQuery.noConflict();
    $("#lightSlider li").hover(function() {
        let zoom = $(this).children().attr('id')
        $("#" + zoom).ezPlus()
    });
    // $(".lslide.active #zoom-01").ezPlus();
})(jQuery)
