<?php
namespace App\ViewShares;

interface ViewShare {
    public static function handle() : array;
}
