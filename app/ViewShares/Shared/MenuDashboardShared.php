<?php

namespace App\ViewShares\Shared;

use App\Helpers\Menu as MenuHelper;
use App\ViewShares\ViewShare;
use Illuminate\Support\Arr;

class MenuDashboardShared implements ViewShare
{
    public static function ECOMMERCE_MENU()
    {
        return [
            [
                'name' => 'Dashboard',
                'id' => 'dashboard',
                'isAllow' => true,
                'icon'  => '<HomeFilled />',
                'link' => route('admin.dashboard'),
            ],
            [
                'name' => 'Người dùng',
                'id' => 'user',
                'isAllow' => true,
                'icon' => '<UserFilled />',
                'child' => [
                    [
                        'name' => 'Quản lý người dùng',
                        'id' => 'users_management',
                        'link' => route('admin.users.index'),
                    ],
                ]
            ],
            [
                'name' => 'Sản phẩm',
                'id' => 'product',
                'isAllow' => true,
                'icon' => '<DataBoard />',
                'child' => [
                    [
                        'name' => 'Quản lý sản phẩm',
                        'id' => 'product_management',
                        'link' => route('admin.products.index'),
                    ],
                    [
                        'name' => 'Quản lý danh mục sản phẩm',
                        'id' => 'product_management',
                        'link' => route('admin.product_category.index'),
                    ],
                ]
            ],
            [
                'name' => 'Khách hàng',
                'id' => 'customer',
                'isAllow' => true,
                'icon' => '<DataBoard />',
                'child' => [
                    [
                        'name' => 'Quản lý khách hàng',
                        'id' => 'customer_management',
                        'link' => route('admin.customers.index'),
                    ],

                ]
            ],
            [
                'name' => 'Thống kê',
                'id' => 'donate',
                'isAllow' => true,
                'icon' => '<DataBoard />',
                'child' => [
                    [
                        'name' => 'Thống kê donate',
                        'id' => 'donate_management',
                        'link' => route('admin.donates.index'),
                    ],

                ]
            ],
            [
                'name' => 'Quyền',
                'id' => 'role',
                'isAllow' => true,
                'child' => [
                    [
                        'name' => 'Phân quyền',
                        'id' => 'role_management',
                        'link' => route('admin.roles.index'),
                    ],
                ]
            ],
        ];
    }

    public static function handle($isFlatten = false): array
    {
        $user = request()->user();

        if (is_null($user)) {
            return [];
        }

        $menus = [
            'dashboardMenu' => static::dashboardMenu($user),
        ];
        return $isFlatten ? Arr::flatten($menus, 1) : $menus;
    }

    private static function menu($menus, $user)
    {
        if ($user->hasRole('super_admin') || $user->hasRole('admin')) {
            return $menus;
        }

    }

    private static function dashboardMenu($user)
    {
        return static::ECOMMERCE_MENU();
    }


}
