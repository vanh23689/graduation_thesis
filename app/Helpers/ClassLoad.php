<?php

namespace App\Helpers;

use App\ViewShares\ViewShare;

class ClassLoad {

    public static function loadViewShareData() {
        $classes = array_slice(scandir(app_path()."/ViewShares/Shared"), 2);
        $classMap ="\\App\\ViewShares\\Shared";

        $combine = [];

        foreach ($classes as $value) {
            $fileName = pathinfo($value, PATHINFO_FILENAME);
            $class = "$classMap\\$fileName";

            if (!class_exists($class) || !is_a($class, ViewShare::class, true)) {
                continue;
            }

            $instance = app($class);
            $combine = array_merge($combine, $instance->handle());
        }

        return $combine;
    }
}
