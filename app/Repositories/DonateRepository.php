<?php

namespace App\Repositories;

use Exception;
use App\Models\Donate;
use App\Interfaces\DonateInterface;

class DonateRepository implements DonateInterface
{
    public function getAll(){
        return Donate::query()
                    ->paginate(10);
    }

    public function getById($id)
    {
        return Donate::findOrFail($id);
    }

    public function delete($id)
    {
        try {
            Donate::destroy($id);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function create(array $attributes)
    {
        return Donate::create($attributes);
    }

    public function update($id, array $attributes)
    {
        return Donate::whereId($id)->update($attributes);
    }
}
