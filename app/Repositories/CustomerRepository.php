<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Interfaces\CustomerInterface;

class CustomerRepository implements CustomerInterface
{
    public function getAll()
    {
        return Customer::query()
                    ->paginate(10);
    }
    public function getById($id)
    {

    }

    public function delete($id)
    {

    }

    public function create(array $attributes)
    {

    }

    public function update($id, array $attributes)
    {

    }

}
