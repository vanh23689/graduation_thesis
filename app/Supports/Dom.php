<?php

namespace App\Supports;

use Symfony\Component\DomCrawler\Crawler;

class Dom
{
    /**
     * Get text of all elements by xpath
     *
     * @param  \Symfony\Component\DomCrawler\Crawler $dom
     * @param  string $xpath
     * @return array<string>
     */
    public static function getElements(Crawler $dom, string $xpath): array
    {
        return $dom->filterXPath($xpath)
            ->each(function (Crawler $node) {
                return $node->text();
            });
    }

    /**
     * Get text of first element by xpath
     *
     * @param  \Symfony\Component\DomCrawler\Crawler $dom
     * @param  string $xpath
     * @return string|null
     */
    public static function getFirstElement(Crawler $dom, string $xpath): ?string
    {
        return static::getElements($dom, $xpath)[0] ?? null;
    }

    /**
     * Get text of pair elements by xpath
     *
     * @param  \Symfony\Component\DomCrawler\Crawler $dom
     * @param  string $xpath
     * @return array<string,?string>
     */
    public static function getPairElements(Crawler $dowm, string $xpath): array
    {
        $nodes = self::getElements($dowm, $xpath);

        if (count($nodes) % 2 === 1) {
            array_push($nodes, null);
        }

        return collect($nodes)->nth(2)->combine(collect($nodes)->nth(2, 1))->toArray();
    }
}
