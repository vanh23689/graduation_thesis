<?php

namespace App\Interfaces;

interface DonateInterface
{
    public function getAll();
    public function getById($id);
    public function delete($id);
    public function create(array $attributes);
    public function update($id, array $attributes);
}
