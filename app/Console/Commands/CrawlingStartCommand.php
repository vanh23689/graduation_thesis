<?php

namespace App\Console\Commands;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Spatie\Crawler\Crawler;
use App\Crawlers\CrawlProfile;
use App\Crawlers\CrawlableSite;
use App\Crawlers\CrawlObserver;
use Illuminate\Console\Command;
use App\Crawlers\RedisCrawlQueue;
use Spatie\Browsershot\Browsershot;
use Illuminate\Support\Facades\File;
use App\Crawlers\MustExecuteJavascript;
use Illuminate\Contracts\Redis\Connection;
use Spatie\Crawler\CrawlQueues\ArrayCrawlQueue;

class CrawlingStartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawling:start {site?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start crawling a website';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = Arr::random(File::allFiles(public_path('../app/Crawlers/Sites')));
        $path = public_path('../app/Crawlers/Sites/');
        $initial = ['/', '.php'];
        $repalce = ['\\'];
        $site = str_replace($initial, $repalce, str_replace($path, '', $files->getPathname()));

        // $site = Str::studly($this->argument('site'));
        $siteClassName = "\\App\\Crawlers\\Sites\\{$site}";


        if (!class_exists($siteClassName)) {
            $this->error("Site class '{$siteClassName}' DON'T exists");
            return 1;
        }
        if (!is_a($siteClassName, CrawlableSite::class, true)) {
            $this->error("Site class '{$siteClassName}' MUST implements '\\App\\Crawlers\\CrawlableSite' interface");
            return 2;
        }

        // phpcs:disable Generic.Files.LineLength
        $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.3 Safari/605.1.15';
        // phpcs:enable

        /** @var \App\Crawlers\CrawlableSite $siteInstance */
        $siteInstance = app($siteClassName);

        /** @var \Illuminate\Contracts\Redis\Connection $redisConnection */
        $redisConnection = app(Connection::class);

        // print_r($siteInstance->export(
        //     $siteInstance->startUrl(),
        //     new Response()
        // ));

        $crawler = Crawler::create($siteInstance->clientOptions())
            ->doNotExecuteJavaScript()
            ->setUserAgent($userAgent)
            ->setConcurrency(5)
            ->setDelayBetweenRequests(350)
            ->setCrawlQueue(new RedisCrawlQueue($redisConnection, strtolower($site)))
            // ->setCrawlQueue(new ArrayCrawlQueue($redisConnection, strtolower($site)))
            ->setCrawlObserver(new CrawlObserver($siteInstance, app('log')))
            ->setCrawlProfile(new CrawlProfile($siteInstance))
            ->ignoreRobots();

            if (is_a($siteInstance, MustExecuteJavascript::class)) {
            $crawler->executeJavaScript()->setBrowsershot((new Browsershot())->noSandbox());
        }

        $crawler->startCrawling($siteInstance->startUrl());

        $this->info('Horizon started successfully.');

    }
}
