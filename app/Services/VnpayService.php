<?php
namespace App\Services;

use Exception;
use App\Models\Donate;
use Illuminate\Http\Request;


class VnpayService
{
    public function ipn(Request $request){
        $data = $request->all();
        $vnp_Url = config('vnp.vnp_Url');
        $vnp_TmnCode = config('vnp.vnp_TmnCode');//Mã website tại VNPAY
        $vnp_HashSecret = config('vnp.vnp_HashSecret'); //Chuỗi bí mật
        $inputData = array();
        $returnData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }
        $vnp_SecureHash = !empty($inputData['vnp_SecureHash']) ? $inputData['vnp_SecureHash'] : '';
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }

        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);
        $vnpTranId = !empty($inputData['vnp_TransactionNo']); //Mã giao dịch tại VNPAY
        $vnp_BankCode = !empty($inputData['vnp_BankCode']); //Ngân hàng thanh toán
        $vnp_Amount = !empty($inputData['vnp_Amount']); // Số tiền thanh toán VNPAY phản hồi
        $Status = 0; // Là trạng thái thanh toán của giao dịch chưa có IPN lưu tại hệ thống của merchant chiều khởi tạo URL thanh toán.
        $orderId = !empty($inputData['vnp_TxnRef']);
        foreach(Donate::all() as $val){
            $val->order_id == $inputData['vnp_TxnRef'] ? $Status = 1 : 0;
        }
        try {
            //Check Orderid
            //Kiểm tra checksum của dữ liệu
            if ($secureHash == $vnp_SecureHash) {
                //Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
                //Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
                //Giả sử: $order = mysqli_fetch_assoc($result);
                // $order = NULL;
                if ($request['vnp_TxnRef'] != NULL) {
                    if($request["vnp_Amount"] == $vnp_Amount) //Kiểm tra số tiền thanh toán của giao dịch: giả sử số tiền kiểm tra là đúng. //$order["Amount"] == $vnp_Amount
                    {
                        if ($Status == 0) {
                            if ($inputData['vnp_ResponseCode'] == '00' && $inputData['vnp_TransactionStatus'] == '00') {
                                $Status = 1; // Trạng thái thanh toán thành công
                                $donate = new Donate();
                                $donate->order_id = $inputData['vnp_TxnRef'];
                                $donate->order_info = $inputData['vnp_OrderInfo'];
                                $donate->amount = $inputData['vnp_Amount'];
                                $donate->bank_code = $inputData['vnp_BankCode'];
                                $donate->bank_no = $inputData['vnp_BankTranNo'];
                                $donate->transaction_no = $inputData['vnp_TransactionNo'];
                                $donate->transaction_status = $inputData['vnp_TransactionStatus'];
                                $donate->card_type = $inputData['vnp_CardType'];
                                $donate->pay_date = $inputData['vnp_PayDate'];
                                $donate->count_refund = 0;
                                $donate->save();
                                //Trả kết quả về cho VNPAY: Website/APP TMĐT ghi nhận yêu cầu thành công
                                $returnData['RspCode'] = '00';
                                $returnData['Message'] = 'Confirm Success';
                                return array_merge($donate->toArray(), [
                                    'secureHash' => $secureHash,
                                    'vnp_SecureHash' => $vnp_SecureHash
                                ]);
                            } else {
                                $Status = 2; // Trạng thái thanh toán thất bại / lỗi
                            }
                        }
                        return [
                            'RspCode' => '02',
                            'error' => 'Đơn hàng đã được xác nhận'
                        ];

                    }
                    else {
                        $returnData['RspCode'] = '04';
                        $returnData['Message'] = 'invalid amount';
                    }
                } else {
                    $returnData['RspCode'] = '01';
                    $returnData['Message'] = 'Order not found';
                }
            } else {
                $returnData['RspCode'] = '97';
                $returnData['Message'] = 'Invalid signature';

            }
        } catch (Exception $e) {
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknow error';
        }
        //Trả lại VNPAY theo định dạng JSON
        // echo json_encode($returnData);
    }
}
