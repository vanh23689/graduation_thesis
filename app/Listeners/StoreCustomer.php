<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Models\Customer;
use App\Events\CustomerLogin;
use App\Models\CustomerAuthentication;
use Illuminate\Support\Facades\Notification;
use App\Notifications\CustomerLoginNotification;

class StoreCustomer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerLogin  $event
     * @return void
     */
    public function handle(CustomerLogin $event)
    {
        $loginTime = Carbon::now()->toDateTimeString();
        $customerDetail = $event->customer;
        $customer = Customer::where('email', $customerDetail['email'])->first();
        $saveHistory = CustomerAuthentication::updateOrCreate(['id' => $customer->id], [
            'name' => $customer->name,
            'email' => $customer->email,
            'customer_id' => $customer->id,
            'login_time' => $loginTime
        ]);
        Notification::send($customer, new CustomerLoginNotification($saveHistory));
        return $saveHistory;

    }
}
