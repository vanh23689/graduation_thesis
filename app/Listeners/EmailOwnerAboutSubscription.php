<?php

namespace App\Listeners;

use App\Events\UserSubscribed;
use Illuminate\Support\Facades\DB;
use App\Mail\UserSubscribedMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailOwnerAboutSubscription
{
    public $delay = 60;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $purchase = $event->email;
        DB::table('newsletter')->insert([
            'email' => $purchase
        ]);

        Mail::to($purchase)->send(new UserSubscribedMessage($purchase));
    }

    public function shouldQueue(UserSubscribed $event)
    {
        return $event->email;
    }
}
