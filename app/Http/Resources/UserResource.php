<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $role = $this->roles->first();

        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'zip_code' => $this->zip_code,
            'city' => $this->city,
            'district' => $this->district,
            'ward' => $this->ward,
            'email_verified_at' => $this->email_verified_at,
            'role' => config('role.' . object_get($role, 'name')),
            'role_name' => object_get($role, 'name'),
            'avatar' => $this->avatar,
            'profile_photo_path' => Str::replace('/storage//storage', '/storage' ,Storage::url($this->profile_photo_path)),
            'password' => Hash::make($this->password),
            'status' => $this->status ? 1 : 0,
            'check_login_times' => 0,
            // 'parent_user' => $this->parentUser ? [
            //     "name" => ($this->parentUser)->name,
            //     "role" => config('role.' . object_get(($this->parentUser)->roles->first(), 'name')) ?? null,
            // ] : null,
        ];
    }
}
