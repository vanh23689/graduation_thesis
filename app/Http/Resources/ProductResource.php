<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'website' => $this->website,
            'tel' => $this->tel,
            'hotline' => $this->hotline,
            'email' => $this->email,
            'address' => $this->address,
            'historical_cost' => $this->historical_cost,
            'initial_price' => $this->initial_price,
            'current_price' => $this->current_price,
            'revenue' => $this->revenue,
            'size' => $this->size,
            'working_hours' => $this->working_hours,
            'description' => $this->description,
            'reduce' => $this->reduce,
            'insurance' => $this->insurance,
            'name_image' => $this->name_image,
            'img' => Storage::url($this->img),
            'logo' => Storage::url($this->logo),
            'cpu' => $this->cpu,
            'lcd' => $this->lcd,
            'ram' => $this->ram,
            'screen' => $this->screen,
            'weight' => $this->weight,
            'touch' => $this->touch,
            'operating_system' => $this->operating_system, // hệ điều hành
            'origin' => $this->origin,
            'version' => $this->version,
            'hdmi' => $this->hdmi, // cổng hdmi
            'zalo' => $this->zalo,
            'pin' => $this->pin,
            'bluetooth' => $this->bluetooth,
            'wifi' => $this->wifi,
            'voice' => $this->voice,
            'usb' => $this->usb,
            'graphics' => $this->graphics, // đồ họa
            'resolution' => $this->resolution, // độ phân giải
            'network_support' => $this->network_support,
            'charging_technology' => $this->charging_technology,
            'type' => $this->type, // kiểu đồ dùng
            'image_processing' => $this->image_processing, // xử lí hình ảnh
            'audio' => $this->audio, // xử lí âm thanh
            'speakerphone' => $this->speakerphone, // loa ngoài
            'headphone' => $this->headphone,
            'wattage' => $this->wattage, // công suất
            'warranty_period' => $this->warranty_period, // hạn bảo hành
            'order_status' => $this->order_status,
            'nfc' => $this->nfc,
            'vga' => $this->vga,
            'product_code' => $this->civils->product_code ?? $this->offices->product_code ?? $this->telecommunications->product_code ?? null,
            // offices
            'caching' => $this->offices->caching ?? null,
            'cpu_speed' => $this->offices->cpu_speed ?? null,
            'ram_capacity' => $this->offices->ram_capacity ?? null,
            'bus_speed' => $this->offices->bus_speed ?? null,
            'capacity' => $this->offices->capacity ?? null,
            'hard_drive' => $this->offices->hard_drive ?? null,
            'longs' => $this->offices->longs ?? null,
            'width' => $this->offices->width ?? null,
            'height' => $this->offices->height ?? null,
            'camera' => $this->offices->camera ?? null,
            'connect' => $this->offices->connect ?? null,
            'lan_standard' => $this->offices->lan_standard ?? null,
            'serial_port' => $this->offices->serial_port ?? null,
            'features' => $this->offices->features ?? null,
            'webcam' => $this->offices->webcam ?? null,
            //civils
            'deodorant' => $this->civils->deodorant ?? null,
            'processor' => $this->civils->processor ?? null,
            'total_peaker_power' => $this->civils->total_peaker_power ?? null,
            'number_of_speakers' => $this->civils->number_of_speakers ?? null,
            'smart_sharing' => $this->civils->smart_sharing ?? null,
            'dimensions_with_stand' => $this->civils->dimensions_with_stand ?? null,
            'bracket_weight' => $this->civils->bracket_weight ?? null,
            'door_number' => $this->civils->door_number ?? null,
            'freezer_capacity' => $this->civils->freezer_capacity ?? null,
            'freezer_compartment_capacity' => $this->civils->freezer_compartment_capacity ?? null,
            'snow' => $this->civils->snow ?? null,
            'refrigerator_technology' => $this->civils->refrigerator_technology ?? null,
            'tray_material' => $this->civils->tray_material ?? null,
            'outer_material' => $this->civils->outer_material ?? null,
            'automatic_ice_maker' => $this->civils->automatic_ice_maker ?? null,
            'get_outside_water' => $this->civils->get_outside_water ?? null,
            'door_bell' => $this->civils->door_bell ?? null,
            'material' => $this->civils->material ?? null,
            'regime' => $this->civils->regime ?? null,
            'steaming_tray' => $this->civils->steaming_tray ?? null,
            'minimum_cooling_rate' => $this->civils->minimum_cooling_rate ?? null,
            'average_cooling_speed' => $this->civils->average_cooling_speed ?? null,
            'maximum_cooling_speed' => $this->civils->maximum_cooling_speed ?? null,
            'rapid_cooling' => $this->civils->rapid_cooling ?? null,
            'error_guessing' => $this->civils->error_guessing ?? null,
            'auto_restart' => $this->civils->auto_restart ?? null,
            'mosquito_repellent' => $this->civils->mosquito_repellent ?? null,
            'hygroscopic' => $this->civils->hygroscopic ?? null,
            'gas' => $this->civils->gas ?? null,
            'noise_level' => $this->civils->noise_level ?? null,
            'effective_range' => $this->civils->effective_range ?? null,
            // telecommunications
            'chipset' => $this->telecommunications->chipset ?? null,
            'memory' => $this->telecommunications->memory ?? null,
            'before_camera' => $this->telecommunications->before_camera ?? null,
            'before_video' => $this->telecommunications->before_video ?? null,
            'after_camera' => $this->telecommunications->after_camera ?? null,
            'after_video' => $this->telecommunications->after_video ?? null,
            'sim' => $this->telecommunications->sim ?? null,
            'flash_light' => $this->telecommunications->flash_light ?? null,
            'fingerprint_sensor' => $this->telecommunications->fingerprint_sensor ?? null,
            'special_features' => $this->telecommunications->special_features ?? null,
            'types_of_sensors' => $this->telecommunications->types_of_sensors ?? null,
            'speaker_phone' => $this->telecommunications->speaker_phone ?? null,
            'producer' => $this->producer ?? null,
            'crawled_at' => $this->crawled_at,

        ];
    }
}
