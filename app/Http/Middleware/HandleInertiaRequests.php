<?php

namespace App\Http\Middleware;

use Inertia\Middleware;
use App\Helpers\ClassLoad;
use App\Models\User;
// use Tightenco\Ziggy\Ziggy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    private function composeViewShare()
    {
        return ClassLoad::loadViewShareData();
    }


    /**
     * Define the props that are shared by default.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        // Get permission of user login.
        $permissions = [];
        $user = $request->user();

        if ($user) {
            foreach ($user->getAllPermissions() as $permission) {
                array_push($permissions, $permission->name);
            }
        }

        return array_merge(parent::share($request),
            [
                'auth' => [
                    'user' => $request->user(),
                    'permissions' => $permissions,
                    'impersonate' => app('impersonate')->isImpersonating()
                ],
                'statistical' => [
                    'users' => User::count(),
                ]
            ],
            $this->composeViewShare(),
            [
                'flash' => function () {
                    return [
                        'success' => Session::get('success'),
                        'error' => Session::get('error'),
                    ];
                }
            ]
        );
    }
}
