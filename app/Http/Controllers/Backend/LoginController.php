<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\User;
use Inertia\Inertia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web')->except('admin.logout');
    }

    protected function showLoginForm(){
        return Inertia::render('Auth/Login', [
            'canResetPassword' => Route::has('password.request'),
            'status' => session('status'),
        ]);
    }

    protected function login(LoginRequest $request){

        $request->authenticate();
        $user = User::where('email', $request->email)->first();

        if($user->email_verified_at === null){
            return back()->with('error', 'Email chưa được xác nhận ');
        }
        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
