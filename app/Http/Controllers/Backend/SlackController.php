<?php

namespace App\Http\Controllers\Backend;

use App\Models\Customer;
use App\Http\Controllers\Controller;
use App\Notifications\SlackNotification;

class SlackController extends Controller
{
    public function slack(){
        $customer = Customer::first();
        $customer->notify(new SlackNotification());
    }
}
