<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show_customer|create_customer|update_customer|delete_customer', ['only' => ['index','show']]);
        $this->middleware('permission:create_customer', ['only' => ['create','store']]);
        $this->middleware('permission:update_customer', ['only' => ['edit','update']]);
    }

    public function index(){
        $customers = Customer::all();
        return inertia('Customers/Index', ['customers' => $customers]);
    }
}
