<?php

namespace App\Http\Controllers\Backend\Users;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Lab404\Impersonate\Models\Impersonate;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show_user|create_user|update_user|delete_user', ['only' => ['index','show']]);
        $this->middleware('permission:create_user', ['only' => ['create','store']]);
        $this->middleware('permission:update_user', ['only' => ['edit','update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return inertia('Users/Index', ['users' => $users]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return inertia('Users/Add', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function impersonate(User $user)
    {
        $currentUser = Auth::user();
        if($currentUser && ($currentUser->hasRole('super_admin') || $currentUser->hasRole('admin'))){
            $currentUser->impersonate($user);
            return redirect()->route('admin.dashboard');
        }
        return abort(404);
    }

    public function leaveImpersonate()
    {
        if (app('impersonate')->isImpersonating()) {
            auth()->user()->leaveImpersonation();

            return redirect()->route('admin.users.index');
        }

        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =  User::findOrFail($id);
        $roles = Role::all();
        $role_name = $user->roles->all();
        return inertia('Users/Edit', [
            'user_id' => $user->id,
            'roles' => $roles,
            'role_name' => $role_name
        ]);
    }
}
