<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\RoleHasPermission;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:show_role|create_role|update_role|delete_role', ['only' => ['index','store']]);
         $this->middleware('permission:create_role', ['only' => ['create','store']]);
         $this->middleware('permission:update_role', ['only' => ['edit','update']]);
    }

    public function index(){
        $roles = Role::orderBy('id','DESC')->paginate(5);
        return inertia('Roles/Index', ['roles' => $roles]);
    }

    public function create(){
        $permissions = Permission::get();
        return inertia('Roles/Add', [
            'permissions' => $permissions
        ]);
    }

    public function edit($id){
        $user = User::find($id);
        $permissions = Permission::get();
        return inertia('Roles/Edit', [
            'user_id' => $user->id,
            'permissions' => $permissions
        ]);
    }
}
