<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Donate;
use Illuminate\Http\Request;

class DonateController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show_donate|create_donate|update_donate|delete_donate', ['only' => ['index','show']]);
        $this->middleware('permission:create_donate', ['only' => ['create','store']]);
        $this->middleware('permission:update_donate', ['only' => ['edit','update']]);
    }

    public function index(){
        $donates = Donate::all();
        return inertia('Donates/Index', ['donates' => $donates]);
    }
}
