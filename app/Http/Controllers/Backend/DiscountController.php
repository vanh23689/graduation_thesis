<?php

namespace App\Http\Controllers\Backend;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\DiscoutSendEmail;

class DiscountController extends Controller
{
    public function index(){
        $customers = Customer::all();

        foreach ($customers as $customer)
        {
            DiscoutSendEmail::dispatch($customer);
        }

        return response()->json([
            'success' => true,
            'message' => 'Send email jobs have been dispatched, please run php artisan queue:work.'
        ], 200);
    }
}
