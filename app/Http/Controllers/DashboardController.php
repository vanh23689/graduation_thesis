<?php

namespace App\Http\Controllers;

use App\Events\DiscountEvent;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Notification;

class DashboardController extends Controller
{
    public function index(){
        return inertia('Dashboard');
    }

    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('admin.login');
    }

    public function send()
    {
    	$user = User::first();

        $delay = now()->addMinutes(2);

        Notification::send($user, (new EmailNotification($user))->delay($delay));

        dd('Notification sent!');
    }

}
