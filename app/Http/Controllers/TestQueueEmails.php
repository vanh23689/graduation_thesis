<?php

namespace App\Http\Controllers;

use App\Jobs\TestSendEmail;
use App\Models\User;


class TestQueueEmails extends Controller
{
     /**
    * test email queues
    **/
    public function sendTestEmails(){
        $users = User::all();
        foreach($users as $user){
            TestSendEmail::dispatch($user);
        }

        return response()->json([
            'success' => true,
            'message' => 'Send email jobs have been dispatched, please run php artisan queue:work.'
        ], 200);
    }
}
