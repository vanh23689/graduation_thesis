<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Interfaces\CustomerInterface;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public CustomerInterface $customerRepository;

    public function __construct(CustomerInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->middleware('permission:delete_product', ['only' => ['destroy']]);
    }

    public function index()
    {
        $customers = Customer::all();

        $male = $customers->reject(function ($customer) {
            return $customer->gender !== "male";
        })->count();
        $female = $customers->reject(function ($customer) {
            return $customer->gender !== "female";
        })->count();
        $other = $customers->reject(function ($customer) {
            return $customer->gender !== "other";
        })->count();

        return response()->json([
                'customers' => $customers,
                'gender' => ['Name', 'Nữ', 'Khác'],
                'dataGender' => [ ($male / ($male+$female+$other) *100), ($female / ($male+$female+$other) *100), ($other / ($male+$female+$other) *100)],
            ]
        );
    }

    public function showCustomer(): JsonResponse
    {
        return response()->json($this->customerRepository->getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customers = Customer::find($id)->delete();
        return response()->json($customers);
    }
}
