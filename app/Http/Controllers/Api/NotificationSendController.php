<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;

class NotificationSendController extends Controller
{
    public function getNotification(){
        $notifications = Notification::query()
                                    ->take(5)
                                    ->latest()
                                    ->with('customer_authentications')
                                    ->get();

        return NotificationResource::collection($notifications);
    }

    public function readAt($id){
        $readAt = Notification::where('id', $id)->first();
        $readAt->read_at = Carbon::now();
        $readAt->save();
    }
}
