<?php

namespace App\Http\Controllers\Api;

use Exception;
use Carbon\Carbon;
use App\Models\Donate;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\DonateInterface;
use App\Http\Controllers\Controller;
use App\Http\Resources\DonateResource;

class DonateController extends Controller
{
    public DonateInterface $donateRepository;

    public function __construct(DonateInterface $donateRepository)
    {
        $this->donateRepository = $donateRepository;
        $this->middleware('permission:delete_product', ['only' => ['destroy']]);
    }

    public function getListDonate()
    {
        return DonateResource::collection($this->donateRepository->getAll());
    }

    public function edit(){

    }

    public function getDetailDonateToTime(Request $request){
        $account = $request->user();
        if (!$account) {
            return;
        }

        $sinceTime = Carbon::now()->subDays(7);
        $untilTime = Carbon::now();
        $donateInsight = Donate::select('id', 'amount', 'bank_code', 'card_type', 'pay_date', 'created_at')
                                ->whereBetween('pay_date', [$sinceTime->startOfDay()->format('YmdHis'), $untilTime->startOfDay()->format('YmdHis')])
                                ->orderBy('pay_date', 'ASC')
                                ->get();
        $period = CarbonPeriod::create($sinceTime, $untilTime);
        $dataDonate = $this->getDataPerDays($period, $donateInsight, 'pay_date', 'amount');
        $dataLabels = [];
        foreach ($period as $key => $date) {
            $dateFormat = $date->format("Y/m/d");
            $dataLabels[$key] = $dateFormat;
        }

        return response()->json([
            'dataDonate' => $dataDonate,
            'dataLabels' => $dataLabels
        ], 200);

    }

    public static function getDataPerDays($period, $items, $fieldTime, $field)
    {
        $dataPeriod = [];

        foreach($period as $key => $date){
            $totalField = $items
                        ->where($fieldTime, '>=', $date->startOfDay()->format('YmdHis'))
                        ->where($fieldTime, '<=', $date->endOfDay()->format('YmdHis'));

            if($totalField->count() != 0){
                $dataPeriod[$key] = $totalField->sum($field);
            }
            else
            {
                // Dữ liệu crawl về thì lấy dữ liệu crawl của ngày trước đó đầu tiên
                $dataNearest = $items->where($fieldTime, '>', $date->format('YmdHis'))
                                        ->sortByDesc($fieldTime)
                                        ->first();
                // Nếu tồn tại data ngày gần nhất
                if ($dataNearest) {
                    $dateNearest = Carbon::createFromTimestamp($dataNearest->$fieldTime);
                    $totalFieldOld = $items
                        ->where($fieldTime, '>=', $dateNearest->startOfDay()->format('YmdHis'))
                        ->where($fieldTime, '<=', $dateNearest->endOfDay()->format('YmdHis'))
                        ->sum($field);
                    $dataPeriod[$key] = $totalFieldOld ? $totalFieldOld : 0;

                } else {
                    $dataPeriod[$key] = 0;
                }
            }
        }
        $maxDonate = collect($dataPeriod)->max();

        return [
            'dataPeriod' => $dataPeriod,
            'maxDonate' => $maxDonate
        ];
    }

    public function destroy($id)
    {
        $this->donateRepository->delete($id);

        return response()->json(
            [
                'status' => true,
                'msg' => __('delete_donate_successfully')
            ], Response::HTTP_NO_CONTENT);
    }


}
