<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:delete_product', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::query()
            ->with(['civils', 'offices', 'telecommunitions'])
            ->paginate($request->input('limit', 10));
        return ProductResource::collection($products);
    }


    public function uploadLogoProduct(Request $request, $id)
    {
        $request->validate([
            'name_image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        try {
            $product_logo = Product::findOrFail($id);
            if($request->file()){
                $file_path = Storage::put('upload_logo', $request->file('name_image'));
                $product_logo->name_image = time().'_'.$request->name_image->getClientOriginalName();
                $product_logo->logo = $file_path;
                $product_logo->save();

                return response()->json([
                    'status' => true,
                    'msg' => __('edit_user_successfully')
                ]);
            }
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function uploadImageProduct(Request $request, $id)
    {
        $request->validate([
            'img' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        try {
            $product = Product::findOrFail($id);
            if($request->file()){
                $file_path = Storage::put('uploads', $request->file('image_product'));
                $product->name_image = time().'_'.$request->name_image->getClientOriginalName();
                $product->image = $file_path;
                $product->save();
                return response()->json([
                    'status' => true,
                    'msg' => __('edit_user_successfully')
                ]);
            }
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            $data = $request->all();
            $product = Product::create([
                'name' => $data['name'],
                'website' => $data['website'],
            ]);
            DB::commit();
            return ProductResource::make($product);
        }catch (Exception $e) {
            DB::rollBack();
            return response()->json($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return ProductResource::make($product);
    }

    // Top 10 products
    public function getTopProducts(){
        $products = Product::query()
                            ->orderBy('total_views', 'DESC')
                            ->limit(10)
                            ->with(['categories'])
                            ->get();

        return response()->json([
            'products' => $products
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        DB::beginTransaction();

        try {
            $product = Product::findOrFail($id);
            $product->update($data);
            return response()->json($product);
            DB::commit();
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userAuthen = auth()->user()->roles->first();

        try {
            $product = Product::findOrFail($id);
            $product->delete();
            return response()->json([
                'status' => true,
                'msg' => __('delete_product_successfully')
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }
}
