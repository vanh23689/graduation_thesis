<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:delete_user', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query()
            ->paginate($request->input('limit', 10));
        return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);

        DB::beginTransaction();
        try {
            $user = User::create($data);
            if($request->file()){
                $file_path = Storage::put('uploads', $request->file('avatar'));
                $user->avatar = time().'_'.$request->avatar->getClientOriginalName();
                $user->profile_photo_path = $file_path;
                $user->save();
            }
            $user->assignRole($request->input('role_name'));
            DB::commit();
            return UserResource::make($user);
        }catch (Exception $e) {
            DB::rollBack();
            return response()->json($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return UserResource::make($user);
    }

    public function upload(Request $request, $id){

        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        try {
            $user = User::findOrFail($id);
            if($request->file()){
                $file_path = Storage::put('uploads', $request->file('avatar'));
                $user->avatar = time().'_'.$request->avatar->getClientOriginalName();
                $user->profile_photo_path = $file_path;
                $user->save();
                return response()->json([
                    'status' => true,
                    'msg' => __('edit_user_successfully')
                ]);
            }
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->update(Arr::except($request->all(), ['role', 'role_name']));
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->input('role_name'));
            DB::commit();
            return response()->json($user);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userAuthen = auth()->user()->roles->first();
        try {
            $user = User::findOrFail($id);
            $rolesId = $user->roles->first();
            if (!$user->hasRole(['super_admin', 'admin']) || $userAuthen->id == $rolesId->id) {
                $user->delete();
            } else {
                throw new FailException(__('you_do_not_have_permission_to_delete_this_account'));
            }
            return response()->json([
                'status' => true,
                'msg' => __('delete_user_successfully')
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }
}
