<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class GetAddressController extends Controller
{
    public function getCity()
    {
        $url = Http::get('https://provinces.open-api.vn/api/p/');
        return json_decode($url);
    }

    public function getProvince($code)
    {
        $url = Http::get('https://provinces.open-api.vn/api/p/' . $code . '?depth=2');
        return json_decode($url);
    }

    public function getWard($code)
    {
        $url = Http::get('https://provinces.open-api.vn/api/d/' . $code . '?depth=2');
        return json_decode($url);
    }


}
