<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Donate;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use function Termwind\render;

use App\Services\VnpayService;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Http;

class DonateController extends Controller
{
    protected $vnpayService;

    public function __construct(VnpayService $vnpayService)
    {
        $this->vnpayService = $vnpayService;
    }
    public function index()
    {
        $categories = ProductCategory::all();
        return view('site.pages.payments.vnpay', compact('categories'));
    }
    public function create(Request $request)
    {
        $vnp_Url = config('vnp.vnp_Url');
        $vnp_TmnCode = config('vnp.vnp_TmnCode');//Mã website tại VNPAY
        $vnp_HashSecret = config('vnp.vnp_HashSecret'); //Chuỗi bí mật
        $vnp_Returnurl = "http://localhost/site/vnpay_php/vnpay_return";
        $vnp_TxnRef = uniqid(); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $this->convertDescription($request->description);
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request->money * 100;
        $vnp_Locale = $request->language;
        $vnp_BankCode = $request->bankCode;
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        //Add Params of 2.0.1 Version
        $vnp_ExpireDate = Carbon::now()->addMinute(15)->format('YmdHis');
        //Billing
        $fullName = trim($request->name);
        if (isset($fullName) && trim($fullName) != '') {
            $name = explode(' ', $fullName);
            $vnp_Bill_FirstName = array_shift($name);
            $vnp_Bill_LastName = array_pop($name);
        }
        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_Bill_FirstName"=>$vnp_Bill_FirstName,
            "vnp_Bill_LastName"=>$vnp_Bill_LastName,
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
            "vnp_ExpireDate"=>$vnp_ExpireDate,
        );
        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }

        //var_dump($inputData);
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }

        $returnData = array('code' => '00'
            , 'message' => 'success'
            , 'data' => $vnp_Url);
            if (isset($request['redirect'])) {
                header('Location: ' . $vnp_Url);
                die();
            } else {
                echo json_encode($returnData);
            }
            // vui lòng tham khảo thêm tại code demo

    }

    public function return(Request $request)
    {
        $categories = ProductCategory::all();
        $dataResponse = $this->vnpayService->ipn($request);
        return view('site.pages.payments.return', compact('categories', 'dataResponse'));
    }

    public function convertDescription($value){
        $unwanted_array = [
            'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ề'=>'e', 'ế'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y'
        ];
        $str = strtr($value, $unwanted_array );
        return strtoupper($str);
    }

    public function transaction(Request $request)
    {
        $categories = ProductCategory::all();
        $vnp_TmnCode = config('vnp.vnp_TmnCode');//Mã website tại VNPAY
        $vnp_HashSecret = config('vnp.vnp_HashSecret'); //Chuỗi bí mật
        $vnp_apiUrl = config('vnp.vnp_Apiurl');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $orderid = $request["orderid"];
            $hashSecret = $vnp_HashSecret;
            $ipaddr = $_SERVER['REMOTE_ADDR'];
            $responseData = array();
            $inputData = array(
                "vnp_Version" => '2.1.0',
                "vnp_Command" => "querydr",
                "vnp_TmnCode" => $vnp_TmnCode,
                "vnp_TxnRef" => $orderid,
                "vnp_OrderInfo" => 'Noi dung thanh toan',
                "vnp_TransDate" => $request['paymentdate'],
                "vnp_CreateDate" => date('YmdHis'),
                "vnp_IpAddr" => $ipaddr
            );

            ksort($inputData);
            $query = "";
            $i = 0;
            $hashdata = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                } else {
                    $hashdata .= urlencode($key) . "=" . urlencode($value);
                    $i = 1;
                }
                $query .= urlencode($key) . "=" . urlencode($value) . '&';
            }

            $vnp_apiUrl = $vnp_apiUrl . "?" . $query;
            if (isset($hashSecret)) {
                $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);
                $vnp_apiUrl .= 'vnp_SecureHash=' . $vnpSecureHash;
            }


            $ch = curl_init($vnp_apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $responseData = curl_exec($ch);
            curl_close($ch);
            $responseData = Str::of(Str::replace("vnp_", "", $responseData))->explode('&');

            return view('site.pages.payments.return_search', compact('categories', 'responseData'));
        }
    }

    public function refund(Request $request)
    {
        $categories = ProductCategory::all();
        $vnp_TmnCode = config('vnp.vnp_TmnCode');//Mã website tại VNPAY
        $vnp_HashSecret = config('vnp.vnp_HashSecret'); //Chuỗi bí mật
        $vnp_apiUrl = config('vnp.vnp_Apiurl');
        $inputData = array();
        $responseData = array();


        try{
            foreach(Donate::all() as $val){
                if($val->order_id == $request->orderid_refund){

                    if($val->count_refund <= 2){
                        Donate::where('order_id', $request->orderid_refund)
                                ->update(['count_refund' => $val->count_refund = $val->count_refund+1]);
                        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                            $amount = ($request["amount"]);
                            $ipaddr = $_SERVER['REMOTE_ADDR'];
                            $inputData = array(
                                "vnp_Version" => '2.1.0',
                                "vnp_TransactionType" => $request["trantype"],
                                "vnp_Command" => "refund",
                                "vnp_CreateBy" => $request["mail"],
                                "vnp_TmnCode" => $vnp_TmnCode,
                                "vnp_TxnRef" => $request["orderid_refund"],
                                "vnp_Amount" => $amount,
                                "vnp_OrderInfo" => $request->description_refund,
                                "vnp_TransDate" => $request['paymentdate'],
                                "vnp_CreateDate" => date('YmdHis'),
                                "vnp_IpAddr" => $ipaddr
                            );
                            ksort($inputData);
                            $query = "";
                            $i = 0;
                            $hashdata = "";

                            foreach ($inputData as $key => $value) {
                                if ($i == 1) {
                                    $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                                } else {
                                    $hashdata .= urlencode($key) . "=" . urlencode($value);
                                    $i = 1;
                                }
                                $query .= urlencode($key) . "=" . urlencode($value) . '&';
                            }

                            $vnp_apiUrl = $vnp_apiUrl . "?" . $query;
                            if (isset($vnp_HashSecret)) {
                                $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);
                                $vnp_apiUrl .= 'vnp_SecureHash=' . $vnpSecureHash;
                            }
                            $ch = curl_init($vnp_apiUrl);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                            $responseData = curl_exec($ch);
                            curl_close($ch);
                            $responseData = Str::of(Str::replace("vnp_", "", $responseData))->explode('&');
                            return view('site.pages.payments.return_refund', compact('responseData', 'categories'));

                        }
                    }else{
                        $responseData['error'] = 'Hoàn tiền quá số lần cho phép';
                        return view('site.pages.payments.return_refund', compact('categories', 'responseData'));
                    }
                }else{
                    $responseData['error'] = 'Mã đơn hàng nhập sai';
                    return view('site.pages.payments.return_refund', compact('categories', 'responseData'));
                }
            }

        }catch(Exception $e){
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknow error';
        }

    }

    public function atm_momo(Request $request)
    {
        $endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor";


        $partnerCode = config('momo.partner_Code');
        $accessKey = config('momo.access_Key');
        $secretKey = config('momo.secret_Key');
        $orderInfo = "Thanh toán qua MoMo";
        $amount = "10000";
        $orderId = time() ."";
        $returnUrl = "http://localhost:8000/atm/result_atm.php";
        $notifyurl = "http://localhost:8000/atm/ipn_momo.php";
        // Lưu ý: link notifyUrl không phải là dạng localhost
        $bankCode = "SML";


        $orderInfo = $request["orderInfo"];
        $amount = $request["amount"];
        $bankCode = $request['bankCode'];
        $returnUrl = $request['returnUrl'];
        $requestId = time()."";
        $requestType = "payWithMoMoATM";
        $extraData = "";
        //before sign HMAC SHA256 signature
        $rawHashArr =  array(
                        'partnerCode' => $partnerCode,
                        'accessKey' => $accessKey,
                        'requestId' => $requestId,
                        'amount' => $amount,
                        'orderId' => $orderId,
                        'orderInfo' => $orderInfo,
                        'bankCode' => $bankCode,
                        'returnUrl' => $returnUrl,
                        'notifyUrl' => $notifyurl,
                        'extraData' => $extraData,
                        'requestType' => $requestType
                        );
        // echo $serectkey;die;
        $rawHash = "partnerCode=".$partnerCode."&accessKey=".$accessKey."&requestId=".$requestId."&bankCode=".$bankCode."&amount=".$amount."&orderId=".$orderId."&orderInfo=".$orderInfo."&returnUrl=".$returnUrl."&notifyUrl=".$notifyurl."&extraData=".$extraData."&requestType=".$requestType;
        $signature = hash_hmac("sha256", $rawHash, $secretKey);

        $data =  array('partnerCode' => $partnerCode,
                        'accessKey' => $accessKey,
                        'requestId' => $requestId,
                        'amount' => $amount,
                        'orderId' => $orderId,
                        'orderInfo' => $orderInfo,
                        'returnUrl' => $returnUrl,
                        'bankCode' => $bankCode,
                        'notifyUrl' => $notifyurl,
                        'extraData' => $extraData,
                        'requestType' => $requestType,
                        'signature' => $signature);
        $result = execPostRequest($endpoint, json_encode($data));
        $jsonResult = json_decode($result,true);  // decode json

        error_log( print_r( $jsonResult, true ) );
        header('Location: '.$jsonResult['payUrl']);
    }

    public function result_atm()
    {

    }
}
