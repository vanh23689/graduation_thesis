<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index(){
        $laptop_id = ProductCategory::where('name', 'Laptop')->first()->id ?? null;
        $tablet_id = ProductCategory::where('name', 'Tablet')->first()->id ?? null;
        $mobile_id = ProductCategory::where('name', 'Smartphone')->first()->id ?? null;
        $airConditioner_id = ProductCategory::where('name', 'Air-conditioner')->first()->id ?? null;
        $cooker_id = ProductCategory::where('name', 'Cooker')->first()->id ?? null;
        $television_id = ProductCategory::where('name', 'Television')->first()->id ?? null;
        $tivi_id = ProductCategory::where('name', 'TV')->first()->id ?? null;
        $pc_id = ProductCategory::where('name', 'Personal Computer')->first()->id ?? null;

        $laptop = Product::where('category_id', $laptop_id)->get();
        $tablet = Product::where('category_id', $tablet_id)->get();
        $mobile = Product::where('category_id', $mobile_id)->get();
        $airConditioner = Product::where('category_id', $airConditioner_id)->get();
        $cooker = Product::where('category_id', $cooker_id)->get();
        $televisions = Product::where('category_id', $television_id)->get();
        $tivi = Product::where('category_id', $tivi_id)->get();
        $pc = Product::where('category_id', $pc_id)->get();

        $minPrice = [
            'laptop' => !$laptop->isEmpty() ? $laptop->min('current_price') : '',
            'tablet' => !$tablet->isEmpty() ? $tablet->min('current_price') : '',
            'mobile' => !$mobile->isEmpty() ? $mobile->min('current_price') : '',
            'airConditioner' => !$airConditioner->isEmpty() ? $airConditioner->min('current_price') : '',
            'cooker' => !$cooker->isEmpty() ? $cooker->min('current_price') : '',
            'televisions' => !$televisions->isEmpty() ? $televisions->min('current_price') : '',
            'tivi' => !$tivi->isEmpty() ? $tivi->min('current_price') : '',
            'pc' => !$pc->isEmpty() ? $pc->min('current_price') : ''
        ];

        $maxPrice = [
            'laptop' => !$laptop->isEmpty() ? $laptop->max('current_price') : '',
            'tablet' => !$tablet->isEmpty() ? $tablet->max('current_price') : '',
            'mobile' => !$mobile->isEmpty() ? $mobile->max('current_price') : '',
            'airConditioner' => !$airConditioner->isEmpty() ? $airConditioner->max('current_price') : '',
            'cooker' => !$cooker->isEmpty() ? $cooker->max()->current_price : '',
            'televisions' => !$televisions->isEmpty() ? $televisions->max('current_price') : '',
            'tivi' => !$tivi->isEmpty() ? $tivi->max('current_price') : '',
            'pc' => !$pc->isEmpty() ? $pc->max('current_price') : '',
        ];
        if(is_countable(Session::get('cart')) && count(Session::get('cart'))){
            $count = [];
        }
        $categories = ProductCategory::all();
        $category = ProductCategory::first();
        $categories_compare = [];

        if(!is_null(Session::get('cart'))){
            foreach(Session::get('cart') as $value){
                array_push($categories_compare, $value['category']);
            }
        }

        $categories_compare = array_unique($categories_compare);

        session()->put($minPrice, $maxPrice);



        return view('site.pages.home.index', compact(   'laptop', 'laptop_id',
                                                        'tablet', 'tablet_id',
                                                        'mobile', 'mobile_id',
                                                        'pc', 'pc_id',
                                                        'airConditioner', 'airConditioner_id',
                                                        'cooker', 'cooker_id',
                                                        'televisions', 'television_id',
                                                        'tivi', 'tivi_id' ,
                                                        'minPrice', 'maxPrice',
                                                        'categories_compare', 'category', 'categories'
        ));
    }

    public function show($id){
        $product = Product::findOrFail($id);
        $category = Product::where('category_id', $product->categories->id)->take(3)->get();


        return response()->json([
            'data' => [
                $product,
                $product->offices,
                $product->civils,
                $product->telecommunitions
            ],
            'category' => $category,

        ]);
    }

    public function logout(Request $request){
        Auth::guard('customer')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('home.index');
    }
}
