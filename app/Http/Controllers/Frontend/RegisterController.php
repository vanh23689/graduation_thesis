<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Customer;
use Illuminate\Support\Str;
use App\Mail\CustomerRegister;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Notifications\CustomerNotification;
use App\Http\Requests\CustomerCreateRequest;
use Illuminate\Support\Facades\Notification;

class RegisterController extends Controller
{
    public function register(CustomerCreateRequest $request){
        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // Mail::to('compare_website@gmail.com')->send(new CustomerRegister([$customer->id, $request]));
        Notification::send($customer, new CustomerNotification($customer));
        if($customer){
            Auth::guard('customer')->login($customer);
            $request->session()->regenerate();
            return response()->json(['success'=>'Đăng ký thành công, vui lòng xác nhận email.']);
        }

        response()->json(['errors' => $customer->errors()]);
    }
}
