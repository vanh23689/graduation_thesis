<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Events\UserSubscribed;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsLetterRequest;

class NewsLetterController extends Controller
{
    public function subscribe(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:newsletter,email'
        ]);

        if($validator->fails()){
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        }else{
            event(new UserSubscribed($request->email));
        }

    }
}
