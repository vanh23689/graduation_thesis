<?php

namespace App\Http\Controllers\Frontend;

use App\Events\CustomerLogin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CustomerLoginRequest;

class LoginController extends Controller
{
    public function login(CustomerLoginRequest $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::guard('customer')->attempt($credentials)) {
            event(new CustomerLogin($credentials));
            $request->session()->regenerate();
            return redirect()->route('home.index');
        }
    }
}
