<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use App\Providers\RouteServiceProvider;

class VerifyCustomerEmailController extends Controller
{
    public function verify(Request $request, $id, $token){
        if (str_contains($token, $request->token)) {
            $email_verified_at = Customer::where('id', (int)$id)->first();
            $email_verified_at->email_verified_at = Carbon::now();
            $email_verified_at->save();
            return redirect()->intended(RouteServiceProvider::DASHBOARD);
        }

        return redirect()->intended(RouteServiceProvider::DASHBOARD);
    }
}
