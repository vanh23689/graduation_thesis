<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CompareController extends Controller
{
    public function index(){
        if(is_countable(Session::get('cart')) && count(Session::get('cart'))){
            $count = [];
        }
        $categories = ProductCategory::all();
        $category = $categories->first();
        $categories_compare = [];

        if(!is_null(Session::get('cart'))){
            foreach(Session::get('cart') as $value){
                array_push($categories_compare, $value['category']);
            }
        }
        $categories_compare = array_unique($categories_compare);
        $product = Session::get('prod_to_compare');
        $prod_civil = $prod_office = $prod_telecommunition = [];
        if(!empty($product)){
            foreach($product as $prod){
                if($prod->civils){
                    $prod_civil = $prod->civils;
                }elseif($prod->offices){
                    $prod_office = $prod->offices;
                }else{
                    $prod_telecommunition = $prod->telecommunitions;
                }
            }
        }

        return view('site.pages.compare.index', compact('categories', 'categories_compare',
            'prod_civil', 'prod_office', 'prod_telecommunition'
    ));
    }

    public function postCompare(Request $request){
        foreach($request->id as $value){
            $product[] = Product::query()
                                ->where('id', intval($value))
                                ->with(['civils', 'offices', 'telecommunitions'])
                                ->first();
        }
        $products = [...$product];
        Session::put('prod_to_compare', $products);
    }

    public function getCompare(){
        return response()->json([
            'product' => Session::get('prod_to_compare')
        ]);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function removeProdToCompare(Request $request)
    {
        dd($request->id);
        if($request->id) {
            $prod_to_compare = session()->get('prod_to_compare');
            if(isset($prod_to_compare[(int)$request->id])) {
                unset($prod_to_compare[(int)$request->id]);
                session()->put('prod_to_compare', $prod_to_compare);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }
}
