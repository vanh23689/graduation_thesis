<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class WishlistController extends Controller
{
    public function index(){
        $count = [];
        if(is_countable(Session::get('cart')) && count(Session::get('cart'))){
            $count = count(Session::get('cart')) ?? [];
        }
        $categories = ProductCategory::all();
        $categories_compare = [];
        foreach ($categories as $category) {
            if(!is_null(Session::get('cart'))){
                foreach(Session::get('cart') as $value){
                    array_push($categories_compare, $value['category']);
                }
            }

        }
        $categories_compare = array_unique($categories_compare);
        return view('site.pages.wishlist.index', compact('categories_compare', 'count', 'categories'));
    }


}
