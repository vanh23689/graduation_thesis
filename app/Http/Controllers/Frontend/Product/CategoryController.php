<?php

namespace App\Http\Controllers\Frontend\Product;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\Arrange;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function show(Request $request, $id){
        // session()->flush();
        $product_categories = ProductCategory::findOrFail($id);
        $products = Product::where('category_id', $product_categories->id)->paginate(9);
        $category_name = $product_categories->name;
        $categories = ProductCategory::all();
        $categories_compare = [];
        $arrange = Arrange::all();

        if(is_countable(Session::get('cart')) && count(Session::get('cart'))){
            $count = [];
        }

        $category = ProductCategory::first();

        if(!is_null(Session::get('cart'))){
            foreach(Session::get('cart') as $value){
                array_push($categories_compare, $value['category']);
            }
        }

        $categories_compare = array_unique($categories_compare);
        if ($request->sort_by){
            if ($request->sort_by == "giam_dan"){
                $products = Product::where('category_id', $product_categories->id)->orderBy('current_price', 'DESC')->paginate(9);
            }else if ($request->sort_by == "tang_dan"){
                $products = Product::where('category_id', $product_categories->id)->orderBy('current_price', 'ASC')->paginate(9);
            }else if ($request->sort_by == "a_z"){
                $products = Product::where('category_id', $product_categories->id)->orderBy('name', 'ASC')->paginate(9);
            }else if ($request->sort_by == "z_a"){
                $products = Product::where('category_id', $product_categories->id)->orderBy('name', 'DESC')->paginate(9);
            }
        }
        return view('site.pages.products.categories', compact('products', 'category_name', 'categories', 'categories_compare', 'category', 'arrange', 'id'));
    }
}
