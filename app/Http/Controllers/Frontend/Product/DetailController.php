<?php

namespace App\Http\Controllers\Frontend\Product;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class DetailController extends Controller
{
    public function show($id, Product $product){
        $product = Product::where('id', (int)$id)->with(['images'])->first();

        $category_name = $product->categories->name;

        $product_images = empty($product->images->pluck('image_url')->toArray()) ? [$product->img] : $product->images->pluck('image_url')->toArray();

        $product_categories = Product::query()
                                    ->where('category_id', $product->categories->id)
                                    ->whereNot('website', null)
                                    ->with(['offices', 'civils', 'telecommunitions'])
                                    ->inRandomOrder()
                                    ->take(5)
                                    ->get();
        $count = [];
        if(is_countable(Session::get('cart')) && count(Session::get('cart'))){
            $count = count(Session::get('cart')) ?? [];
        }
        $categories = ProductCategory::all();
        $category = ProductCategory::first();
        $categories_compare = [];

        if(!is_null(Session::get('cart'))){
            foreach(Session::get('cart') as $value){
                array_push($categories_compare, $value['category']);
            }
        }

        $categories_compare = array_unique($categories_compare);

        if(Cookie::get($product->id) == ''){
            $cookie = Cookie::queue($product->id, 1, 60);
		    $product->total_views = $this->incrementReadCount($product->total_views);
            $product->save();
        }

        return view('site.pages.products.details.detail', compact('product', 'category_name', 'product_categories', 'count', 'categories', 'categories_compare', 'category', 'product_images'));
    }

    public function incrementReadCount($total_views){
        $total_views = $total_views + 1;
        return $total_views;
    }

}
