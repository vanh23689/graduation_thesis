<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    function getSearchAjax(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Product::where('name', 'LIKE', "%{$query}%")->take(7)->latest()->get();
            if($data){
                $output = '<ul class="dropdown-menu p-0 m-0" style="display:block; position:relative; width: 100%; border: none">';
                    foreach($data as $row)
                    {
                    $output .= '
                    <li><a href="'. $row->id .'">'.$row->name.'</a></li>';
                }
                    $output .= '</ul>';
                echo $output;
            }
            return '';

       }
    }
}
