<?php

namespace App\Http\Controllers\Frontend\Cart;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function cart($id)
    {
        $count = count(Session::get('cart'));
        if(empty(Session::get('cart'))){
            return $count++;
        }
        if(!empty(Session::get('cart'))){
            foreach(Session::get('cart') as $cart){
                if($cart['id'] === $id){
                    return $count--;
                }
                return $count++;
            }

        }

    }

    public function wishlist($id)
    {
        $count = count(Session::get('wishlist'));
        if(empty(Session::get('wishlist'))){
            return $count++;
        }
        if(!empty(Session::get('wishlist'))){
            foreach(Session::get('wishlist') as $wishlist){
                if($wishlist['id'] === $id){
                    return $count--;
                }
                return $count++;
            }

        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function addToCart(Request $request, $id)
    {
        $cart = session()->get('cart');

        $category_id = Product::find((int)$id)->category_id;
        $category = ProductCategory::find($category_id)->name;

        if(!empty($cart[$id])){
            $val_cart = collect($cart)->filter(function($value){
                return $value['id'];
            });
            foreach($cart as $c){
                foreach($val_cart as $v){
                    if($c['id'] == $v['id']){
                        unset($cart[(int)$request->id]);
                        session()->put('cart', $cart);
                    }
                }
            }
        }else{
            $cart[$id] = [
                "id" => $request->id,
                "name" => $request->name,
                "min_price" => $request->min_price,
                "max_price" => $request->max_price,
                "image" => $request->img,
                "category" => $category
            ];
        }


        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Thêm sản phẩm so sánh thành công!');
    }

    public function addToWishlist(Request $request, $id){
        $wishlist = session()->get('wishlist');
        $category_id = Product::find((int)$id)->category_id;
        $category = ProductCategory::find($category_id)->name;

        if(!empty($wishlist[$id])){
            $val_wishlist = collect($wishlist)->filter(function($value){
                return $value['id'];
            });
            foreach($wishlist as $c){
                foreach($val_wishlist as $v){
                    if($c['id'] == $v['id']){
                        unset($wishlist[(int)$request->id]);
                        session()->put('wishlist', $wishlist);
                    }
                }
            }
        }else{
            $wishlist[$id] = [
                "id" => $id,
                "name" => $request->name,
                "min_price" => $request->min_price,
                "max_price" => $request->max_price,
                "image" => $request->img,
                "website" => $request->website,
                "category" => $category
            ];
        }
        session()->put('wishlist', $wishlist);
        return redirect()->back()->with('success', 'Thêm sản phẩm yêu thích thành công!');
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function update(Request $request)
    {
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]['quantity'] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function removeCart(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            $prod_to_compare = session()->get('prod_to_compare');
            if(isset($cart[(int)$request->id])) {
                unset($cart[(int)$request->id]);
                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function removeWishlist(Request $request)
    {
        if($request->id) {
            $wishlist = session()->get('wishlist');
            if(isset($wishlist[(int)$request->id])) {
                unset($wishlist[(int)$request->id]);
                session()->put('wishlist', $wishlist);
            }
            session()->flash('success', 'Wishlist removed successfully');
        }
    }

}
