<?php

namespace App\Http\Controllers\Frontend;

use Exception;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\SocialAccount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToSocial($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleSocialCallback($provider)
    {
        try{
            $socialRegister = Socialite::driver($provider)->user();
            $socialCustomer = ($provider == 'facebook') ? SocialAccount::where('facebook_id', $socialRegister->id)->first() :
                                                        SocialAccount::where('google_id', $socialRegister->id)->first();

            if($socialCustomer){
                $socialCustomer->updateOrCreate(['customer_id' => $socialCustomer->customers->id], [
                    'social_token' => $socialRegister->token,
                    'social_refresh_token' => $socialRegister->refreshToken
                ]);
            }else{
                $socialCustomer = Customer::create([
                        'name' => $socialRegister->name,
                        'gender' => $socialRegister->user['gender'] ?? null,
                        'password' => Hash::make('a12345678X')
                    ]);
                SocialAccount::create([
                    'email' => $socialRegister->email,
                    'customer_id' => $socialCustomer->id,
                    'facebook_id' => $provider == 'facebook' ? $socialRegister->id : null,
                    'google_id' => $provider == 'google' ? $socialRegister->id : null,
                    'social_token' => $socialRegister->token,
                    'social_refresh_token' => $socialRegister->refreshToken
                ]);
            }

            Auth::guard('customer')->login(Customer::find($socialCustomer->customers->id ?? $socialCustomer->id), true);

            return redirect()->intended('/site/home');
        } catch (Exception $e)
        {
            dd($e->getMessage());
        }
    }
}
