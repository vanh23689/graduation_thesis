<?php

namespace App\Http\Requests;

use App\Rules\CheckEmailUpdate;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'password' => 'nullable|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'zip_code' => 'nullable',
            'cities' => 'nullable',
            'districts' => 'nullable',
            'wards' => 'nullable',
            'email' => ['required', 'max:255', 'email', new CheckEmailUpdate()]
            // 'logo_url' => 'file|mimes:jpeg,jpg,png,gif,svg|max:10240|nullable',
        ];
    }
}
