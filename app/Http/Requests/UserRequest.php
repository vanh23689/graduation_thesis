<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            // 'parent_user_id' => 'nullable',
            'password' => 'nullable',
            'last_name' => 'required',
            'first_name' => 'required',
            'phone' => 'nullable|regex:/^[0-9]+$/',
            'email' => 'required|unique:users|max:255',
            'email_verified_at' => 'nullable',
            'zip_code' => 'nullable',
            'city' => 'nullable',
            'district' => 'nullable',
            'ward' => 'nullable',
            'avatar' => 'nullable',
            'status' => 'nullable',
            'profile_photo_path' => 'nullable',
            'access_token' => 'nullable',
            'remember_token' => 'nullable',
        ];
    }
}
