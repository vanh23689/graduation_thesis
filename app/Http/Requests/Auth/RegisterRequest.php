<?php

namespace App\Http\Requests\Auth;

use App\Rules\CheckUserRegister;
use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'last_name' => 'required|max:60',
            'first_name' => 'required|max:60',
            'email' => ['required', 'max:60', 'email:rfc,dns', new CheckUserRegister()],
            'password' => ['required', 'confirmed', Password::min(6)->mixedCase()
                                                        ->letters()
                                                        ->numbers()
                                                        ->uncompromised()
            ],
            'password_confirmation' => 'required',
        ];
    }

    public function message(){
        return [
            'password.min' => 'Mật khẩu phải chứa từ 6 kí tự trở lên'
        ];
    }
}
