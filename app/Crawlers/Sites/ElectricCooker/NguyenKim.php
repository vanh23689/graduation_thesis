<?php

namespace App\Crawlers\Sites\ElectricCooker;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductCivil;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class NguyenKim implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://www.nguyenkim.com/noi-com-dien-cuckoo-cr-1055-1-8-lit.html';
    public const DOMAIN = 'www.nguyenkim.com';
    // https://www.nguyenkim.com/noi-com-dien/
    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - t has path start with /dien-thoai-di-dong-samsung/**
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/noi-com-.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {
        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (preg_match('#^\/noi-com-.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//div[@id='nk-logo']/a/img/@src");
        $images = Dom::getElements($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='product_info_name']");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[1]/span/text()");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/div[1]/span/text()");
        $reduce = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/span/text()");
        $insurance = Dom::getFirstElement($dom, "//td[contains(text(), 'Địa điểm bảo hành:')]/following-sibling::td");
        $address = Dom::getFirstElement($dom, "//div[@class='listMall_items']/ul[1]//descendant-or-self::node()");
        $productCode = Dom::getFirstElement($dom, "//td[contains(text(), 'Model:')]/following-sibling::td");
        $origin = Dom::getFirstElement($dom, "//td[contains(text(),'Xuất xứ:')]/following-sibling::td");
        $producer = Dom::getFirstElement($dom, "//td[contains(text(),'Nhà sản xuất:')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước thùng:')]/following-sibling::td");
        $weight = Dom::getFirstElement($dom, "//td[contains(text(),'Khối lượng thùng (kg):')]/following-sibling::td");
        $version = Dom::getFirstElement($dom, "//td[contains(text(),'Khối lượng dàn lạnh:') or contains(text(),'Năm ra mắt :')]/following-sibling::td");
        $screen = Dom::getFirstElement($dom, "//td[contains(text(),'Màn hình hiển thị:')]/following-sibling::td");
        $wattage = Dom::getFirstElement($dom, "//td[contains(text(),'Công suất:')]/following-sibling::td");
        $capacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung tích:')]/following-sibling::td");

        // Cooker
        $productCode = Dom::getFirstElement($dom, "//td[contains(text(),'Model:')]/following-sibling::td");
        $type = Dom::getFirstElement($dom, "//td[contains(text(),'Loại nồi:')]/following-sibling::td");
        $material = Dom::getFirstElement($dom, "//td[contains(text(),'Chất liệu:') or contains(text(),'Chất liệu lòng nồi:')]/following-sibling::td");
        $regime = Dom::getFirstElement($dom, "//td[contains(text(),'Chế độ nấu:')]/following-sibling::td");
        $steamingTray = Dom::getFirstElement($dom, "//td[contains(text(),'Khay hấp:') or contains(text(),'Khay hấp thực phẩm:')]/following-sibling::td");
        // End Cooker

        if(!is_null($productCode)){
            $category_id = ProductCategory::updateOrCreate([
                'name' => 'Cooker'
            ]);

            $product_id = ProductCivil::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'material' => $material ?? null,
                'regime' => $regime ?? null,
                'steaming_tray' => $steamingTray ?? null,
                'capacity' => $capacity ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => $url->__toString(),
                    'name' => $name ?? '',
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'reduce' => $reduce ?? null,
                    'origin' => $origin ?? null,
                    'producer' => $producer ?? null,
                    'logo' => 'https://www.nguyenkim.com' . $logo ?? null,
                    'email' => 'NKare@nguyenkim.com',
                    'insurance' => $insurance ?? null,

                    'hotline' => '1800.6800 ',
                    'img' => $img ?? null,
                    'size' => $size ?? null,
                    'weight' => $weight ?? null,
                    'description' => $description ?? null,
                    'address' => $address ?? null,
                    'wattage' => $wattage ?? null, // công suất
                    'type' => $type ?? null,
                    'screen' => $screen ?? null,
                    'version' => $version ?? null,
                    'category_id' => $category_id->id,
                    'civil_id' => $product_id->id
                ]
            );
            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }

        }
        return [];
    }

}
