<?php

namespace App\Crawlers\Sites\Mobile;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use App\Models\ProductTelecommunication;
use Symfony\Component\DomCrawler\Crawler;

class Cellphones implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://cellphones.com.vn/mobile.html';
    public const DOMAIN = 'cellphones.com.vn';
    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - It is root page
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/#', $url->getPath()) ||
                preg_match('#^\/mobile#', $url->getPath()) ||
                preg_match('#^\/apple.*#', $url->getPath()) ||
                preg_match('#^\/samsung.*#', $url->getPath()) ||
                preg_match('#^\/xiaomi.*#', $url->getPath()) ||
                preg_match('#^\/oppo.*#', $url->getPath()) ||
                preg_match('#^\/oneplus.*#', $url->getPath()) ||
                preg_match('#^\/nokia.*#', $url->getPath()) ||
                preg_match('#^\/nubia.*#', $url->getPath()) ||
                preg_match('#^\/realme.*#', $url->getPath()) ||
                preg_match('#^\/asus.*#', $url->getPath()) ||
                preg_match('#^\/vivo.*#', $url->getPath()) ||
                preg_match('#^\/tecno.*#', $url->getPath()) ||
                preg_match('#^\/cat-.*#', $url->getPath()) ||
                preg_match('#^\/honor.*#', $url->getPath()) ||
                preg_match('#^\/iphone.*#', $url->getPath()) ||
                preg_match('#^\/philips.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {
        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (
            preg_match('#^\/apple.*#', $url->getPath()) ||
            preg_match('#^\/samsung.*#', $url->getPath()) ||
            preg_match('#^\/xiaomi.*#', $url->getPath()) ||
            preg_match('#^\/oppo.*#', $url->getPath()) ||
            preg_match('#^\/oneplus.*#', $url->getPath()) ||
            preg_match('#^\/nokia.*#', $url->getPath()) ||
            preg_match('#^\/nubia.*#', $url->getPath()) ||
            preg_match('#^\/realme.*#', $url->getPath()) ||
            preg_match('#^\/asus.*#', $url->getPath()) ||
            preg_match('#^\/vivo.*#', $url->getPath()) ||
            preg_match('#^\/tecno.*#', $url->getPath()) ||
            preg_match('#^\/cat-.*#', $url->getPath()) ||
            preg_match('#^\/honor.*#', $url->getPath()) ||
            preg_match('#^\/iphone.*#', $url->getPath()) ||
            preg_match('#^\/philips.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//*[local-name()='svg'][1]");
        $images = Dom::getElements($dom, "//div[@class='swiper-slide']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='box-ksp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='title mt-2 product-title has-text-centered']/text() | //div[@class='box-product-name']/h1/text()");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='boxInfoRight']/span/strong | //p[@class='product__price--show']/text()");
        $initialPrice = Dom::getFirstElement($dom, "//p[@class='product__price--through']/text()");
        $cpu = Dom::getFirstElement($dom, "//p[contains(text(),'Loại CPU')]/following-sibling::div");

        $operatingSystem = Dom::getFirstElement($dom, "//p[contains(text(),'Hệ điều hành')]/following-sibling::div");
        $resolution = Dom::getFirstElement($dom, "//p[contains(text(),'Độ phân giải màn hình')]/following-sibling::div");
        $screen = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ màn hình')]/following-sibling::div");


        $wifi = Dom::getFirstElement($dom, "//p[contains(text(), 'Wi-Fi')]/following-sibling::div");
        $bluetooth = Dom::getFirstElement($dom, "//p[contains(text(), 'Bluetooth')]/following-sibling::div");
        $sim = Dom::getFirstElement($dom, "//p[contains(text(), 'Thẻ SIM')]/following-sibling::div");
        $nfc = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ NFC')]/following-sibling::div");
        $networkSupport = Dom::getFirstElement($dom, "//p[contains(text(), 'Hỗ trợ mạng')]/following-sibling::div");
        // ĐT
        $pin = Dom::getFirstElement($dom, "//p[contains(text(), 'Pin')]/following-sibling::div");
        $size = Dom::getFirstElement($dom, "//p[contains(text(),'Kích thước')]/following-sibling::div");
        $weight = Dom::getFirstElement($dom, "//p[contains(text(),'Trọng lượng')]/following-sibling::div");
        $afterCamera = Dom::getFirstElement($dom, "//p[contains(text(), 'Camera sau')]/following-sibling::div");
        $beforeCamera = Dom::getFirstElement($dom, "//p[contains(text(),'Camera trước')]/following-sibling::div");
        $beforeVideo = Dom::getFirstElement($dom, "//p[contains(text(), 'Quay video trước') or contains(text(), 'Quay video')]/following-sibling::div");
        $afterVideo = Dom::getFirstElement($dom, "//p[contains(text(), 'Quay video')]/following-sibling::div");
        $ram = Dom::getFirstElement($dom, "//p[contains(text(), 'Dung lượng RAM')]/following-sibling::div");
        $memory = Dom::getFirstElement($dom, "//p[contains(text(), 'Bộ nhớ trong')]/following-sibling::div");
        $chargingTechnology = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ sạc')]/following-sibling::div");
        $fingerprintSensor = Dom::getFirstElement($dom, "//p[contains(text(), 'Cảm biến vân tay')]/following-sibling::div");
        $typesOfSensors = Dom::getFirstElement($dom, "//p[contains(text(), 'Các loại cảm biến')]/following-sibling::div");
        $specialFeatures = Dom::getFirstElement($dom, "//p[contains(text(), 'Tính năng đặc biệt')]/following-sibling::div");
        $headphone = Dom::getFirstElement($dom, "//p[contains(text(), 'Jack tai nghe 3.5')]/following-sibling::div");
        $zalo = Dom::getFirstElement($dom, "//a[@id='btnZaloChat']/@href");
        $graphics = Dom::getFirstElement($dom, "//p[contains(text(), 'GPU')]/following-sibling::div");
        $chipset = Dom::getFirstElement($dom, "//p[contains(text(), 'Chipset')]/following-sibling::div");
        $productCode = substr($url->getPath(), 1, -5);
        // END ĐT

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Smartphone'
        ]);

        if(!is_null($name)){
            $product_id = ProductTelecommunication::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode,
                'chipset' => $chipset ?? 'mobile',
                'memory' => $memory ?? null,
                'before_camera' => $beforeCamera ?? null,
                'before_video' => $beforeVideo ?? null,
                'after_camera' => $afterCamera ?? null,
                'after_video' => $afterVideo ?? null,
                'sim' => $sim ?? null,
                'flash_light' => $flashLight ?? null,
                'fingerprint_sensor' => $fingerprintSensor ?? null,
                'special_features' => $specialFeatures ?? null,
                'types_of_sensors' => $typesOfSensors ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => $url->__toString(),
                    'name' => $name ?? null,
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'address' => '350-352 Võ Văn Kiệt, Phường Cô Giang, Quận 1, Thành phố Hồ Chí Minh, Việt Nam',
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'reduce' => $reduce ?? null,
                    'origin' => $origin ?? null,
                    'logo' => $logo ?? null,
                    'email' => 'NKare@nguyenkim.com',

                    'hotline' => '1800.6800 ',
                    'img' => $img ?? null,
                    'size' => $size ?? null,
                    'weight' => $weight ?? null,
                    'description' => $description ?? null,
                    'address' => $address ?? null,
                    'graphics' => $graphics ?? null,
                    'headphone' => $headphone ?? null,
                    'ram' => $ram ?? null,
                    'charging_technology' => $chargingTechnology ?? null,
                    'pin' => $pin ?? null,
                    'cpu' => $cpu ?? null,
                    'wifi' => $wifi ?? null,
                    'nfc' => $nfc ?? null,
                    'operating_system' => $operatingSystem ?? null,
                    'resolution' => $resolution ?? null,
                    'screen' => $screen ?? null,
                    'bluetooth' => $bluetooth ?? null,
                    'network_support' => $networkSupport ?? null,
                    'zalo' => $zalo ?? null,
                    'category_id' => $category_id->id,
                    'telecommunication_id' => $product_id->id
                ]
            );

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }
        return [];
    }
}
