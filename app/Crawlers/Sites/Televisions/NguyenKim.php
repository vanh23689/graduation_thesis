<?php

namespace App\Crawlers\Sites\Televisions;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductCivil;
use App\Models\ProductDetail;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class NguyenKim implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://www.nguyenkim.com/tu-lanh/';
    public const DOMAIN = 'www.nguyenkim.com';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - t has path start with /dien-thoai-di-dong-samsung/**
        if (
            $url->getHost() === self::DOMAIN &&
            (
                $url->getPath() === '/' ||
                preg_match('#^\/tu-lanh.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {
        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (preg_match('#^\/tu-lanh.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//div[@id='nk-logo']/a/img/@src");
        $images = Dom::getElements($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='product_info_name']");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[1]/span/text()");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/div[1]/span/text()");
        $reduce = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/span/text()");
        $insurance = Dom::getFirstElement($dom, "//td[contains(text(), 'Địa điểm bảo hành:')]/following-sibling::td");
        $address = Dom::getFirstElement($dom, "//div[@class='listMall_items']/ul[1]//descendant-or-self::node()");
        $productCode = Dom::getFirstElement($dom, "//td[contains(text(), 'Model:')]/following-sibling::td");
        $origin = Dom::getFirstElement($dom, "//td[contains(text(),'Xuất xứ:')]/following-sibling::td");
        $producer = Dom::getFirstElement($dom, "//td[contains(text(),'Nhà sản xuất:')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước thùng:')]/following-sibling::td");
        $weight = Dom::getFirstElement($dom, "//td[contains(text(),'Khối lượng thùng (kg):')]/following-sibling::td");

        // Televisions
        $refrigeratorStyle = Dom::getFirstElement($dom, "//td[contains(text(),'Kiểu tủ lạnh:')]/following-sibling::td");
        $doorNumber = Dom::getFirstElement($dom, "//td[contains(text(),'Số cửa tủ:')]/following-sibling::td");
        $refrigeratorCapacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung tích tủ lạnh:')]/following-sibling::td");
        $freezerCompartmentCapacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung tích ngăn lạnh:')]/following-sibling::td");
        $freezerCapacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung tích ngăn đá:')]/following-sibling::td");
        $snow = Dom::getFirstElement($dom, "//td[contains(text(),'Đóng tuyết:')]/following-sibling::td");
        $refrigeratorTechnology = Dom::getFirstElement($dom, "//td[contains(text(),'Công nghệ làm lạnh:')]/following-sibling::td");
        $trayMaterial = Dom::getFirstElement($dom, "//td[contains(text(),'Chất liệu khay:')]/following-sibling::td");
        $deodorant = Dom::getFirstElement($dom, "//td[contains(text(),'Kháng khuẩn / Khử mùi:')]/following-sibling::td");
        $outerMaterial = Dom::getFirstElement($dom, "//td[contains(text(),'Chất liệu bên ngoài:')]/following-sibling::td");
        $automaticIceMaker = Dom::getFirstElement($dom, "//td[contains(text(),'Làm đá tự động:')]/following-sibling::td");
        $getOutsideWater = Dom::getFirstElement($dom, "//td[contains(text(),'Lấy nước bên ngoài:')]/following-sibling::td");
        $doorBell = Dom::getFirstElement($dom, "//td[contains(text(),'Chuông báo cửa:')]/following-sibling::td");
        // End televisions

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Television'
        ]);

        if(!is_null($name)){
            $civil_id = ProductCivil::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'door_number' => $doorNumber ?? null,
                'capacity' => $refrigeratorCapacity ?? null,
                'freezer_compartment_capacity' => $freezerCompartmentCapacity ?? null,
                'freezer_capacity' => $freezerCapacity ?? null,
                'snow' => $snow ?? null,
                'refrigerator_technology' => $refrigeratorTechnology ?? null,
                'tray_material' => $trayMaterial ?? null,
                'deodorant' => $deodorant ?? null,
                'outer_material' => $outerMaterial ?? null,
                'automatic_ice_maker' => $automaticIceMaker ?? null,
                'get_outside_water' => $getOutsideWater ?? null,
                'door_bell' => $doorBell ?? null
            ]);

            $data = Product::create([
                'website' => $url->__toString(),
                'name' => $name ?? '',
                'crawled_at' => now()->format('Y-m-d H:i:s'),
                'initial_price' => $initialPrice ?? null,
                'current_price' => $currentPrice ?? null,
                'reduce' => $reduce ?? null,
                'origin' => $origin ?? null,
                'producer' => $producer ?? null,
                'logo' => 'https://www.nguyenkim.com' . $logo ?? null,
                'email' => 'NKare@nguyenkim.com',
                'insurance' => $insurance ?? null,

                'hotline' => '1800.6800 ',
                'img' => $img ?? null,
                'size' => $size ?? null,
                'weight' => $weight ?? null,
                'description' => $description ?? null,
                'address' => $address ?? null,
                'type' => $refrigeratorStyle ?? null,
                'category_id' => $category_id->id,
                'civil_id' => $civil_id->id
            ]);

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }

        }
        return [];
    }

}
