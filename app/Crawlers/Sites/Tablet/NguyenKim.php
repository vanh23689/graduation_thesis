<?php

namespace App\Crawlers\Sites\Tablet;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Str;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use App\Models\ProductTelecommunication;
use Symfony\Component\DomCrawler\Crawler;

class NguyenKim implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://www.nguyenkim.com/may-tinh-bang/';
    public const DOMAIN = 'www.nguyenkim.com';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - t has path start with /may-tinh-bang/**
        if (
            $url->getHost() === self::DOMAIN &&
            (
                $url->getPath() === '/' ||
                preg_match('#^\/may-tinh-bang.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {

        // Only crawls data from detail page (it has path start with /may-tinh-bang/** )
        if (preg_match('#^\/may-tinh-bang.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//div[@id='nk-logo']/a/img/@src");
        $images = Dom::getElements($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='product_info_name']");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[1]/span/text()");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/div[1]/span/text()");
        $reduce = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/span/text()");
        $ram = Dom::getFirstElement($dom, "//td[contains(text(),'RAM:')]/following-sibling::td");
        $cpu = Dom::getFirstElement($dom, "//td[contains(text(),'CPU:')]/following-sibling::td");
        $chipset = Dom::getFirstElement($dom, "//td[contains(text(),'Chipset:')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước màn hình:')]/following-sibling::td");
        $pin = Dom::getFirstElement($dom, "//td[contains(text(), 'Loại Pin:')]/following-sibling::td");
        $screen = Dom::getFirstElement($dom, "//td[contains(text(), 'Loại màn hình:')]/following-sibling::td");
        $graphics = Dom::getFirstElement($dom, "//td[contains(text(), 'Độ phân giải màn hình:')]/following-sibling::td");
        $insurance = Dom::getFirstElement($dom, "//td[contains(text(), 'Địa điểm bảo hành:')]/following-sibling::td");
        $address = Dom::getFirstElement($dom, "//div[@class='listMall_items']/ul[1]//descendant-or-self::node()");
        $productCode = Dom::getFirstElement($dom, "//td[contains(text(), 'Model:')]/following-sibling::td");
        $type = Dom::getFirstElement($dom, "//td[contains(text(),'Loại Máy:')]/following-sibling::td");
        $wifi = Dom::getFirstElement($dom, "//td[contains(text(), 'WIFI:')]/following-sibling::td");
        $bluetooth = Dom::getFirstElement($dom, "//td[contains(text(), 'Bluetooth:')]/following-sibling::td");
        $operatingSystem = Dom::getFirstElement($dom, "//td[contains(text(), 'Hệ điều hành:')]/following-sibling::td");
        $version = Dom::getFirstElement($dom, "//td[contains(text(), 'Năm ra mắt :')]/following-sibling::td");
        $origin = Dom::getFirstElement($dom, "//td[contains(text(),'Xuất xứ:')]/following-sibling::td");
        $producer = Dom::getFirstElement($dom, "//td[contains(text(),'Nhà sản xuất:')]/following-sibling::td");
        $beforeCamera = Dom::getFirstElement($dom, "//td[contains(text(),'Camera trước:')]/following-sibling::td");
        $afterCamera = Dom::getFirstElement($dom, "//td[contains(text(), 'Camera sau:')]/following-sibling::td");
        $memory = Dom::getFirstElement($dom, "//td[contains(text(), 'Bộ nhớ trong:')]/following-sibling::td");

        if(!is_null($name)){
            $category_id = ProductCategory::updateOrCreate([
                'name' => 'Tablet'
            ]);

            $product_id = ProductTelecommunication::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'before_camera' => $beforeCamera ?? null,
                'before_video' => $beforeVideo ?? null,
                'after_camera' => !empty($afterCamera) ? ((Str::length($afterCamera) > 255) ? null : $afterCamera) : null,
                'after_video' => $afterVideo ?? null,
                'chipset' => $chipset ?? null,
                'memory' => $memory ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => $url->__toString(),
                    'name' => $name ?? null,
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'address' => $address ?? null,
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'version' => $version ?? null,
                    'operating_system' => $operatingSystem ?? null,
                    'origin' => $origin ?? null,
                    'producer' => $producer ?? null,
                    'type' => $type ?? null,
                    'reduce' => $reduce ?? null,
                    'ram' => $ram ?? null,
                    'cpu' => $cpu ?? null,
                    'screen' => $screen ?? null,
                    'graphics' => $graphics ?? null,
                    'size' => $size ?? null,
                    'wifi' => $wifi ?? null,
                    'bluetooth' => $bluetooth ?? null,
                    'pin' => $pin ?? null,
                    'logo' => 'https://www.nguyenkim.com' . $logo ?? null,
                    'email' => 'NKare@nguyenkim.com',
                    'insurance' => $insurance ?? null,

                    'hotline' => '1800.6800 ',
                    'img' => $img ?? null,
                    'description' => $description ?? null,
                    'address' => $address ?? null,
                    'telecommunication_id' => $product_id->id,
                    'category_id' => $category_id->id
                ]
            );

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }
        return [];
    }

}
