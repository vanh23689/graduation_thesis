<?php

namespace App\Crawlers\Sites\PC;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductOffice;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class Cellphones implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://cellphones.com.vn/pc-gaming-cps-002.html';
    public const DOMAIN = 'cellphones.com.vn';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - It is root page
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/pc.*#', $url->getPath()) ||
                preg_match('#^\/may-tinh.*#', $url->getPath())

            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {
        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (
            preg_match('#^\/pc.*#', $url->getPath()) ||
            preg_match('#^\/may-tinh.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//*[local-name()='svg']");
        $images = Dom::getElements($dom, "//div[@class='swiper-slide']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='box-ksp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='title mt-2 product-title has-text-centered']/text() | //div[@class='box-product-name']/h1/text()");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='boxInfoRight']/span/strong | //p[@class='product__price--show']/text()");
        $initialPrice = Dom::getFirstElement($dom, "//p[@class='product__price--through']/text()");
        $cpu = Dom::getFirstElement($dom, "//p[contains(text(),'Loại CPU')]/following-sibling::div");
        $graphics = Dom::getFirstElement($dom, "//p[contains(text(), 'Loại card đồ họa')]/following-sibling::div");
        $ram = Dom::getFirstElement($dom, "//p[contains(text(), 'Loại RAM')]/following-sibling::div");
        $type = Dom::getFirstElement($dom, "//p[contains(text(),'Loại case (PC)')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//p[contains(text(),'Kích thước') or contains(text(),'Kích thước màn hình')]/following-sibling::div");
        $features = Dom::getFirstElement($dom, "//p[contains(text(),'Tính năng đặc biệt')]/following-sibling::div");

        $operatingSystem = Dom::getFirstElement($dom, "//p[contains(text(),'Hệ điều hành')]/following-sibling::div");
        $wifi = Dom::getFirstElement($dom, "//p[contains(text(), 'Wi-Fi')]/following-sibling::div");
        $bluetooth = Dom::getFirstElement($dom, "//p[contains(text(), 'Bluetooth')]/following-sibling::div");
        $weight = Dom::getFirstElement($dom, "//p[contains(text(), 'Trọng lượng')]/following-sibling::div");
        $producer = Dom::getFirstElement($dom, "//p[contains(text(), 'Hãng sản xuất')]/following-sibling::div");
        $touch = Dom::getFirstElement($dom, "//p[contains(text(), 'Màn hình cảm ứng')]/following-sibling::div");
        $screen = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ màn hình')]/following-sibling::div");
        $resolution = Dom::getFirstElement($dom, "//p[contains(text(), 'Độ phân giải màn hình') or contains(text(), 'Độ phân giải')]/following-sibling::div");
        $warrantyPeriod = Dom::getFirstElement($dom, "//p[contains(text(), 'Bảo mật')]/following-sibling::div");

        $productCode = Dom::getFirstElement($dom, "//div[@class='additional-information mr-2']");
        $ramCapacity = Dom::getFirstElement($dom, "//p[contains(text(), 'Dung lượng RAM')]/following-sibling::div");
        $hardDrive = Dom::getFirstElement($dom, "//p[contains(text(),'Ổ cứng')]/following-sibling::td");
        $serialPort = Dom::getFirstElement($dom, "//p[contains(text(),'Cổng I/O mặt sau (PC lắp ráp)') or contains(text(),'Cổng giao tiếp')]/following-sibling::td");
        $webcam = Dom::getFirstElement($dom, "//p[contains(text(), 'Webcam')]/following-sibling::div");

        if(!is_null($name)){
            $category_id = ProductCategory::updateOrCreate([
                'name' => 'Personal Computer'
            ]);
            $product_id = ProductOffice::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'ram_capacity' => $ramCapacity ?? null,
                'hard_drive' => $hardDrive ?? null,
                'serial_port' => $serialPort ?? null,
                'features' => $features ?? null,
                'webcam' => $webcam ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => $url->__toString(),
                    'name' => $name ?? '',
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'address' => '350-352 Võ Văn Kiệt, Phường Cô Giang, Quận 1, Thành phố Hồ Chí Minh, Việt Nam',
                    'tel' => '028.7108.9666',
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'cpu' => $cpu ?? null,
                    'ram' => $ram ?? null,
                    'screen' => $screen ?? null,
                    'size' => $size ?? null,
                    'logo' => $logo ?? null,
                    'operating_system' => $operatingSystem ?? null,
                    'graphics' => $graphics ?? null,
                    'pin' => $pin ?? null,
                    'bluetooth' => $bluetooth ?? null,
                    'zalo' => $zalo ?? null,
                    'wifi' => $wifi ?? null,
                    'type' => $type ?? null,
                    'weight' => $weight ?? null,
                    'producer' => $producer ?? null,
                    'touch' => $touch ?? null,
                    'resolution' => $resolution ?? null,
                    'warranty_period' => $warrantyPeriod ?? null,
                    'insurance' => 'Gọi bảo hành 1800.2064 (8h00 - 21h00)',

                    'img' => $img ?? null,
                    'description' => $description ?? null,
                    'office_id' => $product_id->id,
                    'category_id' => $category_id->id
                ]
            );

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }
        return [];
    }
}
