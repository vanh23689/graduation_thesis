<?php

namespace App\Crawlers\Sites\Laptop;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductOffice;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class NguyenKim implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://www.nguyenkim.com/laptop-macbook-pro-m1-2020-13-inch-256gb-myd82sa-a-xam.html';
    public const DOMAIN = 'www.nguyenkim.com';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - t has path start with /dien-thoai-di-dong-samsung/**
        if (
            $url->getHost() === self::DOMAIN &&
            (
                $url->getPath() === '/' ||
                preg_match('#^\/laptop-.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {

        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (preg_match('#^\/laptop-.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//div[@id='nk-logo']/a/img/@src");

        $img = Dom::getFirstElement($dom, "//div[@class='wrap-img-tag-pdp']/img/@src");
        $images = Dom::getElements($dom, "//img[@class=' lazyloaded']/@alt");
        $name = Dom::getFirstElement($dom, "//h1[@class='product_info_name']");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[1]/span/text()");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/div[1]/span/text()");
        $reduce = Dom::getFirstElement($dom, "//div[@class='product_info_left']/div[2]/span/text()");
        $ram = Dom::getFirstElement($dom, "//td[contains(text(),'Loại RAM:')]/following-sibling::td");
        $cpu = Dom::getFirstElement($dom, "//td[contains(text(),'CPU:')]/following-sibling::td");
        $origin = Dom::getFirstElement($dom, "//td[contains(text(),'Xuất xứ:')]/following-sibling::td");
        $producer = Dom::getFirstElement($dom, "//td[contains(text(),'Nhà sản xuất:')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước màn hình:')]/following-sibling::td");
        $pin = Dom::getFirstElement($dom, "//td[contains(text(), 'Dung lượng Pin:')]/following-sibling::td");
        $screen = Dom::getFirstElement($dom, "//td[contains(text(), 'Loại màn hình:')]/following-sibling::td");
        $insurance = Dom::getFirstElement($dom, "//td[contains(text(), 'Địa điểm bảo hành:')]/following-sibling::td");
        $address = Dom::getFirstElement($dom, "//div[@class='listMall_items']/ul[1]//descendant-or-self::node()");
        $version = Dom::getFirstElement($dom, "//td[contains(text(), 'Năm ra mắt :')]/following-sibling::td");
        $operatingSystem = Dom::getFirstElement($dom, "//td[contains(text(), 'Hệ điều hành:') or contains(text(), 'HĐH kèm theo máy:')]/following-sibling::td");
        $bluetooth = Dom::getFirstElement($dom, "//td[contains(text(), 'Bluetooth:')]/following-sibling::td");
        $graphics = Dom::getFirstElement($dom, "//td[contains(text(), 'Bộ xử lý đồ họa:')]/following-sibling::td");
        $resolution = Dom::getFirstElement($dom, "//td[contains(text(), 'Độ phân giải màn hình:')]/following-sibling::td");
        $audio = Dom::getFirstElement($dom, "//td[contains(text(), 'Công nghệ âm thanh:')]/following-sibling::td");
        $touch =Dom::getFirstElement($dom, "//td[contains(text(), 'Màn hình cảm ứng:')]/following-sibling::td");
        $usb = Dom::getFirstElement($dom, "//td[contains(text(),'Cổng USB:')]/following-sibling::td");
        $wifi = Dom::getFirstElement($dom, "//td[contains(text(),'Chuẩn WiFi:')]/following-sibling::td");
        $type = Dom::getFirstElement($dom, "//td[contains(text(),'Loại Máy:	')]/following-sibling::td");
        $warrantyPeriod = Dom::getFirstElement($dom, "//td[contains(text(), 'Thời gian bảo hành:')]/following-sibling::td");


        $connect = Dom::getFirstElement($dom, "//td[contains(text(),'Kết nối khác laptop:')]/following-sibling::td");
        $ramCapacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung lượng RAM:')]/following-sibling::td");
        $hardDrive = Dom::getFirstElement($dom, "//td[contains(text(),'Loại ổ đĩa cứng:')]/following-sibling::td");
        $capacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung lượng :')]/following-sibling::td");
        $productCode = Dom::getFirstElement($dom, "//td[contains(text(), 'Model:')]/following-sibling::td");

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Laptop'
        ]);
        if(!is_null($name)){
            $product_id = ProductOffice::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'connect' => $connect ?? null,
                'ram_capacity' => $ramCapacity ?? null,
                'hard_drive' => $hardDrive ?? null,
                'capacity' => $capacity ?? null
            ]);

            $data = Product::create([
                'website' => $url->__toString(),
                'name' => $name ?? '',
                'crawled_at' => now()->format('Y-m-d H:i:s'),
                'address' => '16A20 Vũ Ngọc Phan, Phường 13, Q.Bình Thạnh, HCM',
                'initial_price' => $initialPrice ?? null,
                'current_price' => $currentPrice ?? null,
                'reduce' => $reduce ?? null,
                'ram' => $ram ?? null,
                'cpu' => $cpu ?? null,
                'origin' => $origin ?? null,
                'pin' => $pin ?? null,
                'type' => $type ?? null,
                'producer' => $producer ?? null,
                'version' => $version ?? null,
                'operating_system' => $operatingSystem ?? null,
                'bluetooth' => $bluetooth ?? null,
                'graphics' => $graphics ?? null,
                'resolution' => $resolution ?? null,
                'audio' => $audio ?? null,
                'touch' => $touch ?? null,
                'usb' => $usb ?? null,
                'wifi' => $wifi ?? null,
                'screen' => $screen ?? null,
                'size' => $size ?? null,
                'logo' => 'https://www.nguyenkim.com' . $logo ?? null,
                'warranty_period' => $warrantyPeriod ?? null,
                'email' => 'NKare@nguyenkim.com',
                'insurance' => $insurance ?? null,

                'hotline' => '1800.6800 ',
                'img' => $img ?? null,
                'description' => $description ?? null,
                'address' => $address ?? null,
                'office_id' => $product_id->id,
                'category_id' => $category_id->id
            ]);

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }
            return [];
    }

}
