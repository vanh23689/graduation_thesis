<?php

namespace App\Crawlers\Sites\Laptop;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Str;
use App\Models\ProductImage;
use App\Models\ProductOffice;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class MaytinhFPT implements CrawlableSite, MustExecuteJavascript
{
    protected const BASE_URL = 'https://fptshop.com.vn/may-tinh-xach-tay/hp-pavilion-14-dv2051tu-i3-1215u';
    public const DOMAIN = 'fptshop.com.vn';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
    */
    public function clientOptions(): array {
        return [];
    }
    /**
     * Bắt đầu crawl với URL vừa truyền vào
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }
    /**
     * Xác đinh có crawl từ link vừa truyền vào hay không bằng preg_match
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // - Là trang chủ
        // - Có đường dẫn chứa /may-tinh-xach-tay/
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/may-tinh-xach-tay\/#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }
    /**
     * Xác định xuất data từ các url đã cho
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {

        // Only crawls data from detail page (it has path start with /may-tinh-xach-tay/** )
        if (preg_match('#^\/may-tinh-xach-tay\/#', $url->getPath())) {
            return true;
        }

        return false;
    }
    /**
     * Xuất các dữ liệu vừa lấy được
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $name = Dom::getFirstElement($dom, "//h1[@class='st-name']/text()");
        $images = Dom::getElements($dom, "//div[@class='swiper-wrapper js--slide--full']/div/img/@src");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content or //div[@class='card-body']");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='st-price__left']/div[2]");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='st-price__left']/div[1]");
        $reduce = Dom::getFirstElement($dom, "//div[@class='price-sing']/span[last()]");
        // cpu
        $cpu = Dom::getFirstElement($dom, "//td[contains(text(),'CPU')]/following-sibling::td");
        $cpuSpeed = Dom::getFirstElement($dom, "//td[contains(text(),'Tốc độ CPU')]/following-sibling::td") . ','. Dom::getFirstElement($dom, "//td[contains(text(),'Tốc độ tối đa')]/following-sibling::td");
        // bus
        $busSpeed = Dom::getFirstElement($dom, "//td[contains(text(),'Tốc độ BUS')]/following-sibling::td");

        $ram = Dom::getFirstElement($dom, "//td[contains(text(),'RAM')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Loại RAM')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Số khe cắm rời')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Số khe RAM còn lại')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Số RAM onboard')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Hỗ trợ RAM tối đa')]/following-sibling::td");

        $ramCapacity = Dom::getFirstElement($dom, "//td[contains(text(),'Dung lượng RAM')]/following-sibling::td") . ',' .
                        Dom::getFirstElement($dom, "//td[contains(text(),'Tốc độ RAM')]/following-sibling::td");

        $screen =   Dom::getFirstElement($dom, "//td[contains(text(),'Màn hình')]/following-sibling::td") . ',' .
                    Dom::getFirstElement($dom, "//div[contains(text(),'Màn hình')]/following-sibling::table[last()]");
        $resolution = Dom::getFirstElement($dom, "//td[contains(text(),'Độ phân giải')]/following-sibling::td");
        $touch = Dom::getFirstElement($dom, "//td[contains(text(), 'Màn hình cảm ứng:')]/following-sibling::td");
        $hardDrive = Dom::getFirstElement($dom, "//td[contains(text(),'Ổ cứng')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//td[contains(text(),'Kiểu ổ cứng')]/following-sibling::td") . ',' .
                Dom::getFirstElement($dom, "//div[contains(text(), 'Lưu trữ')]//following-sibling::table[last()]"). ',' .
                Dom::getFirstElement($dom, "//div[contains(text(), 'Lưu trữ')]//following-sibling::div[last()]");
        $serialPort = Dom::getFirstElement($dom, "//td[contains(text(),'Cổng giao tiếp')]/following-sibling::td");
        $wifi = Dom::getFirstElement($dom, "//td[contains(text(),'Wifi')]/following-sibling::td");
        $bluetooth = Dom::getFirstElement($dom, "//td[contains(text(),'Bluetooth')]/following-sibling::td");
        $webcam = Dom::getFirstElement($dom, "//td[contains(text(),'Webcam')]/following-sibling::td");
        $pin = Dom::getFirstElement($dom, "//div[contains(text(), 'Thông tin pin & Sạc')]//following-sibling::table");
        $graphic = Dom::getFirstElement($dom, "//td[contains(text(),'Đồ họa')]/following-sibling::td");
        $operatingSystem = Dom::getFirstElement($dom, "//td[contains(text(),'Hệ điều hành')]/following-sibling::td") . ',' .
                        Dom::getFirstElement($dom, "//div[contains(text(),'Hệ điều hành')]/following-sibling::table");
        $weight = Dom::getFirstElement($dom, "//td[contains(text(),'Trọng lượng')]/following-sibling::td");
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước')]/following-sibling::td");
        $origin = Dom::getFirstElement($dom, "//td[contains(text(),'Xuất xứ')]/following-sibling::td");
        $version = Dom::getFirstElement($dom, "//td[contains(text(),'Năm ra mắt')]/following-sibling::td");
        $warrantyPeriod = Dom::getFirstElement($dom, "//li[contains(text(),'Thời gian bảo hành (tháng)')]/div");
        $producer = Dom::getFirstElement($dom, "//li[contains(text(),'Thương hiệu')]/div");
        $productCode = Dom::getFirstElement($dom, "//div[@class='c-modal__row'] | //span[@class='st-sku']");

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Laptop'
        ]);

        if(!is_null($name)){
            $product_id = ProductOffice::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode ?? substr($url->getPath(), 1, -5),
                'ram_capacity' => $ramCapacity ?? null,
                'hard_drive' => $hardDrive ?? null,
                'bus_speed' => $busSpeed ?? null,
                'cpu_speed' => $cpuSpeed ?? null,
                'serial_port' => $serialPort ?? null,
                'webcam' => $webcam ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => !empty($url) ? (Str::length($url->__toString()) > 255 ? null : $url->__toString()) : null,
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'name' => $name ?? '',
                    'logo' => 'https://fptshop.com.vn/Content/v4/images/iconhd.png',
                    'description' => $description ?? null,
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'reduce' => $reduce ?? null,
                    'cpu' => $cpu ?? null,
                    'ram' => $ram ?? null,
                    'img' => $img ?? null,
                    'screen' => $screen ?? null,
                    'graphics' => $graphic ?? null,
                    'operating_system' => $operatingSystem ?? null,
                    'resolution' => $resolution ?? null,
                    'touch' => $touch ?? null,
                    'wifi' => $wifi ?? null,
                    'bluetooth' => $bluetooth ?? null,
                    'pin' => $pin ?? null,
                    'warranty_period' => $warrantyPeriod ?? null,
                    'producer' => $producer ?? null,
                    'weight' => $weight ?? null,
                    'size' => $size ?? null,
                    'origin' => $origin ?? null,
                    'version' => $version ?? null,
                    'office_id' => $product_id->id,
                    'category_id' => $category_id->id,

                ]
            );

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }

        }
            return [];
    }
}
