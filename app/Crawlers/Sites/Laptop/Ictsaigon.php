<?php

namespace App\Crawlers\Sites\Laptop;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductOffice;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class Ictsaigon implements CrawlableSite
{
    private const BASE_URL = 'https://ictsaigon.com.vn/laptop/';
    public const DOMAIN = 'ictsaigon.com.vn';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - It is root page
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {

        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (preg_match('#^\/laptop-.*#', $url->getPath()) ||
            preg_match('#^\/dell-.*#', $url->getPath()) ||
            preg_match('#^\/pc-.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $images = Dom::getElements($dom, "//img[@class='wp-post-image skip-lazy']/@src");
        $logo = Dom::getFirstElement($dom, "//img[@class='header_logo header-logo']/@src");
        $img = Dom::getFirstElement($dom, "//img[@class='wp-post-image skip-lazy']/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='product-title product_title entry-title']");
        list($currentPrice, $initialPrice) = $this->parseNode($dom);
        $orderStatus = Dom::getFirstElement($dom, "//b[contains(text(),'Tình trạng:')]/following-sibling::span");
        $reduce = Dom::getFirstElement($dom, "//div[@class='badge-container is-larger absolute left top z-1']/span[last()]");
        $cpu = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[1] | //td[contains(text(),'CPU')]/following-sibling::td");
        $ram = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[2] | //td[contains(text(),'Ram')]/following-sibling::td");
        $hardDrive = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[3]");
        $graphic = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[4]");
        $screen = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[5] | //td[contains(text(),'Màn hình')]/following-sibling::td");
        $operatingSystem = Dom::getFirstElement($dom, "//div[@class='product-short-description']/p/text()[6]");
        $cardVGA = Dom::getFirstElement($dom, "//td[contains(text(),'VGA')]/following-sibling::td");
        $connect = Dom::getFirstElement($dom, "//td[contains(text(),'Kết nối')]/following-sibling::td");
        $weight = Dom::getFirstElement($dom, "//td[contains(text(),'Cân nặng')]/following-sibling::td");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $productCode = substr($url->getPath(), 1, -5);
        $size = Dom::getFirstElement($dom, "//td[contains(text(),'Kích thước')]/following-sibling::td");

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Laptop'
        ]);

        if(!is_null($name)){
            $product_id = ProductOffice::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode,
                'connect' => $connect ?? null,
                'hard_drive' => $hardDrive ?? null,
            ]);

            $data = Product::create([
                'website' => $url->__toString(),
                'name' => $name ?? '',
                'crawled_at' => now()->format('Y-m-d H:i:s'),
                'address' => '16A20 Vũ Ngọc Phan, Phường 13, Q.Bình Thạnh, HCM',
                'tel' => '0939.008.008',
                'initial_price' => $initialPrice ?? null,
                'current_price' => $currentPrice ?? null,
                'reduce' => $reduce ?? null,
                'vga' => $cardVGA ?? null,
                'cpu' => $cpu ?? null,
                'ram' => $ram ?? null,
                'screen' => $screen ?? null,
                'size' => $size ?? null,
                'logo' => $logo ?? null,
                'operating_system' => $operatingSystem ?? null,
                'weight' => $weight ?? null,
                'order_status' => $orderStatus ?? null,
                'graphics' => $graphic ?? null,
                'email' => 'info@ictsaigon.vn',
                'insurance' => 'Trạm bảo hành HN: 149 Ngọc Đại, Đại Mỗ, Từ Liêm, Hà Nội 028 730 28258 - Ex 102
                [08h00 - 17h30 (T2 - T7)]',

                'hotline' => '0906 652 739',
                'img' => $img ?? null,
                'description' => $description ?? null,
                'office_id' => $product_id->id,
                'category_id' => $category_id->id
            ]);

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }

        }
        return [];

    }

    protected function parseNode($dom){
        $nodes = Dom::getFirstElement($dom, "//div[@class='price-wrapper']/p[1]/span[1]");

        if(!empty($nodes)){
            $nodes = collect(explode("₫", $nodes))
            ->reject(fn ($val): bool => empty($val))
            ->values()->toArray();
            if(count($nodes) === 2){
                $currentPrice = $nodes[0];
                $initialPrice = $nodes[1];
                return [$currentPrice, $initialPrice];
            }
            if(count($nodes) === 1){
                $currentPrice = $nodes[0];
                return [$currentPrice, null];
            }
            return ['', ''];
        }
    }
}
