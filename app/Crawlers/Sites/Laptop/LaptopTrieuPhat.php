<?php

namespace App\Crawlers\Sites\Laptop;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use App\Models\ProductOffice;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class LaptopTrieuPhat implements CrawlableSite
{
    private const BASE_URL = 'https://laptoptrieuphat.com/dell-inspiron-7300-2n1-i5-10210u/';
    public const DOMAIN = 'laptoptrieuphat.com';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [
            RequestOptions::ALLOW_REDIRECTS => true,
        ];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - It is root page
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {

        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (preg_match('#^\/#', $url->getPath())) {
            return true;
        }
        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//a[@class='logo']/img/@src");
        $img = Dom::getFirstElement($dom, "//div[@class='main-thumbnail mb-10px']/a/img/@src");
        $images = Dom::getElements($dom, "//div[@class='bgwhite']/div[last()-2]/p[1]/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='title-pro-sing']");
        $initialPrice = Dom::getFirstElement($dom, "//div[@class='price-sing']/del | | //div[@class='cauhinh active']/div/strong");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='price-sing']/ins | //div[@class='price-sing']/span[last()] | //div[@class='cauhinh']/div/strong");
        $reduce = Dom::getFirstElement($dom, "//div[@class='price-sing']/span[last()]");
        $cpu = Dom::getFirstElement($dom, "//span[contains(text(),'CPU')]/following-sibling::span");
        $lcd = Dom::getFirstElement($dom, "//span[contains(text(),'LCD')]/following-sibling::span");
        $cardVGA = Dom::getFirstElement($dom, "//span[contains(text(),'Card VGA')]/following-sibling::span");
        $description = Dom::getFirstElement($dom, "//div[@class='bgwhite']");
        $hotline = Dom::getFirstElement($dom, "//div[@class='hotline']/a/text()");
        $address = Dom::getFirstElement($dom, "//div[@class='firsthead']/following-sibling::div");
        $email = Dom::getElements($dom, "//p[contains(text(), 'Email')]/following-sibling::p");
        $workingHours = Dom::getFirstElement($dom, "//p[contains(text(), 'Giờ mở cửa')]/following-sibling::p");
        $insurance = Dom::getFirstElement($dom, "//p[contains(text(), 'Nhận bảo hành')]/following-sibling::p");
        $zalo = Dom::getFirstElement($dom, "//p[contains(text(), 'Zalo')]/following-sibling::p");
        $orderStatus = Dom::getFirstElement($dom, "//div[@class='alert alert-danger']/text() | //span[@class='statuspro text-success']/text()");
        $productCode = substr($url->getPath(), 1, -5);

        $category_id = ProductCategory::updateOrCreate([
            'name' => 'Laptop'
        ]);

        if(!is_null($name)){
            $product_id = ProductOffice::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode,
                'connect' => $connect ?? null,
                'hard_drive' => $hardDrive ?? null,
            ]);

            $data = Product::create([
                'website' => $url->__toString(),
                'name' => $name ?? '',
                'crawled_at' => now()->format('Y-m-d H:i:s'),
                'address' => $address ?? null,
                'tel' => '0939.008.008',
                'initial_price' => $initialPrice ?? null,
                'current_price' => $currentPrice ?? null,
                'reduce' => $reduce ?? null,
                'vga' => $cardVGA ?? null,
                'cpu' => $cpu ?? null,
                'lcd' => $lcd ?? null,
                'email' => $email[0] ?? null,
                'working_hours' => $workingHours ?? null,
                'insurance' => $insurance ?? null,
                'zalo' => $zalo ?? null,

                'hotline' => $hotline ?? '',
                'order_status' => $orderStatus ?? '',
                'logo' => $logo ?? '',
                'img' => $img ?? '',
                'description' => $description ?? null,
                'office_id' => $product_id->id,
                'category_id' => $category_id->id
            ]);

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }

        return [];
    }

}
