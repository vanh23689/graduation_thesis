<?php

namespace App\Crawlers\Sites\Tablet;

use App\Supports\Dom;
use App\Models\Product;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Str;
use GuzzleHttp\RequestOptions;
use App\Crawlers\CrawlableSite;
use App\Models\ProductCategory;
use Psr\Http\Message\UriInterface;
use App\Crawlers\MustExecuteJavascript;
use Psr\Http\Message\ResponseInterface;
use App\Models\ProductTelecommunication;
use Symfony\Component\DomCrawler\Crawler;

class Cellphones implements CrawlableSite, MustExecuteJavascript
{
    private const BASE_URL = 'https://cellphones.com.vn/dong-ho-thong-minh-huawei-watch-gt-3-pro.html';
    public const DOMAIN = 'cellphones.com.vn';

    /**
     * Khai báo sử dụng trình thu thập thông tin với Guzzle phía máy khách (Options for Guzzle client)
     *
     * @see \Spatie\Crawler\Crawler::$defaultClientOptions
     * @return array
     */
    public function clientOptions(): array
    {
        return [];
    }

    /**
     * URL bắt đầu thu thập thông tin
     *
     * @return \Psr\Http\Message\UriInterface
     */
    public function startUrl(): UriInterface
    {
        return Utils::uriFor(self::BASE_URL);
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        // It should crawl all URLs has same domain and pass any rules
        // - It is root page
        if (
            $url->getHost() === self::DOMAIN &&
            (
                preg_match('#^\/dong-ho.*#', $url->getPath()) ||
                preg_match('#^\/vong-deo.*#', $url->getPath()) ||
                preg_match('#^\/apple-watch.*#', $url->getPath())
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xác định xem URL đã cho có nên được xuất hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldExport(UriInterface $url): bool
    {
        // Only crawls data from detail page (it has path start with /san-pham/** )
        if (
            preg_match('#^\/dong-ho.*#', $url->getPath()) ||
            preg_match('#^\/vong-deo.*#', $url->getPath()) ||
            preg_match('#^\/apple-watch.*#', $url->getPath())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Xuất dữ liệu từ phản hồi
     *
     * @param  \Psr\Http\Message\UriInterface      $url
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @return array
     */
    public function export(UriInterface $url, ResponseInterface $response): array
    {
        $html = $response->getBody()->__toString();
        $dom  = new Crawler($html, $url->__toString());

        $logo = Dom::getFirstElement($dom, "//*[local-name()='svg'][1]");
        $img = Dom::getFirstElement($dom, "//div[@class='box-ksp']/img/@src");
        $name = Dom::getFirstElement($dom, "//h1[@class='title mt-2 product-title has-text-centered']/text() | //div[@class='box-product-name']/h1/text()");
        $description = Dom::getFirstElement($dom, "//meta[@name='description']/@content");
        $currentPrice = Dom::getFirstElement($dom, "//div[@class='boxInfoRight']/span/strong | //p[@class='product__price--show']/text()");
        $initialPrice = Dom::getFirstElement($dom, "//p[@class='product__price--through']/text()");
        $cpu = Dom::getFirstElement($dom, "//p[contains(text(),'Loại CPU')]/following-sibling::div");
        $size = Dom::getFirstElement($dom, "//p[contains(text(),'Kích thước màn hình')]/following-sibling::div");
        $operatingSystem = Dom::getFirstElement($dom, "//p[contains(text(),'Hệ điều hành')]/following-sibling::div");
        $pin = Dom::getFirstElement($dom, "//p[contains(text(), 'Pin')]/following-sibling::div");
        $bluetooth = Dom::getFirstElement($dom, "//p[contains(text(), 'Bluetooth')]/following-sibling::div");
        $screen = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ màn hình')]/following-sibling::div");
        $ram = Dom::getFirstElement($dom, "//p[contains(text(), 'Bộ nhớ trong')]/following-sibling::div");
        $beforeCamera = Dom::getFirstElement($dom, "//p[contains(text(),'Camera trước')]/following-sibling::div");
        $beforeVideo = Dom::getFirstElement($dom, "//p[contains(text(), 'Quay video trước')]/following-sibling::div");
        $afterCamera = Dom::getFirstElement($dom, "//p[contains(text(), 'Camera sau')]/following-sibling::div");
        $afterVideo = Dom::getFirstElement($dom, "//p[contains(text(), 'Quay video')]/following-sibling::div");
        $memory = Dom::getFirstElement($dom, "//p[contains(text(), 'Bộ nhớ trong')]/following-sibling::div");
        $wifi = Dom::getFirstElement($dom, "//p[contains(text(), 'Wi-Fi')]/following-sibling::div");
        $nfc = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ NFC')]/following-sibling::div");
        $networkSupport = Dom::getFirstElement($dom, "//p[contains(text(), 'Hỗ trợ mạng')]/following-sibling::div");
        $chargingTechnology = Dom::getFirstElement($dom, "//p[contains(text(), 'Công nghệ sạc')]/following-sibling::div");
        $fingerprintSensor = Dom::getFirstElement($dom, "//p[contains(text(), 'Cảm biến vân tay')]/following-sibling::div");
        $typesOfSensors = Dom::getFirstElement($dom, "//p[contains(text(), 'Các loại cảm biến')]/following-sibling::div");
        $sim = Dom::getFirstElement($dom, "//p[contains(text(), 'Thẻ SIM')]/following-sibling::div");
        $headphone = Dom::getFirstElement($dom, "//p[contains(text(), 'Jack tai nghe 3.5')]/following-sibling::div");
        $zalo = Dom::getFirstElement($dom, "//a[@id='btnZaloChat']/@href");
        $specialFeatures = Dom::getFirstElement($dom, "//p[contains(text(), 'Tính năng đặc biệt')]/following-sibling::div");
        $graphics = Dom::getFirstElement($dom, "//p[contains(text(), 'GPU')]/following-sibling::div");
        $chipset = Dom::getFirstElement($dom, "//p[contains(text(), 'Chipset')]/following-sibling::div");
        $productCode = substr($url->getPath(), 1, -5);

        if(!is_null($name)){
            $category_id = ProductCategory::updateOrCreate([
                'name' => 'Tablet'
            ]);
            $product_id = ProductTelecommunication::updateOrCreate(['product_code' => $productCode], [
                'product_code' => $productCode,
                'before_camera' => $beforeCamera ?? null,
                'before_video' => $beforeVideo ?? null,
                'after_camera' => !empty($afterCamera) ? ((Str::length($afterCamera) > 255) ? null : $afterCamera) : null,
                'after_video' => $afterVideo ?? null,
                'fingerprint_sensor' => $fingerprintSensor ?? null,
                'special_features' => $specialFeatures ?? null,
                'types_of_sensors' => $typesOfSensors ?? null,
                'chipset' => $chipset ?? null,
                'sim' => $sim ?? null,
                'memory' => $memory ?? null,
            ]);

            $data = Product::create(
                [
                    'website' => $url->__toString(),
                    'name' => $name ?? '',
                    'crawled_at' => now()->format('Y-m-d H:i:s'),
                    'address' => '350-352 Võ Văn Kiệt, Phường Cô Giang, Quận 1, Thành phố Hồ Chí Minh, Việt Nam',
                    'tel' => '028.7108.9666',
                    'initial_price' => $initialPrice ?? null,
                    'current_price' => $currentPrice ?? null,
                    'cpu' => $cpu ?? null,
                    'ram' => $ram ?? null,
                    'screen' => $screen ?? null,
                    'size' => $size ?? null,
                    'logo' => $logo ?? null,
                    'operating_system' => $operatingSystem ?? null,
                    'graphics' => $graphics ?? null,
                    'pin' => $pin ?? null,
                    'bluetooth' => $bluetooth ?? null,
                    'headphone' => $headphone ?? null,
                    'zalo' => $zalo ?? null,
                    'wifi' => $wifi ?? null,
                    'nfc' => $nfc ?? null,
                    'network_support' => $networkSupport ?? null,
                    'charging_technology' => $chargingTechnology ?? null,
                    'insurance' => 'Gọi bảo hành 1800.2064 (8h00 - 21h00)',

                    'img' => $img ?? null,
                    'description' => $description ?? null,
                    'telecommunication_id' => $product_id->id,
                    'category_id' => $category_id->id
                ]
            );

            $product = Product::find($data->id);
            if(!empty($images)){
                foreach($images as $img){
                    $product->images()->createMany([
                        [
                            'image_url' => $img,
                            'product_id' => $data->id,
                        ]
                    ]);
                }
            }
        }
        return [];
    }
}
