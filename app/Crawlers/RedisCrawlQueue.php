<?php

namespace App\Crawlers;

use GuzzleHttp\Psr7\UriNormalizer;
use Illuminate\Contracts\Redis\Connection;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlQueues\CrawlQueue;
use Spatie\Crawler\CrawlUrl;
use Spatie\Crawler\Exceptions\UrlNotFoundByIndex;

class RedisCrawlQueue implements CrawlQueue
{
    // All known URLs, indexed by URL string.
    public const URLS = 'urls';

    // Pending URLs, indexed by URL string.
    public const PENDING_URLS = 'pending';

    /**
     * @var \Illuminate\Contracts\Redis\Connection
     */
    private $redis;

    /**
     * @var string
     */
    private $prefix;

    /**
     * Constructor
     *
     * @param \Illuminate\Contracts\Redis\Connection $redis
     */
    public function __construct(Connection $redis, ?string $prefix = null)
    {
        $this->redis = $redis;
        $this->prefix = $prefix ?? uniqid('crawl') . ':';

        // make sure prefix has a colon at the end
        if (substr($this->prefix, -1) !== ':') {
            $this->prefix .= ':';
        }
    }

    public function add(CrawlUrl $url): self
    {
        $id = $this->hash($url->url);

        $url->setId($id);
        if ($this->hSetNx(self::URLS, $id, serialize($url))) {
            $this->hSetNx(self::PENDING_URLS, $id, true);
        }

        return $this;
    }

    public function has(CrawlUrl | UriInterface $crawlUrl): bool
    {
        $id = $this->hash($crawlUrl instanceof CrawlUrl ? $crawlUrl->url : $crawlUrl);

        return (bool) $this->hExists(self::URLS, $id);
    }

    public function hasPendingUrls(): bool
    {
        return (bool) $this->hLen(self::PENDING_URLS);
    }

    /**
     * Get CrawlUrl by ID
     *
     * @param  string $id
     * @return \Spatie\Crawler\CrawlUrl
     * @throws \Spatie\Crawler\Exceptions\UrlNotFoundByIndex
     */
    public function getUrlById($id): CrawlUrl
    {
        if (!$this->hExists(self::URLS, $id)) {
            throw new UrlNotFoundByIndex("Crawl url {$id} not found in hashes.");
        }

        return unserialize($this->hGet(self::URLS, $id));
    }

    public function getPendingUrl(): ?CrawlUrl
    {
        $keys = $this->hKeys(self::PENDING_URLS);

        foreach ($keys as $id) {
            try {
                return $this->getUrlById($id);
            } catch (\Throwable $th) {
                $this->hDel(self::PENDING_URLS, $id);
                //throw $th;
            }
        }

        return null;
    }

    public function hasAlreadyBeenProcessed(CrawlUrl $url): bool
    {
        $id = $this->hash($url->url);

        if ($this->hExists(self::PENDING_URLS, $id)) {
            return false;
        }

        if ($this->hExists(self::URLS, $id)) {
            return true;
        }

        return false;
    }

    public function markAsProcessed(CrawlUrl $crawlUrl): void
    {
        $id = $this->hash($crawlUrl->url);

        $this->hDel(self::PENDING_URLS, $id);
    }

    public function getProcessedUrlCount(): int
    {
        return $this->hLen(self::URLS) - $this->hLen(self::PENDING_URLS);
    }

    /**
     * Adds a value to the hash stored at key only if this field isn't already in the hash.
     * Return TRUE if the field was set, FALSE if it was already present.
     *
     * @param  string $key
     * @param  string $hashKey
     * @param  mixed  $value
     * @return bool
     */
    private function hSetNx(string $key, string $hashKey, mixed $value): bool
    {
        return (bool) $this->redis->hSetNx($this->key($key), $hashKey, $value);
    }

    /**
     * Gets a value from the Redis hash stored at key.
     * If the hash table doesn't exist, or the key doesn't exist, FALSE is returned.
     *
     * @param string $key
     * @param string $hashKey
     * @return mixed
     */
    private function hGet(string $key, string $hashKey): mixed
    {
        return $this->redis->hGet($this->key($key), $hashKey);
    }

    /**
     * Verify if the specified member exists in a key.
     * If the member exists in the hash table, return TRUE, otherwise return FALSE.
     *
     * @param  string $key
     * @param  string $hashKey
     * @return bool
     */
    private function hExists(string $key, string $hashKey): bool
    {
        return (bool) $this->redis->hExists($this->key($key), $hashKey);
    }

    /**
     * Removes a value from the Redis hash stored at key.
     * If the hash table doesn't exist, or the key doesn't exist, FALSE is returned.
     *
     * @param  string $key
     * @param  string $hashKey
     * @return bool
     */
    private function hDel(string $key, string $hashKey): bool
    {
        return $this->redis->hDel($this->key($key), $hashKey);
    }

    /**
     * Returns the length of a Redis hash, in number of items
     *
     * @param  string $key
     * @return int
     */
    private function hLen(string $key): int
    {
        return (int) $this->redis->hLen($this->key($key));
    }

    /**
     * Returns the keys in a Redis hash, as an array of strings.
     *
     * @param  string $key
     * @return array|string[]
     */
    private function hKeys(string $key): array
    {
        return $this->redis->hKeys($this->key($key));
    }

    /**
     * Add prefix to Redis key
     *
     * @param  string $type
     * @return string
     */
    private function key(string $type): string
    {
        return $this->prefix . $type;
    }

    /**
     * Hash uri by MD5
     *
     * @param  \Psr\Http\Message\UriInterface $uri
     * @return string
     */
    private function hash(UriInterface $uri): string
    {
        // Normalize, remove fragment and replace scheme with `https`
        $normalizedUri = $this->normalizeUri($uri)
            ->withFragment('')
            ->withScheme('https');

        return md5((string) $normalizedUri);
    }

    /**
     * Normalize uri
     *
     * @param  \Psr\Http\Message\UriInterface $uri
     * @return \Psr\Http\Message\UriInterface
     */
    private function normalizeUri(UriInterface $uri): UriInterface
    {
        return UriNormalizer::normalize($uri, UriNormalizer::PRESERVING_NORMALIZATIONS
            | UriNormalizer::REMOVE_DUPLICATE_SLASHES
            | UriNormalizer::SORT_QUERY_PARAMETERS);
    }
}
