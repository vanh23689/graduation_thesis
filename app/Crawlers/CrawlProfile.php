<?php

namespace App\Crawlers;

use App\Crawlers\CrawlableSite;
use Spatie\Crawler\CrawlProfiles\CrawlProfile as BaseCrawlProfile;
use Psr\Http\Message\UriInterface;

class CrawlProfile extends BaseCrawlProfile
{
    /**
     * @var \App\Crawlers\CrawlableSite
     */
    protected $site;

    /**
     * Constructor
     *
     * @param  \App\Crawlers\CrawlableSite  $site
     * @param  \Psr\Log\LoggerInterface|null $logger
     */
    public function __construct(CrawlableSite $site)
    {
        $this->site = $site;
    }

    /**
     * Xác định xem URL đã cho có nên được thu thập thông tin hay không.
     *
     * @param  \Psr\Http\Message\UriInterface $url
     * @return bool
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        return $this->site->shouldCrawl($url);
    }
}
