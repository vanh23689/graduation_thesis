<?php

namespace App\Crawlers;

use App\Models\Product;
use Psr\Log\NullLogger;
use Illuminate\Support\Arr;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Spatie\Crawler\CrawlObservers\CrawlObserver as BaseCrawlObserver;

class CrawlObserver extends BaseCrawlObserver
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \App\Crawlers\CrawlableSite
     */
    protected $site;

    /**
     * Constructor
     *
     * @param  \App\Crawlers\CrawlableSite  $site
     * @param  \Psr\Log\LoggerInterface|null $logger
     */
    public function __construct(CrawlableSite $site, ?LoggerInterface $logger = null)
    {
        $this->site = $site;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * Called when the crawler will crawl the url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {
        $this->logger->debug('Start crawling', [
            'path' => $url->getPath(),
        ]);

    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    ): void {
        $this->logger->debug('Crawled', [
            'status' => $response->getStatusCode(),
            'path' => $url->getPath(),
        ]);
        // Only save data from detail page
        if (!$this->site->shouldExport($url)) {
            return;
        }

        $data = $this->site->export($url, $response);
        Product::query()
            ->updateOrCreate(Arr::only($data, ['website', 'name', 'registration_code']), $data);
        // $this->logger->debug('Saved info ' . print_r($data, true));
    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \GuzzleHttp\Exception\RequestException $requestException
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void {
        $this->logger->debug('Crawl failed', [
            'status' => $requestException->getResponse()?->getStatusCode(),
            'path' => $url->getPath(),
        ]);
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        $this->logger->debug('Crawling finished');
    }
}
