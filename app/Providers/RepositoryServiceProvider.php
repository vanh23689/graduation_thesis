<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\DonateInterface;
use App\Interfaces\CustomerInterface;
use App\Repositories\DonateRepository;
use App\Repositories\CustomerRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DonateInterface::class, DonateRepository::class);
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
