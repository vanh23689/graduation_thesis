<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arrange extends Model
{
    use HasFactory;
    protected $table = 'arrange';
    protected $guarded = ['name'];
}
