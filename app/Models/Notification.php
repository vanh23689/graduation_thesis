<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    public function customer_authentications(){
        return $this->hasOne(CustomerAuthentication::class, 'customer_id', 'notifiable_id');
    }
}
