<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    use HasFactory;
    protected $fillable = ['customer_id', 'facebook_id', 'google_id', 'social_token', 'social_refresh_token', 'email'];

    public function customers()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
