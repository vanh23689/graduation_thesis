<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'registration_date' => 'date',
        'crawled_at' => 'datetime',
    ];

    public function categories(){
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function civils(){
        return $this->belongsTo(ProductCivil::class, 'civil_id');
    }

    public function offices(){
        return $this->belongsTo(ProductOffice::class, 'office_id');
    }

    public function telecommunitions(){
        return $this->belongsTo(ProductTelecommunication::class, 'telecommunication_id');
    }

    public function users(){
        return $this->belongsTo(User::class);
    }

    public function images(){
        return $this->hasMany(ProductImage::class);
    }
}
