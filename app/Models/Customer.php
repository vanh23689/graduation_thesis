<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Http\Controllers\Auth\Customer as Authenticatable;

class Customer extends Authenticatable
{
    use HasFactory;
    use Notifiable, SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */

    public function routeNotificationForSlack($notification)
    {
        return 'https://hooks.slack.com/services/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';
    }

    public function social_account()
    {
        return $this->hasOne(SocialAccount::class, 'customer_id');
    }
}
