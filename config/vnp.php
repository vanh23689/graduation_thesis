<?php
return [
    'vnp_TmnCode' => env('VNP_TMNCODE', 0),
    'vnp_HashSecret' => env('VNP_HASHSECRET', 0),
    'vnp_Url' => env('VNP_URL', null),
    'vnp_Returnurl' => env('VNP_RETURNURL', null),
    'vnp_Apiurl' => env('VNP_APIURL', null),
];
