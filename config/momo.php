<?php
return [
    'partner_Code' => env('PARTNER_CODE', null),
    'access_Key' => env('ACCESS_KEY', null),
    'secret_Key' => env('SECRET_KEY', null)
];
