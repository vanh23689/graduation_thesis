<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\DonateController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\GetAddressController;
use App\Http\Controllers\Api\ProductCategoryController;
use App\Http\Controllers\Api\NotificationSendController;

Route::get('get-zip-code/{zip_code}', [GetAddressController::class, 'getZipCode'])->name('api.get_zip_code');
Route::get('get-city', [GetAddressController::class, 'getCity'])->name('api.get_city');
Route::get('get-provinces/{code}', [GetAddressController::class, 'getProvince'])->name('api.get_provinces');
Route::get('get-ward/{code}', [GetAddressController::class, 'getWard'])->name('api.get_ward');
Route::group(['middleware' => 'auth:sanctum', 'as' => 'api.', 'verified'], function () {
    // User
    Route::resource('/users', UserController::class);
    // Route::post('/user_create_local', [UserController::class, 'UserCreateLocal'])->name('users.create_local');
    // Avatar
    Route::put('/upload-user/{id}', [UserController::class, 'upload'])->name('user.upload');
    //Role
    Route::resource('roles', RoleController::class);
    // Products
    Route::resource('/products', ProductController::class);
    Route::put('/upload-product-logo/{id}', [ProductController::class, 'uploadLogoProduct'])->name('product-logo.upload');
    Route::put('/upload-product-image/{id}', [ProductController::class, 'uploadImageProduct'])->name('product-image.upload');
    // Notification
    Route::get('/get-notification', [NotificationSendController::class, 'getNotification'])->name('get-notification');
    Route::put('/read-at/{id}', [NotificationSendController::class, 'readAt'])->name('read-at');
    // Product Categories
    Route::resource('/product_category', ProductCategoryController::class);

    // Customer
    Route::resource('/customers', CustomerController::class);
    Route::get('/show-customers', [CustomerController::class, 'showCustomer'])->name('showCustomers');
    // Donates
    Route::get('/donates', [DonateController::class, 'getListDonate'])->name('donates.index');
    Route::delete('/donates/{id}', [DonateController::class, 'destroy'])->name('donates.destroy');

    // Top 10 products
    Route::get('/top_products', [ProductController::class, 'getTopProducts'])->name('top_products.index');

    // get-detail-account-to-time
    Route::get('/get-detail-donate-time', [DonateController::class, 'getDetailDonateToTime'])
    ->name('get.donate.time');
});
