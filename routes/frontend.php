<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DonateController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\LoginController;
use App\Http\Controllers\Frontend\SocialController;
use App\Http\Controllers\Frontend\CompareController;
use App\Http\Controllers\Frontend\RegisterController;
use App\Http\Controllers\Frontend\WishlistController;
use App\Http\Controllers\Frontend\Cart\CartController;
use App\Http\Controllers\Frontend\NewsLetterController;
use App\Http\Controllers\Frontend\Product\DetailController;
use App\Http\Controllers\Frontend\Product\CategoryController;
use App\Http\Controllers\Frontend\VerifyCustomerEmailController;

Route::prefix('')->group(function(){
    Route::get('home', [HomeController::class, 'index'])->name('home.index');
    Route::get('show-modal/{id}', [HomeController::class, 'show'])->name('home.show');
    Route::get('wishlist', [wishlistController::class, 'index'])->name('wishlist.index');

    // Customer
    Route::post('register-customer', [RegisterController::class, 'register'])->name('register-customer');
    Route::post('login-customer', [LoginController::class, 'login'])->name('login-customer');
    // Social
    Route::controller(SocialController::class)->group(function () {
        Route::get('auth/{provider}', 'redirectToSocial')->name('auth.social');
        Route::get('auth/{provider}/callback', 'handleSocialCallback');
    });

    // Email verification
    Route::get('verify-email/{id}/{token}', [VerifyCustomerEmailController::class, 'verify'])
                                            ->middleware(['throttle:6,1'])
                                            ->name('verify-email.customer');
    // Cart
    Route::get('cart/{id}', [CartController::class, 'cart'])->name('cart');
    Route::get('add-to-cart/{id}', [CartController::class, 'addToCart'])->name('add.to.cart');
    Route::patch('update-cart', [CartController::class, 'update'])->name('update.cart');
    Route::delete('remove-from-cart', [CartController::class, 'removeCart'])->name('remove.from.cart');
    // End cart

    // wishlist
    Route::get('wishlist/{id}', [CartController::class, 'wishlist'])->name('wishlist');
    Route::get('add-to-wishlist/{id}', [CartController::class, 'addToWishlist'])->name('add.to.wishlist');
    Route::delete('remove-from-wishlist', [CartController::class, 'removeWishlist'])->name('remove.from.wishlist');
    // End wishlist
    Route::delete('remove-product-to-compare', [CompareController::class, 'removeProdToCompare'])->name('remove.from.product_to_compare');
    // Product to compare

    // End product to compare
    // Product Category
    Route::get('product-categories/{productCategory}', [CategoryController::class, 'show'])->name('product-category.show');
    // End Product Category

    // Product Detail
    Route::get('product-detail/{productDetail}', [DetailController::class, 'show'])->name('product-detail.show');
    // End Product Detail

    // send join us
    Route::post('/subscribe', [NewsLetterController::class, 'subscribe'])->name('subscribe');

    // compare
    Route::get('/compare', [CompareController::class, 'index'])->name('compare.index');
    Route::post('/compare-post', [CompareController::class, 'postCompare'])->name('compare.post');
    Route::get('/compare-get', [CompareController::class, 'getCompare'])->name('compare.get');

    // VN Pay
    Route::post('/donate-create', [DonateController::class, 'create'])->name('donate.create');
    Route::get('/donate-vnpay', [DonateController::class, 'index'])->name('donate.vnpay.index');
    Route::get('/vnpay_php/vnpay_return', [DonateController::class, 'return'])->name('return.index');

    Route::post('/transaction', [DonateController::class, 'transaction'])->name('transaction.post');
    Route::post('/refund', [DonateController::class, 'refund'])->name('refund.post');
    // Momo

    Route::get('/atm-momo', [DonateController::class, 'atm_momo'])->name('donate.momo.index');
    Route::get('/result-atm', [DonateController::class, 'result_atm'])->name('donate.result.index');

    // filter price
    Route::post('/arrange-price', [CategoryController::class, 'arrangePrice'])->name('arrange.price');
    // logout
    Route::get('/logout', [HomeController::class, 'logout'])->name('logout.customer');
});
