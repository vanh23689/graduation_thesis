<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\SlackController;
use App\Http\Controllers\Backend\DonateController;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\Backend\CustomerController;
use App\Http\Controllers\Backend\DiscountController;
use App\Http\Controllers\Backend\RegisterController;
use App\Http\Controllers\Backend\Users\UserController;
use App\Http\Controllers\Backend\NotificationSendController;
use App\Http\Controllers\Backend\Products\ProductController;
use App\Http\Controllers\Backend\Email\VerifyEmailController;
use App\Http\Controllers\Backend\Users\UserProfileController;
use App\Http\Controllers\Backend\Password\NewPasswordController;
use App\Http\Controllers\Backend\Products\ProductCategoryController;
use App\Http\Controllers\Backend\Password\PasswordResetLinkController;
use App\Http\Controllers\Backend\Password\ConfirmablePasswordController;
use App\Http\Controllers\Backend\Email\EmailVerificationPromptController;
use App\Http\Controllers\Backend\Email\EmailVerificationNotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Authentication Routes...
Route::prefix('admin')->name('admin.')->group(function(){
    Route::middleware('guest')->group(function () {
        // Start Login
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login'])->name('login.post');
        // End Login
        // Start Register
        Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [RegisterController::class, 'create'])->name('register.post');
        // End Register
        });
        // End Password Confirm
    Route::middleware('auth')->group(function(){
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
        // Users
        Route::resource('users', UserController::class);
        Route::get('user_profile', [UserProfileController::class, 'index'])->name('user_profile.index');
        Route::get('user_profile/edit/{user}', [UserProfileController::class, 'edit'])->name('user_profile.edit');
        // Roles
        Route::resource('roles', RoleController::class);
        // Products
        Route::resource('products', ProductController::class);
        // Customers
        Route::resource('customers', CustomerController::class);
        // Product Categories
        Route::resource('product_category', ProductCategoryController::class);
        // Donate
        Route::get('donates', [DonateController::class, 'index'])->name('donates.index');
        Route::get('donate-edit', [DonateController::class, 'edit'])->name('donates.edit');
        //Coupon
        Route::get('coupon', [DiscountController::class, 'index'])->name('coupon.index');
        // notice
        Route::get('/notice', [NotificationSendController::class, 'index'])->name('notice.index');
        Route::post('/store-token', [NotificationSendController::class, 'updateDeviceToken'])->name('store.token');
        Route::post('/send-web-notification', [NotificationSendController::class, 'sendNotification'])->name('send.web-notification');
        //Logout
        Route::get('/logout', [DashboardController::class, 'destroy'])->name('logout');
    });
});
Route::prefix('admin')->group(function(){
    // Email
    Route::get('verify-email', [EmailVerificationPromptController::class, '__invoke'])
    ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('verification.verify');

    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
            ->middleware('throttle:6,1')
            ->name('verification.send');
    // End Email
    // Password
    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
    ->middleware('guest')
    ->name('password.request');

    Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
    ->middleware('guest')
    ->name('password.email');

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
    ->middleware('guest')
    ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
    ->middleware('guest')
    ->name('password.update');

    // Password Confirm
    Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
            ->name('password.confirm');

    Route::post('confirm-password', [ConfirmablePasswordController::class, 'store']);
    // End Password

    Route::get('send', [DashboardController::class, 'send'])->name('home.send');

    // Search
    Route::post('search/name', [SearchController::class, 'getSearchAjax'])->name('search');

    // Notification Slack
    Route::get('slack', [SlackController::class,'slack'])->name('slack');

    // Impersonate
    Route::get('/{user}/impersonate', [UserController::class, 'impersonate'])->name('users.impersonate');
    Route::get('/leave-impersonate', [UserController::class, 'leaveImpersonate'])->name('users.leave-impersonate');

});

