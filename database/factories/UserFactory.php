<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'first_name' => fake()->name(),
            'last_name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'phone' => fake()->numberBetween(),
            'email_verified_at' => now(),
            'password' => '$2y$10$HfuPxSDIP6Ie0uxI6QNHb.cZPhOrA/9a2EtSd0u/zQImyprZUXSGq', // password
            'check_login_times' => 0,
            'remember_token' => Str::random(10),
        ];
        Role::create(['guard_name' => 'web', 'name' => 'user']);
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,

            ];
        });
    }
}
