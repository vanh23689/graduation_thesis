<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'phone' => fake()->numberBetween(),
            'gender' => fake()->randomElement(['male', 'female', 'other']),
            'password' => '$2y$10$HfuPxSDIP6Ie0uxI6QNHb.cZPhOrA/9a2EtSd0u/zQImyprZUXSGq', // password
            'remember_token' => Str::random(10),
            'email_verified_at' => now()
        ];
    }
}
