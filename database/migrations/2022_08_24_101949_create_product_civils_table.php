<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_civils', function (Blueprint $table) {
            // đồ gia dụng
            $table->id();
            $table->string('product_code')->nullable();
            $table->string('capacity')->nullable(); // dung tích
            $table->string('deodorant')->nullable(); // khử mùi
            // start tivi
            $table->string('processor')->nullable(); // bộ vi xử lí
            $table->string('total_peaker_power')->nullable(); // tổng công suất loa
            $table->string('number_of_speakers')->nullable(); // số lượng loa
            $table->string('smart_sharing')->nullable(); // chia sẻ thông minh
            $table->string('dimensions_with_stand')->nullable(); // kích thước chân đế
            $table->string('bracket_weight')->nullable(); // khối lượng chân đế
            // end tivi
            // start televisions
            $table->string('door_number')->nullable(); // số cửa
            $table->string('freezer_capacity')->nullable();//dung tích ngăn đá
            $table->string('freezer_compartment_capacity')->nullable(); // dung tích ngăn lạnh
            $table->string('snow')->nullable();
            $table->string('refrigerator_technology')->nullable(); // công nghệ làm lạnh
            $table->string('tray_material')->nullable(); // chất liệu khay
            $table->string('outer_material')->nullable(); //chất liệu bên ngoài
            $table->string('automatic_ice_maker')->nullable(); // làm đá tự động
            $table->string('get_outside_water')->nullable(); // lấy nước bên ngoài
            $table->string('door_bell')->nullable(); // chuông báo cửa
            // end televisions
            // start cooker
            $table->string('material')->nullable(); // chất liệu
            $table->string('regime')->nullable(); // chế độ
            $table->string('steaming_tray')->nullable(); // khay hấp
            // end cooker
            // start air-conditioner
            $table->string('minimum_cooling_rate')->nullable(); // Tốc độ làm lạnh tối thiểu
            $table->string('average_cooling_speed')->nullable();// Tốc độ làm lạnh trung bình
            $table->string('maximum_cooling_speed')->nullable();// Tốc độ làm lạnh tối đa
            $table->string('rapid_cooling')->nullable(); // làm lạnh nhanh
            $table->string('error_guessing')->nullable(); // tự đoán lỗi
            $table->string('auto_restart')->nullable(); // tự khởi động
            $table->string('mosquito_repellent')->nullable(); // Xua muỗi
            $table->string('gas')->nullable(); // Gas
            $table->string('hygroscopic')->nullable(); // Hút ẩm
            $table->string('noise_level')->nullable(); // độ ồn
            $table->string('effective_range')->nullable(); // Phạm vi hiệu quả
            // end air-conditioner
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_civils');
    }
};
