<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('website')->nullable();
            $table->string('tel')->nullable();
            $table->string('hotline')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('initial_price')->nullable();
            $table->string('current_price')->nullable();
            $table->string('size')->nullable();
            $table->string('working_hours')->nullable();
            $table->text('description')->nullable();
            $table->string('reduce')->nullable();
            $table->string('insurance')->nullable();
            $table->string('name_image')->nullable();
            $table->string('logo')->nullable();
            $table->string('cpu')->nullable();
            $table->string('lcd')->nullable();
            $table->string('ram')->nullable();
            $table->string('img')->nullable();
            $table->string('screen')->nullable();
            $table->string('weight')->nullable();
            $table->string('touch')->nullable(); // cảm ứng
            $table->string('operating_system')->nullable(); // hệ điều hành
            $table->string('origin')->nullable();
            $table->string('version')->nullable();
            $table->string('hdmi')->nullable(); // cổng hdmi
            $table->string('zalo')->nullable();
            $table->string('pin')->nullable();
            $table->string('bluetooth')->nullable();
            $table->string('wifi')->nullable();
            $table->string('voice')->nullable();
            $table->string('usb')->nullable();
            $table->string('graphics')->nullable(); // đồ họa
            $table->string('resolution')->nullable(); // độ phân giải
            $table->string('network_support')->nullable();
            $table->string('charging_technology')->nullable();
            $table->string('type')->nullable(); // kiểu đồ dùng
            $table->string('image_processing')->nullable(); // xử lí hình ảnh
            $table->text('audio')->nullable(); // xử lí âm thanh
            $table->string('speakerphone')->nullable(); //loa ngoài
            $table->string('headphone')->nullable();
            $table->string('wattage')->nullable(); // công suất
            $table->string('warranty_period')->nullable(); // hạn bảo hành
            $table->string('order_status')->nullable();
            $table->string('nfc')->nullable();
            $table->string('vga')->nullable();
            $table->string('producer')->nullable();
            $table->timestamp('crawled_at')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('civil_id')->nullable();
            $table->unsignedBigInteger('office_id')->nullable();
            $table->unsignedBigInteger('telecommunication_id')->nullable();
            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');
            $table->foreign('civil_id')->references('id')->on('product_civils')->onDelete('cascade');
            $table->foreign('office_id')->references('id')->on('product_offices')->onDelete('cascade');
            $table->foreign('telecommunication_id')->references('id')->on('product_telecommunications')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
