<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donates', function (Blueprint $table) {
            $table->id();
            $table->string('order_id'); //mã đơn hàng
            $table->text('order_info'); // nội dung thanh toán
            $table->bigInteger('amount'); // số tiền
            $table->string('bank_code');
            $table->string('bank_no')->nullable(); //mã giao dịch tại ngân hàng
            $table->string('transaction_no'); // mã giao dịch tại vnpay
            $table->string('transaction_status'); // mã phản hồi //00: thành công, !00: thất bại
            $table->string('card_type')->nullable();
            $table->string('pay_date');//thời gian thanh toán
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donates');
    }
};
