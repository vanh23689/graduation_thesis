<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_offices', function (Blueprint $table) {
            // Các thiết bị điện tử được dùng trong văn phòng phổ biến nhất như: máy vi tính, laptop, máy photocopy, …
            $table->id();
            $table->string('product_code')->nullable();
            $table->string('caching')->nullable(); //bộ nhớ đệm
            $table->string('cpu_speed')->nullable();
            $table->string('ram_capacity')->nullable(); // dung lượng ram
            $table->string('bus_speed')->nullable(); // tốc độ bus ram
            $table->string('capacity')->nullable(); // dung lượng
            $table->string('hard_drive')->nullable(); // loại ổ đĩa cứng
            $table->string('longs')->nullable(); // c.dài
            $table->string('width')->nullable(); // c.rộng
            $table->string('height')->nullable(); // c.cao
            $table->string('camera')->nullable();
            $table->string('connect')->nullable(); // kết nối khác laptop
            $table->string('lan_standard')->nullable(); // chuẩn LAN
            $table->string('serial_port')->nullable();// cổng I/O mặt sau
            $table->string('features')->nullable();// tính năng
            $table->string('webcam')->nullable();// webcam
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_offices');
    }
};
