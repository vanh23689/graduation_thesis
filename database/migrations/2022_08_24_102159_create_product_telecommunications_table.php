<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_telecommunications', function (Blueprint $table) {
            // Các thiết bị điện tử dùng trong ngành viễn thông thường có chức năng đặc thù là thu phát sóng. Chúng ta có thể kể đến các thiết bị như: điện thoại, các thiết bị truyền xa, thiết bị thu phát sóng,…
            $table->id();
            // ĐT
            $table->string('product_code')->nullable();
            $table->string('chipset')->nullable();
            $table->string('memory')->nullable();
            $table->text('before_camera')->nullable();
            $table->text('before_video')->nullable();
            $table->text('after_camera')->nullable();
            $table->text('after_video')->nullable();
            $table->string('sim')->nullable();
            $table->string('flash_light')->nullable();
            $table->string('fingerprint_sensor')->nullable();
            $table->string('special_features')->nullable();
            $table->string('types_of_sensors')->nullable();
            $table->string('speaker_phone')->nullable(); // loa ngoài
            // End ĐT

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_telecommunications');
    }
};
