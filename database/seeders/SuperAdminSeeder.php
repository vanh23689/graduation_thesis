<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // 1 Role Supper Admin
        Role::create(['guard_name' => 'web', 'name' => 'super_admin']);
        // 1 Role Admin
        Role::create(['guard_name' => 'web', 'name' => 'admin']);

        // 1 Role User
        Role::create(['guard_name' => 'web', 'name' => 'user']);


        // Create super admin
        $super = User::create([
            'last_name' => 'Admin',
            'first_name' => 'super',
            'phone' => '0987654321',
            'email' => 'instacheki@tryhatch.co.jp',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'remember_token' => Str::random(10),
        ]);

        // Add Role for Acc
        $super->assignRole('super_admin');

        // Permission
        Permission::create(['guard_name' => 'web', 'name' => 'create_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_user']);

        Permission::create(['guard_name' => 'web', 'name' => 'create_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_product']);

        Permission::create(['guard_name' => 'web', 'name' => 'show_donate']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_donate']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_donate']);

        Permission::create(['guard_name' => 'web', 'name' => 'create_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_customer']);

        // Permission notifications
        // Permission::create(['guard_name' => 'web', 'name' => 'manage_notification']);

        // Super Admin không chỉ quản lý các Admin còn lại, họ còn thực hiện và kiểm soát các dữ liệu nhạy cảm, thiết lập bảo mật, bảo vệ dịch vụ Google Workspace của tổ chức

        // Add Permission for Role
        $role_super = Role::findByName('super_admin');
        $role_super->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user',
            'create_product', 'show_product', 'update_product', 'delete_product',
            'show_donate', 'update_donate', 'delete_donate',
            'create_customer', 'show_customer', 'update_customer', 'delete_customer'
        ]);



        $role_admin = Role::findByName('admin');
        $role_admin->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user',
            'create_product', 'show_product', 'update_product', 'delete_product',
            'show_donate', 'update_donate', 'delete_donate',
            'create_customer', 'show_customer', 'update_customer', 'delete_customer'
        ]);
    }
}
