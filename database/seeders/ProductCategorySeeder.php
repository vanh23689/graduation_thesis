<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ([
            [
                'name' => 'Thiết bị viễn thông',
                'parent_id' => 0,
            ],
            [
                'name' => 'Thiết bị văn phòng',
                'parent_id' => 0,
            ],
            [
                'name' => 'Thiết bị dùng trong nhà',
                'parent_id' => 0,
            ],
            [
                'name' => 'Điện thoại',
                'parent_id' => 1
            ],
            [
                'name' => 'Laptop',
                'parent_id' => 2
            ],
            [
                'name' => 'PC',
                'parent_id' => 2
            ],
            [
                'name' => 'Tablet',
                'parent_id' => 2
            ],
            [
                'name' => 'TV',
                'parent_id' => 3
            ],
            [
                'name' => 'Air-conditioner',
                'parent_id' => 3
            ],
            [
                'name' => 'Cooker',
                'parent_id' => 3
            ],
            [
                'name' => 'Television',
                'parent_id' => 3
            ],
        ]);
        ProductCategory::insert($data);
    }
}
