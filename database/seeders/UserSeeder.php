<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // 1 Role Supper Admin
        Role::create(['guard_name' => 'web', 'name' => 'super_admin']);
        // 1 Role Admin
        Role::create(['guard_name' => 'web', 'name' => 'admin']);

        // 1 Role User
        Role::create(['guard_name' => 'web', 'name' => 'user']);


        //Create Acc
        $super_admin = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'Master',
            'phone' => '0987654321',
            'email' => 'master@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'check_login_times' => 1,
            'remember_token' => Str::random(10),
        ]);

        $admin = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'Admin',
            'phone' => '0987654321',
            'email' => 'admin@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'check_login_times' => 1,
            'remember_token' => Str::random(10),
        ]);

        $user = User::create([
            'last_name' => 'Rabiloo',
            'first_name' => 'User',
            'phone' => '0987654321',
            'email' => 'user@rabiloo.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('a12345678X'),
            'check_login_times' => 1,
            'remember_token' => Str::random(10),
        ]);

        //Add Role for Acc
        $super_admin->assignRole('super_admin');
        $admin->assignRole('admin');
        $user->assignRole('user');

        // Permission
        Permission::create(['guard_name' => 'web', 'name' => 'create_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_user']);

        Permission::create(['guard_name' => 'web', 'name' => 'create_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_product']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_product']);

        Permission::create(['guard_name' => 'web', 'name' => 'show_donate']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_donate']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_donate']);

        Permission::create(['guard_name' => 'web', 'name' => 'create_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_customer']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_customer']);

        Permission::create(['guard_name' => 'web', 'name' => 'create_role']);
        Permission::create(['guard_name' => 'web', 'name' => 'show_role']);
        Permission::create(['guard_name' => 'web', 'name' => 'update_role']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete_role']);

        // Permission notifications
        // Permission::create(['guard_name' => 'web', 'name' => 'manage_notification']);

        //Add Permission for Role
        $role_super_admin = Role::findByName('super_admin');
        $role_super_admin->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user',
            'create_product', 'show_product', 'update_product', 'delete_product',
            'show_donate', 'update_donate', 'delete_donate',
            'create_customer', 'show_customer', 'update_customer', 'delete_customer',
            'create_role', 'show_role', 'update_role', 'delete_role'

            //  'manage_notification'
        ]);



        $role_admin = Role::findByName('admin');
        $role_admin->givePermissionTo([
            'create_user', 'show_user', 'update_user', 'delete_user',
            'create_product', 'show_product', 'update_product', 'delete_product',
            'show_donate', 'update_donate', 'delete_donate',
            'create_customer', 'show_customer', 'update_customer', 'delete_customer',
            'create_role', 'show_role', 'update_role', 'delete_role'
        ]);
    }
}
