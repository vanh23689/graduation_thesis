<?php

namespace Database\Seeders;

use App\Models\Arrange;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'thấp đến cao'
            ],
            [
                'name' => 'cao đến thấp'
            ]
        ];
        Arrange::insert($data);
    }
}
