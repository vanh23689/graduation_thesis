<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'create_role', 'show_role', 'update_role', 'delete_role',
            'create_product', 'show_product', 'update_product', 'delete_product',
            'show_donate', 'update_donate', 'delete_donate',
            'create_customer', 'show_customer', 'update_customer', 'delete_customer',
            'create_role', 'show_role', 'update_role', 'delete_role',
         ];

         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
